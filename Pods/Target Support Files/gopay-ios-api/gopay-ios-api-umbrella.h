#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "GPAdditionalParam.h"
#import "GPAPI.h"
#import "GPAPIConstants.h"
#import "GPCallback.h"
#import "GPContact.h"
#import "GPCreateNextPayment.h"
#import "GPItem.h"
#import "GPPayer.h"
#import "GPPayment.h"
#import "GPPaymentRequest.h"
#import "GPPreauthorization.h"
#import "GPRecurrence.h"
#import "GPTarget.h"
#import "GPAPIWebView.h"

FOUNDATION_EXPORT double gopay_ios_apiVersionNumber;
FOUNDATION_EXPORT const unsigned char gopay_ios_apiVersionString[];


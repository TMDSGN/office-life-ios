# GoPay iOS SDK

## Installation

###### The simplest way to install SDK is to use CocoaPods:
Add this line to your podfile:
```ruby
pod 'gopay-ios-api'
```

## Usage

#### 1. Initialize the GoPay iOS SDK
Add the GoPay iOS SDK header file to your app delegate. Import this header in any of your implementation files to use the SDK.

```objective-c
#import <GPAPI.h>
```

Add the following code to initialize the SDK in your app delegate's **application:didFinishLaunchingWithOptions:** method.

```objective-c
[[GPAPI sharedAPI] setClientID:@"<<Your client ID>>" clientSecret:@"<<Your client secret>>" productionMode:NO];
```

You dont't need specify token scope because SDK automatically get it depending on the operation that performs.

#### 2. Initialize Payment object
We need to initialize Payment to store info about payment in it

```objective-c
GPPaymentRequest *payment = [[GPPaymentRequest alloc] init];
payment.amount = 20;
payment.currency = @"CZK";
payment.order_number = @"001";
payment.order_description = @"test";
    
GPPayer *payer = [[GPPayer alloc] init];
payer.allowed_payment_instruments = @[];
payer.default_payment_instrument = @"PAYMENT_CARD";
payer.contact.first_name = @"Zbynek";
payer.contact.last_name = @"Zak";
payer.contact.email = @"zbynek.zak@gopay.cz";
payer.contact.phone_number = @"+420777456123";
payer.contact.city = @"C.Budejovice";
payer.contact.street = @"Plana 67";
payer.contact.postal_code = @"370 03";
payer.contact.country_code = @"CZE";
    
payment.payer = payer;
    
GPTarget *target = [[GPTarget alloc] init];
target.type = PAY_TARGET_ACCOUNT;
target.goid = @"8035442195"; // Usually your go id
    
payment.target = target;
```
#### 3. Create payment
```objective-c

[GPAPI sharedAPI] createPayment:payment withHandleSuccess:^(GPPayment *payment) {

} failure:^(NSError *error) {

}];
```

#### 4. Open payment gateway in GPAPIWebView
In handle success block we get payment url a then we load it in GPAPIWebView which is UIViewController with UIWebView and some special functionality.
```objective-c
#import <GPAPIWebView.h>

GPAPIWebView *webView = [[GPAPIWebView alloc] init];
webView.openInSafariUrls = [[GPAPI sharedAPI] bankURLs];
webView.url = payment.gw_url; // z vytvorene platby ziskame url brany, kterou otevreme v GPAPIWebView
        
webView.webViewDidDone = ^(long long paymentID) {
    [self dismissViewControllerAnimated:YES completion:nil];
};
        
webView.webViewCanceled = ^{
    [self dismissViewControllerAnimated:YES completion:nil];
};
        
UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webView];
navController.modalPresentationStyle = UIModalPresentationFormSheet;
[self presentViewController:navController animated:YES completion:nil]; 
```

#### 5. Check payment status
```objective-c
[[GPAPI sharedAPI] getPaymentWithID:paymentID withHandleSuccess:^(GPPayment *payment) {
    NSLog(@"payment state: %@", [payment userReadableState]);
} failure:^(NSError *error) {
    NSLog(@"error: %@", error);
}];
```
#### 6. Refund payment
To refund payment we need payment id and amount to be refunded.
```objective-c
[[GPAPI sharedAPI] refundPaymentWithID:3000006620 amount:10000 withHandleSuccess:^(GPAPIResult result) {
        
} failure:^(NSError *error) {

}];
```

#### 7. Create recurrence
To create recurrence we need to create **GPCreateNextPayment** object and set **amount, currency, order_number, order_description** and **items** then we call **createRecurrence** with this object and payment id.
```objective-c
GPCreateNextPayment *createNextPayment = [[GPCreateNextPayment alloc] init];
createNextPayment.amount = 500;
createNextPayment.currency = @"CZK";
createNextPayment.order_number = @"002";
createNextPayment.order_description = @"pojisteni02";
createNextPayment.items = @[[[GPItem alloc] initWithName:@"item01" amount:500]];

[[GPAPI sharedAPI] createRecurrence:createNextPayment forPaymentWithID:3000006542 withHandleSuccess:^(GPPayment *payment) {
        
} failure:^(NSError *error) {

}];
```
#### 8. Void reccurence 
To void reccurence we need payment id.
```objective-c
[[GPAPI sharedAPI] voidRecurrenceForPaymentWithID:3000139597 withHandleSuccess:^(GPAPIResult result) {

} failure:^(NSError *error) {
        
}];

```
#### 9. Void authorization
To void authorization we need payment id.
```objective-c
[[GPAPI sharedAPI] refundPaymentWithID:3000006620 amount:10000 withHandleSuccess:^(GPAPIResult result) {
        
} failure:^(NSError *error) {

}];
```
#### 10. Capture preauthorized payment
To capture preauthorized payment we need payment id.
```objective-c
[[GPAPI sharedAPI] capturePaymentWithID:3000141703 withHandleSuccess:^(GPAPIResult result) {

} failure:^(NSError *error) {
        
}];
```

### SDK responses
Every SDK call has success and failure block. In failure block we get **NSError** object, where in **userInfo** are errors from API.
```objective-c
NSLog(@"errors: %@", error.userInfo[@"errors"]);
```
In success block we get **GPPayment** object from **createPayment**, **getPaymentWithID** and **createRecurrence** methods. **GPPayment** is defined in **GPPayment.h**. From **refundPaymentWithID**, **voidRecurrence**, **voidAuthorization** and **capturePayment** we get **GPAPIResult** enum which is defined in **GPAPIConstants.h**.

### Enums
All enums are defined in **GPAPIConstants.h**.




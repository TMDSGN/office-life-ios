//
//  GPAPIWebView.h
//  GoPay-API-DEMO
//
//  Created by Kryštof Celba on 05.09.15.
//  Copyright (c) 2015 GoPay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GPAPIWebView : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *webView;

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *returnURL;

@property (nonatomic, strong) NSArray *openInSafariUrls;

@property (copy, nonatomic) void (^webViewDidDone) (long long paymentID) ;
@property (copy, nonatomic) void (^webViewCanceled) () ;

@end

//
//  GPAPIWebView.m
//  GoPay-API-DEMO
//
//  Created by Kryštof Celba on 05.09.15.
//  Copyright (c) 2015 GoPay. All rights reserved.
//

#import "GPAPIWebView.h"

@implementation GPAPIWebView


- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    self.webView.delegate = self;
    
    self.webView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Zavřít" style:UIBarButtonItemStyleDone target:self action:@selector(dissmis)];
    
    [self.view addSubview:self.webView];
}

- (void) viewDidAppear:(BOOL)animated {
    [self updateViewConstraints];
}


- (void) updateViewConstraints {
    [super updateViewConstraints];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.webView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:0
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.webView
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:0
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.0
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.webView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:0
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.webView
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:0
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.0
                                                           constant:0]];
    
    
}

- (void)dissmis {
    if (self.webViewCanceled) {
        self.webViewCanceled();
    }
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([request.URL.absoluteString rangeOfString:@"gopay-ios-api.cz"].location != NSNotFound) {
        
        NSString *pattern = @"id=(\\d+)";
        
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:pattern
                                      options:NSRegularExpressionCaseInsensitive
                                      error:nil];
        NSTextCheckingResult *textCheckingResult = [regex firstMatchInString:request.URL.absoluteString options:0 range:NSMakeRange(0, request.URL.absoluteString.length)];
        
        NSRange matchRange = [textCheckingResult rangeAtIndex:1];
        NSString *match = [request.URL.absoluteString substringWithRange:matchRange];
        
        if (self.webViewDidDone) {
            self.webViewDidDone((match.length > 0) ? [match longLongValue] : -1);
        }
        return NO;
    }
    
    for (NSString *url in self.openInSafariUrls) {
        if ([request.URL.absoluteString isEqualToString:url]) {
            [[UIApplication sharedApplication] openURL:request.URL];
            return NO;
        }
    }
    
    return YES;
}

@end

//
//  GPAdditionalParam.m
//  Pods
//
//  Created by Kryštof Celba on 02.12.15.
//
//

#import "GPAdditionalParam.h"

@implementation GPAdditionalParam

- (instancetype) initWithName:(NSString *) name amount:(NSString *) value {
    if (self = [super init]) {
        self.name = name;
        self.value = value;
    }
    return self;
}

@end

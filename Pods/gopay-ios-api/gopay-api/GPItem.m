//
//  GPItem.m
//  Pods
//
//  Created by Kryštof Celba on 02.12.15.
//
//

#import "GPItem.h"

@implementation GPItem

- (instancetype) initWithName:(NSString *) name amount:(long) amount {
    if (self = [super init]) {
        self.name = name;
        self.amount = amount;
        self.count = 1;
    }
    return self;
}


- (instancetype) initWithName:(NSString *) name amount:(long) amount count:(long) count {
    if (self = [super init]) {
        self.name = name;
        self.amount = amount;
        self.count = count;
    }
    return self;
}

@end

//
//  GPPayment.h
//  GoPay
//
//  Created by Kryštof Celba on 08.10.14.
//  Copyright (c) 2014 GoPay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "GPPayer.h"
#import "GPTarget.h"
#import "GPRecurrence.h"
#import "GPPreauthorization.h"
#import "GPAdditionalParam.h"
#import "GPAPIConstants.h"

@interface GPPayment : JSONModel

@property (nonatomic) long long id;

@property (nonatomic) GPPaymentState state;

@property (nonatomic) long long amount;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) GPPayer<Optional> *payer;
@property (nonatomic, strong) GPTarget *target;

@property (nonatomic, strong) NSString<Optional> *order_number;
@property (nonatomic, strong) NSString<Optional> *order_description;

@property (nonatomic, strong) GPPreauthorization<Optional> *preauthorization;

@property (nonatomic, strong) GPRecurrence<Optional> *recurrence;

@property (nonatomic, strong) NSArray<GPAdditionalParam, Optional> *additional_params;

@property (nonatomic, strong) NSString<Optional> *lang;
@property (nonatomic, strong) NSString *gw_url;

- (NSString *) userReadableState;

@end

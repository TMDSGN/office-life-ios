//
//  GPContact.h
//  GoPay
//
//  Created by Kryštof Celba on 08.10.14.
//  Copyright (c) 2014 GoPay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface GPContact : JSONModel

@property (nonatomic, strong) NSString<Optional> *first_name;
@property (nonatomic, strong) NSString<Optional> *last_name;

@property (nonatomic, strong) NSString<Optional> *email;
@property (nonatomic, strong) NSString<Optional> *phone_number;

//adress
@property (nonatomic, strong) NSString<Optional> *city;
@property (nonatomic, strong) NSString<Optional> *street;
@property (nonatomic, strong) NSString<Optional> *postal_code;
@property (nonatomic, strong) NSString<Optional> *country_code;


@end

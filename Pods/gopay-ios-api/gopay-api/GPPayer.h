//
//  GPPayer.h
//  GoPay
//
//  Created by Kryštof Celba on 08.10.14.
//  Copyright (c) 2014 GoPay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "GPContact.h"
#import "GPAPIConstants.h"

@protocol NSString

@end

@interface GPPayer : JSONModel

@property (nonatomic, strong) NSArray<NSString, Optional> *allowed_payment_instruments;
@property (nonatomic, strong) NSString<Optional> *default_payment_instrument;

@property (nonatomic, strong) NSString<Optional> *default_swift;
@property (nonatomic, strong) NSArray<NSString, Optional> *allowed_swifts;

@property (nonatomic, strong) GPContact<Optional> *contact;


@end

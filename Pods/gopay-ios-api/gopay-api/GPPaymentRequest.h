//
//  GPPaymentRequest.h
//  Pods
//
//  Created by Kryštof Celba on 03.12.15.
//
//
#import "JSONModel.h"

#import "GPPayer.h"
#import "GPTarget.h"
#import "GPCallback.h"
#import "GPRecurrence.h"
#import "GPItem.h"
#import "GPAdditionalParam.h"

@interface GPPaymentRequest : JSONModel

@property (nonatomic, strong) GPPayer<Optional> *payer;
@property (nonatomic, strong) GPTarget *target;

@property (nonatomic) long long amount;
@property (nonatomic, strong) NSString *currency;

@property (nonatomic, strong) NSString<Optional> *order_number;
@property (nonatomic, strong) NSString<Optional> *order_description;

@property (nonatomic, strong) NSArray<GPItem> *items;
@property (nonatomic) BOOL preauthorization;
@property (nonatomic, strong) GPRecurrence *recurrence;
@property (nonatomic, strong) GPCallback *callback;
@property (nonatomic, strong) NSArray<GPAdditionalParam, Optional> *additional_params;

@property (nonatomic, strong) NSString *lang;

@end
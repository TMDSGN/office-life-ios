//
//  GPItem.h
//  Pods
//
//  Created by Kryštof Celba on 02.12.15.
//
//

#import "JSONModel.h"

@protocol GPItem
@end

@interface GPItem : JSONModel

- (instancetype) initWithName:(NSString *) name amount:(long) amount;
- (instancetype) initWithName:(NSString *) name amount:(long) amount count:(long) count;

@property (nonatomic, strong) NSString *name;
@property (nonatomic) long amount;
@property (nonatomic) long count;

@end

//
//  GPCreateNextPayment.h
//  Pods
//
//  Created by Kryštof Celba on 02.12.15.
//
//

#import "JSONModel.h"
#import "GPItem.h"
#import "GPAdditionalParam.h"

@interface GPCreateNextPayment : JSONModel

@property (nonatomic) long long amount;
@property (nonatomic, strong) NSString *currency;

@property (nonatomic, strong) NSString *order_number;
@property (nonatomic, strong) NSString *order_description;

@property (nonatomic, strong) NSArray<GPItem> *items;
@property (nonatomic, strong) NSArray<GPAdditionalParam, Optional> *additional_params; // optional

@end

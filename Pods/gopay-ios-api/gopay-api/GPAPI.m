//
//  GPAPI.m
//  GoPay-API-DEMO
//
//  Created by Kryštof Celba on 02.09.15.
//  Copyright (c) 2015 GoPay. All rights reserved.
//

#import "GPAPI.h"
#import "AFNetworking/AFNetworking.h"
#import "AFNetworking/AFNetworkActivityIndicatorManager.h"
#import "MF_Base64Additions.h"

#define BASE_URL self.productionMode ? @"https://gate.gopay.cz/api" : @"https://gw.sandbox.gopay.com/api"

@interface GPAPI ()

@property (strong, nonatomic) NSString *clientID;
@property (strong, nonatomic) NSString *clientSecret;

@property (nonatomic) BOOL productionMode;

@end


@implementation GPAPI

+ (id)sharedAPI
{
    static GPAPI *sharedAPI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAPI = [[super alloc] init];
        sharedAPI.operationQueue = [[NSOperationQueue alloc] init];
        
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    });
    return sharedAPI;
}

- (void) setClientID:(NSString *) clientID clientSecret:(NSString *) clientSecret productionMode:(BOOL) productionMode {
    self.productionMode = productionMode;
    self.clientID = clientID;
    self.clientSecret = clientSecret;
    
    [self getBankURLsWithHandleSuccess:^(NSArray *banks) {
        self.bankURLs = banks;
    } failure:^(NSError *error) {
        
    }];
}


#pragma mark - common
- (NSMutableURLRequest *)POSTRequestWithPath:(NSString *)path data:(NSString *)data contentType:(NSString *)contentType
{
    NSMutableURLRequest *req=[[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", BASE_URL, path]]];
    
    [req setHTTPMethod:@"POST"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [req setValue:contentType forHTTPHeaderField:@"Content-Type"];
    [req setHTTPBody:[NSData dataWithBytes:[data UTF8String] length:[data length]]];
    
    return  req;
}

- (NSMutableURLRequest *)GETRequestWithPath:(NSString *)path
{
    NSMutableURLRequest *req=[[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", BASE_URL, path]]];
    
    [req setHTTPMethod:@"GET"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    return  req;
}

- (AFHTTPRequestOperation *)requestOperationWithRequest:(NSURLRequest *)request withHandleSuccess:(void (^__strong)(AFHTTPRequestOperation *__strong, __strong id))success failure:(void (^__strong)(AFHTTPRequestOperation *__strong,NSError *__strong))failure
{
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    requestOperation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSMutableDictionary *mutableUserInfo = error.userInfo.mutableCopy;
        [mutableUserInfo setObject:operation.responseObject[@"errors"] forKey:@"errors"];
        NSError *newError = [[NSError alloc] initWithDomain:@"cz.gopay" code:operation.response.statusCode userInfo:mutableUserInfo];
        failure(operation, newError);
    }];
    
    [self.operationQueue addOperation:requestOperation];
    
    return requestOperation;
}

- (void) getTokenWithType:(NSString *)type withHandleSuccess:(void (^)(NSString *token))success failure:(void (^)(NSError *error))failure
{
    if (!self.clientID || !self.clientSecret) {
        NSLog(@"Please set your GoPay Client ID and Client Secret first!");
        abort();
    }
    
    
    NSString *postStr = [NSString stringWithFormat:@"grant_type=client_credentials&scope=%@", type];
    
    NSMutableURLRequest *req = [self POSTRequestWithPath:@"oauth2/token" data:postStr contentType:@"application/x-www-form-urlencoded"];
    
    NSString *authHeader = [@"Basic " stringByAppendingFormat:@"%@", [[NSString stringWithFormat:@"%@:%@", self.clientID, self.clientSecret] base64String]];
    [req addValue:authHeader forHTTPHeaderField:@"Authorization"];
    
    [self requestOperationWithRequest:req withHandleSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject[@"access_token"]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void) createPayment:(GPPaymentRequest *)payment withHandleSuccess:(void (^)(GPPayment *payment))success failure:(void (^)(NSError *error))failure
{
    [self getTokenWithType:@"payment-create" withHandleSuccess:^(NSString *token) {
        payment.callback = [[GPCallback alloc] init];
        payment.callback.return_url = @"https://www.gopay-ios-api.cz/return";
        payment.callback.notification_url = @"https://www.gopay-ios-api.cz/notify";
        
        NSMutableURLRequest *req = [self POSTRequestWithPath:[NSString stringWithFormat:@"payments/payment"] data:[payment toJSONString] contentType:@"application/json"];
        
        NSString *authHeader = [NSString stringWithFormat:@"Bearer %@", token];
        [req addValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        
        [self requestOperationWithRequest:req withHandleSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error;
            
            GPPayment *newPayment = [[GPPayment alloc] initWithDictionary:responseObject error:&error];
            success(newPayment);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Handle Error
            failure(error);
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void) getPaymentWithID: (long long) paymentID withHandleSuccess:(void (^)(GPPayment *payment))success failure:(void (^)(NSError *error))failure {
    
    [self getTokenWithType:@"payment-all" withHandleSuccess:^(NSString *token) {

        NSMutableURLRequest *req = [self GETRequestWithPath:[NSString stringWithFormat:@"payments/payment/%lld", paymentID]];
        
        NSString *authHeader = [NSString stringWithFormat:@"Bearer %@", token];
        [req addValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        
        [self requestOperationWithRequest:req withHandleSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            GPPayment *newPayment = [[GPPayment alloc] initWithDictionary:responseObject error:nil];
            success(newPayment);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Handle Error
            failure(error);
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (GPAPIResult) resultFromString:(NSString *) string {
    if ([string isEqualToString:@"ACCEPTED"]) {
        return GPAPI_RESULT_ACCEPTED;
    } else if ([string isEqualToString:@"FINISHED"]) {
        return GPAPI_RESULT_FINISHED;
    } else {
        return GPAPI_RESULT_FAILED;
    }
}

- (void) refundPaymentWithID: (long long) paymentID amount: (long) amount withHandleSuccess:(void (^)(GPAPIResult result))success failure:(void (^)(NSError *error))failure {
    [self getTokenWithType:@"payment-all" withHandleSuccess:^(NSString *token) {
        
        NSMutableURLRequest *req = [self POSTRequestWithPath:[NSString stringWithFormat:@"payments/payment/%lld/refund", paymentID] data:[NSString stringWithFormat:@"amount=%ld", amount] contentType:@"application/x-www-form-urlencoded"];
        
        NSString *authHeader = [NSString stringWithFormat:@"Bearer %@", token];
        [req addValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        [self requestOperationWithRequest:req withHandleSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            success([self resultFromString:responseObject[@"result"]]);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Handle Error
            failure(error);
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void) createRecurrence:(GPCreateNextPayment *)nextPayment forPaymentWithID:(long long) paymentID withHandleSuccess:(void (^)(GPPayment *payment))success failure:(void (^)(NSError *error))failure {
    [self getTokenWithType:@"payment-all" withHandleSuccess:^(NSString *token) {
        
        
        NSMutableURLRequest *req = [self POSTRequestWithPath:[NSString stringWithFormat:@"payments/payment/%lld/create-recurrence", paymentID] data:[nextPayment toJSONString] contentType:@"application/json"];
        
        NSString *authHeader = [NSString stringWithFormat:@"Bearer %@", token];
        [req addValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        [self requestOperationWithRequest:req withHandleSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            GPPayment *newPayment = [[GPPayment alloc] initWithDictionary:responseObject error:nil];
            success(newPayment);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Handle Error
            failure(error);
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void) voidRecurrenceForPaymentWithID:(long long) paymentID withHandleSuccess:(void (^)(GPAPIResult result))success failure:(void (^)(NSError *error))failure {
    [self getTokenWithType:@"payment-all" withHandleSuccess:^(NSString *token) {
        
        NSMutableURLRequest *req = [self POSTRequestWithPath:[NSString stringWithFormat:@"payments/payment/%lld/void-recurrence", paymentID] data:@"" contentType:@"application/x-www-form-urlencoded"];
        
        NSString *authHeader = [NSString stringWithFormat:@"Bearer %@", token];
        [req addValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        [self requestOperationWithRequest:req withHandleSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            success([self resultFromString:responseObject[@"result"]]);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void) voidAuthorizationForPaymentWithID:(long long) paymentID withHandleSuccess:(void (^)(GPAPIResult result))success failure:(void (^)(NSError *error))failure {
    [self getTokenWithType:@"payment-all" withHandleSuccess:^(NSString *token) {
        
        NSMutableURLRequest *req = [self POSTRequestWithPath:[NSString stringWithFormat:@"payments/payment/%lld/void-authorization", paymentID] data:@"" contentType:@"application/x-www-form-urlencoded"];
        
        NSString *authHeader = [NSString stringWithFormat:@"Bearer %@", token];
        [req addValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        [self requestOperationWithRequest:req withHandleSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            success([self resultFromString:responseObject[@"result"]]);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Handle Error
            failure(error);
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}


- (void) capturePaymentWithID:(long long) paymentID withHandleSuccess:(void (^)(GPAPIResult result))success failure:(void (^)(NSError *error))failure {
    [self getTokenWithType:@"payment-all" withHandleSuccess:^(NSString *token) {
        
        NSMutableURLRequest *req = [self POSTRequestWithPath:[NSString stringWithFormat:@"payments/payment/%lld/capture", paymentID] data:@"" contentType:@"application/x-www-form-urlencoded"];
        
        NSString *authHeader = [NSString stringWithFormat:@"Bearer %@", token];
        [req addValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        
        [self requestOperationWithRequest:req withHandleSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            success([self resultFromString:responseObject[@"result"]]);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Handle Error
            failure(error);
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}



- (void) getBankURLsWithHandleSuccess:(void (^)(NSArray *banks))success failure:(void (^)(NSError *error))failure {
    [self getTokenWithType:@"payment-all" withHandleSuccess:^(NSString *token) {
        
        NSMutableURLRequest *req = [self GETRequestWithPath:[NSString stringWithFormat:@"enum/banks"]];
        
        NSString *authHeader = [NSString stringWithFormat:@"Bearer %@", token];
        [req addValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        [self requestOperationWithRequest:req withHandleSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSArray *bankURLs = @[];
            for (NSDictionary *bank in responseObject[@"banks"]) {
                NSString *ib_url = bank[@"ib_url"];
                if ([ib_url isKindOfClass:NSString.class]) {
                    bankURLs = [bankURLs arrayByAddingObject:ib_url];
                }
            }
            
            success(bankURLs);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Handle Error
            failure(error);
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end

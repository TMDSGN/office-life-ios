//
//  GPTarget.h
//  GoPay
//
//  Created by Kryštof Celba on 08.10.14.
//  Copyright (c) 2014 GoPay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "GPAPIConstants.h"

@interface GPTarget : JSONModel

@property (nonatomic) GPPaymentTargetType type;

@property (nonatomic) NSString *goid;

@end

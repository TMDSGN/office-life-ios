//
//  GPCallback.h
//  Pods
//
//  Created by Kryštof Celba on 03.12.15.
//
//

#import "JSONModel.h"

@interface GPCallback : JSONModel

@property (nonatomic, strong) NSString *return_url;
@property (nonatomic, strong) NSString *notification_url;

@end

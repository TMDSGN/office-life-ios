//
//  Target.m
//  GoPay
//
//  Created by Kryštof Celba on 08.10.14.
//  Copyright (c) 2014 GoPay. All rights reserved.
//

#import "GPTarget.h"

#define PAYMENT_TARGET_TYPE_KEYS @[@"ACCOUNT"]

@implementation GPTarget

- (void)setTypeWithNSString:(NSString*)string
{
    self.type = [PAYMENT_TARGET_TYPE_KEYS indexOfObject:string];
}

-(id)JSONObjectForType
{
    return PAYMENT_TARGET_TYPE_KEYS[self.type];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    if ([propertyName isEqualToString: @"go_id"]) return YES;
    return NO;
}

@end

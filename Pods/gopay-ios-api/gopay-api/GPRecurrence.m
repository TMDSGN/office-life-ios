//
//  GPRecurrence.m
//  Pods
//
//  Created by Kryštof Celba on 02.12.15.
//
//

#import "GPRecurrence.h"

#define RECURRENCE_CYCLE_KEYS @[@"DAY", @"WEEK", @"MONTH", @"ON_DEMAND"]
#define RECURRENCE_STATE_KEYS @[@"REQUESTED", @"STARTED", @"STOPPED"]

@implementation GPRecurrence

- (void)setRecurrence_cycleWithNSString:(NSString*)string
{
    self.recurrence_cycle = [RECURRENCE_CYCLE_KEYS indexOfObject:string];
}

- (id)JSONObjectForRecurrence_cycle
{
    return RECURRENCE_CYCLE_KEYS[self.recurrence_cycle];
}

- (void)setRecurrence_stateWithNSString:(NSString*)string
{
    self.recurrence_state = [RECURRENCE_STATE_KEYS indexOfObject:string];
}

- (id)JSONObjectForRecurrence_state
{
    return RECURRENCE_STATE_KEYS[self.recurrence_state];
}

@end

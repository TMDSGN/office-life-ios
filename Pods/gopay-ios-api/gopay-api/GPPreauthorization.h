//
//  GPPreauthorization.h
//  Pods
//
//  Created by Kryštof Celba on 09.12.15.
//
//

#import "JSONModel.h"

@interface GPPreauthorization : JSONModel

@property (nonatomic) BOOL requested;
@property (strong, nonatomic) NSString *state;

@end

//
//  GPPayer.m
//  GoPay
//
//  Created by Kryštof Celba on 08.10.14.
//  Copyright (c) 2014 GoPay. All rights reserved.
//

#import "GPPayer.h"

@implementation GPPayer

- (instancetype) init
{
    if (self = [super init]) {
        self.default_payment_instrument = @"BANK_ACCOUNT";
        self.contact = [[GPContact alloc] init];
    }
    return self;
}

@end

//
//  GPPayment.m
//  GoPay
//
//  Created by Kryštof Celba on 08.10.14.
//  Copyright (c) 2014 GoPay. All rights reserved.
//

#import "GPPayment.h"

#define PAYMENT_STATE_USER_READABLE_STRINGS @[@"Platba založena", @"Platební metoda vybrána", @"Platba zaplacena", @"Platba předautorizována", @"Platba zrušena", @"Vypršelá platnost platby", @"Platba refundována", @"Platba částečně refundována"]

#define PAYMENT_STATE_KEYS @[@"CREATED", @"PAYMENT_METHOD_CHOSEN", @"PAID", @"AUTHORIZED", @"CANCELED", @"TIMEOUTED", @"REFUNDED", @"PARTIALLY_REFUNDED"]

@implementation GPPayment

- (instancetype)init
{
    if (self = [super init]) {
        self.payer = [[GPPayer alloc] init];
        self.target = [[GPTarget alloc] init];
    }
    
    return self;
}

- (NSString *) userReadableState {
    return PAYMENT_STATE_USER_READABLE_STRINGS[self.state];
}

- (void)setStateWithNSString:(NSString*)string
{
    self.state = [PAYMENT_STATE_KEYS indexOfObject:string];
}

- (id)JSONObjectForState
{
    return PAYMENT_STATE_KEYS[self.state];
}

@end

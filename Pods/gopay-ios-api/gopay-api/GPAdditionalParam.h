//
//  GPAdditionalParam.h
//  Pods
//
//  Created by Kryštof Celba on 02.12.15.
//
//

#import "JSONModel.h"

@protocol GPAdditionalParam
@end

@interface GPAdditionalParam : JSONModel

- (instancetype) initWithName:(NSString *) name amount:(NSString *) value;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *value;

@end

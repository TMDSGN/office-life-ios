//
//  GPAPI.h
//  GoPay-API-DEMO
//
//  Created by Kryštof Celba on 02.09.15.
//  Copyright (c) 2015 GoPay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPPayment.h"
#import "GPCreateNextPayment.h"
#import "GPPaymentRequest.h"


@interface GPAPI : NSObject

@property (strong, nonatomic) NSOperationQueue *operationQueue;

@property(strong, nonatomic) NSArray *bankURLs;

+ (id) sharedAPI;

- (void) setClientID:(NSString *) clientID clientSecret:(NSString *) clientSecret productionMode:(BOOL) productionMode;

- (void) createPayment:(GPPaymentRequest *)payment withHandleSuccess:(void (^)(GPPayment *payment))success failure:(void (^)(NSError *error))failure;

- (void) getPaymentWithID:(long long) paymentID withHandleSuccess:(void (^)(GPPayment *payment))success failure:(void (^)(NSError *error))failure;

- (void) refundPaymentWithID: (long long) paymentID amount: (long) amount withHandleSuccess:(void (^)(GPAPIResult result))success failure:(void (^)(NSError *error))failure;

- (void) createRecurrence:(GPCreateNextPayment *)nextPayment forPaymentWithID:(long long) paymentID withHandleSuccess:(void (^)(GPPayment *payment))success failure:(void (^)(NSError *error))failure;

- (void) voidRecurrenceForPaymentWithID:(long long) paymentID withHandleSuccess:(void (^)(GPAPIResult result))success failure:(void (^)(NSError *error))failure;

- (void) voidAuthorizationForPaymentWithID:(long long) paymentID withHandleSuccess:(void (^)(GPAPIResult result))success failure:(void (^)(NSError *error))failure;

- (void) capturePaymentWithID:(long long) paymentID withHandleSuccess:(void (^)(GPAPIResult result))success failure:(void (^)(NSError *error))failure;

@end

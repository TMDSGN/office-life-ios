//
//  GPReccurence.h
//  Pods
//
//  Created by Kryštof Celba on 02.12.15.
//
//

#import "JSONModel.h"
#import "GPAPIConstants.h"



@interface GPRecurrence : JSONModel

@property (nonatomic) GPReccurenceCycle recurrence_cycle;
@property (nonatomic) long recurrence_period;

@property (nonatomic, strong) NSString *recurrence_date_to;

@property (nonatomic) GPReccurenceState recurrence_state;

@end

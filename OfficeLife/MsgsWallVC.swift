//
//  MsgsVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 27.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import iOS_Slide_Menu
import IQKeyboardManagerSwift
import SwiftLoader

class MsgsWallVC: HamburgerContainerVC {
    
    var storedOffsets = [Int: CGFloat]()
    
    var reloadTheWallBool = false
    //activity wall
    var activityId: String = ""
    var activityWall = false
    //messages wall
    var conversationReceiverId: String = ""
    var messagesWall = false
    
    fileprivate var currentPage = 1
    fileprivate var totalPage = 1
    
    fileprivate var idArr = [String]()
    fileprivate var postedByIdArr = [String]()
    fileprivate var postedByNameArr = [String]()
    fileprivate var postedByPhotoArr = [String]()
    fileprivate var subtitleArr = [Date]()
    fileprivate var descArr = [String]()
    fileprivate var comentsTextArr = [[String]]()
    fileprivate var comentsPhotoArr = [[String]]()
    fileprivate var comentsCreatedArr = [[String]]()
    fileprivate var comentsUserIdArr = [[String]]()
    
    fileprivate var conversationReadArr = [Bool]()
    fileprivate var conversationPreviewTextArr = [String]()
    fileprivate var conversationReceiverIdArr = [String]()
    fileprivate var conversationReceiverNameArr = [String]()
    fileprivate var conversationReceiverPhotoArr = [String]()
    fileprivate var conversationDateArr = [String]()
    fileprivate var conversationMyPostArr = [Bool]()
    
    fileprivate var activityIdArr = [String]()
    fileprivate var tagArr = [String]()
    //main wall
    fileprivate var dateArr = [String]()
    fileprivate var attachmentPhotoArr = [[String]]()
    
    fileprivate var imagePicker = UIImagePickerController()
    fileprivate var photoString = [String]()
    
    fileprivate var loaderActive = true
    fileprivate var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObservers()
        customCellRegister()
        self.msgField.delegate = self
        self.msgField.placeholder = "Write message".localized()
        loadWall()
        if !Session.sharedInstance.profileWriteHomeWallPermission && !messagesWall {
            UIView.animate(withDuration: 0.1, animations: {
                self.tableViewToBotConstant.constant = 0
                self.msgContainer.isHidden = true
                self.view.layoutIfNeeded()
            })
        }
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        doneBtn.setTitle("Close".localized(), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if activityWall || messagesWall {
            if messagesWall {
                tableView.transform = CGAffineTransform(rotationAngle: -(CGFloat)(Double.pi));
            }
            navigationItem.leftBarButtonItem = nil
            let leftCustomBtn = UIButton(type: .custom)
            leftCustomBtn.setImage(UIImage(named: "arrow_back"), for: .normal)
            leftCustomBtn.frame = CGRect(x: 0, y: 0, width: 10, height: 30)
            leftCustomBtn.addTarget(self, action: #selector(btnBack), for: .touchUpInside)
            SlideNavigationController.sharedInstance().leftBarButtonItem = UIBarButtonItem(customView: leftCustomBtn)
        } else {
            setupDefaultMenuButton()
            setupNavBar("chatIconOval", "notificationIcon")
        }
        if reloadTheWallBool {
            if self.activityWall {
                self.title = "Activity wall".localized()
                getActivityPosts()
            } else if self.messagesWall {
                appTitle.text = "Messages".localized()
                getMessages()
            } else {
                getWallPosts(pulltorefresh: true)
            }
        }
        reloadTheWallBool = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !self.activityWall && !self.messagesWall {
            Session.sharedInstance.newFeedNotification = false
        }
    }
    
    @objc fileprivate func loadWall(){
        if self.activityWall {
            appTitle.text = "Activity wall".localized()
            appTitle.isHidden = false
            appLogo.isHidden = true
            getActivityPosts()
        } else if self.messagesWall {
            appTitle.text = "Messages".localized()
            appTitle.isHidden = false
            appLogo.isHidden = true
            getMessages()
        } else {
            appTitle.isHidden = true
            appLogo.isHidden = false
            getWallPosts(pulltorefresh: true)
        }
    }
    
    @objc fileprivate func openActivityDetail(_ notification: NSNotification){
        if !activityWall && !messagesWall {
            let dict = notification.object as! NSDictionary
            let tempDetailId = dict["detailId"] as! String
            
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ActivityDetailVC") as! ActivityDetailVC
            openNewVC.activityId = tempDetailId
            openNewVC.titleCategory = "Activity".localized()
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
        }
    }
    
    @objc fileprivate func refresh(sender:AnyObject) {
        self.loaderActive = false
        if self.activityWall {
            getActivityPosts()
        } else if self.messagesWall {
            getMessages()
        } else {
            getWallPosts(pulltorefresh: true)
        }
    }
    
    @objc fileprivate func openPeopleProfile(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let tempUserId = dict["userId"] as! String
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileReq().getMyProfileInfo(desiredProfileId: tempUserId, completionHandler: { (done, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr, photoStr) in
                SwiftLoader.hide()
                if done {
                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "PeopleDetailVC") as! PeopleDetailVC
                    openNewVC.userId = tempUserId
                    openNewVC.userFullName = name + " " + surname
                    openNewVC.interestStr = interestsArr.joined(separator: "")
                    openNewVC.contactStr = email
                    openNewVC.photoUser = photo
                    openNewVC.idCompany = photoStr
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(openNewVC, animated: true)
                    });
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @objc fileprivate func reloadTheWall(){
        reloadTheWallBool = true
    }
    
    @objc fileprivate func btnBack(){
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    fileprivate func getWallPosts(pulltorefresh: Bool){
        if Reachability.isConnectedToNetwork() {
            if loaderActive {
                SwiftLoader.show(animated: true)
            }
            if pulltorefresh {
                self.currentPage = 1
                self.totalPage = 1
            }
            HomeWallReq().getPosts(page: self.currentPage.description) { (done, currentPage, totalPage, dateArr, idArr, nameArr, tagArr, tagNameArr, textArr, postedByIdArr, postedByNameArr, postedByPhotoArr, attachmentPhotoArr, pinnedArr, canPinArr, canPemoveArr, myPostArr, attachmentComentsTextArr, attachmentComentsPhotoArr, attachmentComentsCreatedArr, attachmentComentsUserIdArr, activityIdArr) in
                if pulltorefresh {
                    self.idArr.removeAll()
                    self.dateArr.removeAll()
                    self.postedByIdArr.removeAll()
                    self.postedByNameArr.removeAll()
                    self.postedByPhotoArr.removeAll()
                    self.descArr.removeAll()
                    self.attachmentPhotoArr.removeAll()
                    self.comentsTextArr.removeAll()
                    self.comentsPhotoArr.removeAll()
                    self.tagArr.removeAll()
                    self.comentsCreatedArr.removeAll()
                    self.comentsUserIdArr.removeAll()
                    self.activityIdArr.removeAll()
                }
                self.loaderActive = true
                self.refreshControl!.endRefreshing()
                SwiftLoader.hide()
                self.currentPage = currentPage
                self.totalPage = totalPage
                self.idArr += idArr
                self.dateArr += dateArr
                self.postedByIdArr += postedByIdArr
                self.postedByNameArr += postedByNameArr
                self.postedByPhotoArr += postedByPhotoArr
                self.descArr += textArr
                self.attachmentPhotoArr += attachmentPhotoArr
                self.comentsTextArr += attachmentComentsTextArr
                self.comentsPhotoArr += attachmentComentsPhotoArr
                self.tagArr += tagNameArr
                self.comentsCreatedArr += attachmentComentsCreatedArr
                self.comentsUserIdArr += attachmentComentsUserIdArr
                self.activityIdArr += activityIdArr
                self.tableView.reloadData()
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func getActivityPosts(){
        if Reachability.isConnectedToNetwork() {
            if loaderActive {
                SwiftLoader.show(animated: true)
            }
            ActivitiesReq().getActivitiesWall(activityId: activityId) { (done, idArr, postedByIdArr, postedByNameArr, postedByPhotoArr, myPostArr, dateArr, textArr, comentsPhotoArr, attachmentComentsTextArr, attachmentComentsPhotoArr, attachmentComentsCreatedArr, attachmentComentsUserIdArr) in
                self.loaderActive = true
                self.refreshControl!.endRefreshing()
                SwiftLoader.hide()
                if done {
                    self.idArr = idArr
                    self.postedByIdArr = postedByIdArr
                    self.postedByNameArr = postedByNameArr
                    self.postedByPhotoArr = postedByPhotoArr
                    self.descArr = textArr
                    self.subtitleArr = dateArr
                    self.attachmentPhotoArr = comentsPhotoArr
                    self.comentsTextArr = attachmentComentsTextArr
                    self.comentsPhotoArr = attachmentComentsPhotoArr
                    self.comentsCreatedArr = attachmentComentsCreatedArr
                    self.comentsUserIdArr = attachmentComentsUserIdArr
                    self.tableView.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func getMessages(){
        if !conversationReceiverId.description.isEmpty {
            if Reachability.isConnectedToNetwork() {
                if loaderActive {
                    SwiftLoader.show(animated: true)
                }
                MsgsReq().getConversationsBetweenPersons(userId: conversationReceiverId.description) { (done, readArr, previewTextArr, receiverIdArr, receiverNameArr, receiverPhotoArr, dateArr, myPostArr, attachmentPhotoArr) in
                    self.loaderActive = true
                    self.refreshControl!.endRefreshing()
                    SwiftLoader.hide()
                    if done {
                        self.conversationReadArr = readArr.reversed()
                        self.conversationPreviewTextArr = previewTextArr.reversed()
                        self.conversationReceiverIdArr = receiverIdArr.reversed()
                        self.conversationReceiverNameArr = receiverNameArr.reversed()
                        self.conversationReceiverPhotoArr = receiverPhotoArr.reversed()
                        self.conversationDateArr = dateArr.reversed()
                        self.conversationMyPostArr = myPostArr.reversed()
                        self.attachmentPhotoArr = attachmentPhotoArr.reversed()
                        self.tableView.reloadData()
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                    }
                }
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
            }
        }
    }
    
    fileprivate func postComent(){
        if Reachability.isConnectedToNetwork() {
            if !msgField.text!.isEmpty || !photoString.isEmpty {
                if activityWall {
                    SwiftLoader.show(animated: true)
                    ActivitiesReq().createNewActivityWallPost(activityId: activityId, text: msgField.text!, photo: photoString, completionHandler: { (done) in
                        SwiftLoader.hide()
                        if done {
                            self.msgField.text = ""
                            self.tableView.setContentOffset(CGPoint.zero, animated: false)
                            DelayFunc.sharedInstance.delay(1.5, closure: {
                                self.getActivityPosts()
                            })
                        }
                    })
                } else if messagesWall {
                    SwiftLoader.show(animated: true)
                    MsgsReq().postNewMsg(userConversationId: conversationReceiverId, text: msgField.text!, photo:photoString, completionHandler: { (done) in
                        SwiftLoader.hide()
                        if done {
                            self.msgField.text = ""
                            self.tableView.setContentOffset(CGPoint.zero, animated: false)
                            DelayFunc.sharedInstance.delay(1.5, closure: {
                                self.getMessages()
                            })
                        }
                    })
                } else {
                    SwiftLoader.show(animated: true)
                    HomeWallReq().createNewWallPost(text: msgField.text!, photo: photoString) { (done) in
                        SwiftLoader.hide()
                        if done {
                            self.msgField.text = ""
                            self.tableView.setContentOffset(CGPoint.zero, animated: false)
                            DelayFunc.sharedInstance.delay(1.5, closure: {
                                self.getWallPosts(pulltorefresh: false)
                            })
                        }
                    }
                }
            } else {
                self.showSimpleAlert(title: "Error".localized(), message: "You cannot sent a blank message.".localized())
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @objc fileprivate func showSelectedPhoto(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let tempStr = dict["imgStr"] as! String
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "ImagePresenterVC") as! ImagePresenterVC
        modal.imgString = tempStr
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: false, completion: nil)
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.openLibrary), name: NSNotification.Name(rawValue: "openLibrary"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openCamera), name: NSNotification.Name(rawValue: "openCamera"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSelectedPhoto), name: NSNotification.Name(rawValue: "showSelectedPhoto"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTheWall), name: NSNotification.Name(rawValue: "reloadTheWall"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openPeopleProfile), name: NSNotification.Name(rawValue: "openPeopleProfile"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openActivityDetail), name: NSNotification.Name(rawValue: "openActivityDetail"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadWall), name: NSNotification.Name(rawValue: "loadWall"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openLibrary"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openCamera"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showSelectedPhoto"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadTheWall"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openPeopleProfile"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openActivityDetail"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "loadWall"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notificationForCurrentThreadCheck"), object: nil)
    }
    
    @IBOutlet weak var appTitle: UILabel!
    @IBOutlet weak var appLogo: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var msgField: UITextField!
    @IBOutlet weak var msgContainer: UIView!
    @IBOutlet weak var tableViewToBotConstant: NSLayoutConstraint!
    @IBOutlet weak var doneBtn: UIButton!
    
}

extension MsgsWallVC {
    
    @IBAction func pickPhotoBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "TypePickerVC") as! TypePickerVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
    
    @IBAction func closeKeyboardBtn(_ sender: Any) {
        view.endEditing(true)
    }
}

extension MsgsWallVC: UITableViewDataSource, UITableViewDelegate {
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "MsgsCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MsgsCellVC")
        let nib2 = UINib(nibName: "FromMeCell", bundle: nil)
        tableView.register(nib2, forCellReuseIdentifier: "FromMeCellVC")
        let nib3 = UINib(nibName: "NotFromMeCell", bundle: nil)
        tableView.register(nib3, forCellReuseIdentifier: "NotFromMeCellVC")
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 200
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if activityWall {
            return idArr.count
        } else if messagesWall {
            return conversationReceiverIdArr.count
        } else {
            return idArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reload"), object: nil)
        if !messagesWall {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "MsgsCellVC") as! MsgsCellVC
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.titleLabel.text = postedByNameArr[indexPath.row]
            cell.descriptionTextView.text = descArr[indexPath.row]
            
            //attachment display photo
            let temp = attachmentPhotoArr[indexPath.row]
            if temp != [String]() {
                cell.imgArr = temp
                UIView.animate(withDuration: 0.1, animations: {
                    cell.imgIconHeight.constant = self.view.frame.size.width/2-85
                }, completion: nil)
            } else {
                cell.imgArr = [String]()
                UIView.animate(withDuration: 0.1, animations: {
                    cell.imgIconHeight.constant = 0
                }, completion: nil)
            }
            
            //údaje toho kdo to přidal na zeď
            cell.userId = postedByIdArr[indexPath.row]
            if !postedByPhotoArr[indexPath.row].isEmpty {
                let tempurl = URL(string: postedByPhotoArr[indexPath.row])
                cell.imgIcon.hnk_setImage(from: tempurl)
            } else {
                cell.imgIcon.backgroundColor = UIColor.clear
                cell.imgIcon.image = UIImage(named: "placeholder_photo")
            }
            
            cell.activityCell = true
            if activityWall {
                //nacházíme se v aktivitě
                cell.msgsCountLabel.text = String(comentsTextArr[indexPath.row].count)
                cell.inboxMsgIcon.isHidden = true
                cell.detailId = idArr[indexPath.row]
                cell.activityCell = true
                cell.subtitleLabel.text = "posted a new comment ".localized() + DateConvertor.sharedInstance.timeAgoSinceDate(date: subtitleArr[indexPath.row], numericDates: true)
                cell.blueContainerTitleLabel.text = "Activity".localized()
                cell.blueContainerView.backgroundColor = UIColor(red: 92/255.0, green: 151/255.0, blue: 71/255.0, alpha: 1.0)
            } else {
                //nacházíme se na hlavní zdi
                if tagArr[indexPath.row].lowercased() != "activity" {
                    //tohle je od Admina
                    cell.inboxMsgIcon.isHidden = false
                    cell.msgsCountLabel.isHidden = true
                    cell.msgsCountIcon.isHidden = true
                    cell.activityCell = false
                } else {
                    //tohle k aktivitě
                    cell.inboxMsgIcon.isHidden = true
                    cell.msgsCountLabel.isHidden = false
                    cell.msgsCountIcon.isHidden = false
                    cell.msgsCountLabel.text = comentsTextArr[indexPath.row].count.description
                    cell.detailId = activityIdArr[indexPath.row]
                }
                // kdy se to vytvořilo a jaký je to typ ohlášení
                cell.subtitleLabel.text = "posted a new comment ".localized() + dateArr[indexPath.row]
                cell.blueContainerTitleLabel.text = tagArr[indexPath.row].localized()
                cell.blueContainerView.isHidden = false
                switch tagArr[indexPath.row] {
                case "Concierge":
                    cell.blueContainerView.backgroundColor = UIColor(red: 174/255.0, green: 86/255.0, blue: 154/255.0, alpha: 1.0)
                case "Office Life":
                    cell.blueContainerView.backgroundColor = UIColor(red: 174/255.0, green: 86/255.0, blue: 154/255.0, alpha: 1.0)
                case "ACTIVITY":
                    cell.blueContainerView.backgroundColor = UIColor(red: 92/255.0, green: 151/255.0, blue: 71/255.0, alpha: 1.0)
                default:
                    break
                }
            }
            
            //Stránkování
            var tempCount: Int = 0
            if activityWall {
                tempCount = postedByNameArr.count
            } else {
                tempCount = idArr.count
            }
            if indexPath.row == tempCount-1 {
                if self.currentPage < self.totalPage {
                    self.currentPage += 1
                    if activityWall {
                        getActivityPosts()
                    } else {
                        getWallPosts(pulltorefresh: false)
                    }
                }
            }
            return cell
        } else {
            if conversationMyPostArr[indexPath.row] {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "FromMeCellVC") as! FromMeCellVC
                cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.userId = Session.sharedInstance.userToken
                cell.titleLabel.text = conversationReceiverNameArr[indexPath.row]
                cell.descriptionTextView.text = conversationPreviewTextArr[indexPath.row]
                cell.subtitleLabel.text = "posted a new comment ".localized() + conversationDateArr[indexPath.row]
                if !conversationReceiverPhotoArr[indexPath.row].isEmpty {
                    let tempurl = URL(string: conversationReceiverPhotoArr[indexPath.row])
                    cell.imgIcon.hnk_setImage(from: tempurl)
                } else {
                    cell.imgIcon.backgroundColor = UIColor.clear
                    cell.imgIcon.image = UIImage(named: "placeholder_photo")
                }
                let temp = attachmentPhotoArr[indexPath.row]
                if temp != [String]() {
                    cell.imgArr = temp
                    UIView.animate(withDuration: 0.1, animations: {
                        cell.imgIconHeight.constant = self.view.frame.size.width/2-85
                    }, completion: nil)
                } else {
                    cell.imgArr = [String]()
                    UIView.animate(withDuration: 0.1, animations: {
                        cell.imgIconHeight.constant = 0
                    }, completion: nil)
                }
                return cell
            } else {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "NotFromMeCellVC") as! NotFromMeCellVC
                cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.userId = conversationReceiverIdArr[indexPath.row]
                cell.titleLabel.text = conversationReceiverNameArr[indexPath.row]
                cell.descriptionTextView.text = conversationPreviewTextArr[indexPath.row]
                cell.subtitleLabel.text = "posted a new comment ".localized() + conversationDateArr[indexPath.row]
                if !conversationReceiverPhotoArr[indexPath.row].isEmpty {
                    let tempurl = URL(string: conversationReceiverPhotoArr[indexPath.row])
                    cell.imgIcon.hnk_setImage(from: tempurl)
                } else {
                    cell.imgIcon.backgroundColor = UIColor.clear
                    cell.imgIcon.image = UIImage(named: "placeholder_photo")
                }
                let temp = attachmentPhotoArr[indexPath.row]
                if temp != [String]() {
                    cell.imgArr = temp
                    UIView.animate(withDuration: 0.1, animations: {
                        cell.imgIconHeight.constant = self.view.frame.size.width/2-85
                    }, completion: nil)
                } else {
                    cell.imgArr = [String]()
                    UIView.animate(withDuration: 0.1, animations: {
                        cell.imgIconHeight.constant = 0
                    }, completion: nil)
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !activityWall && !messagesWall {
            if !postedByIdArr[indexPath.row].isEmpty && tagArr[indexPath.row].lowercased() == "activity" {
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ComentsToWallPostVC") as! ComentsToWallPostVC
                openNewVC.comentNameArr = comentsTextArr[indexPath.row]
                openNewVC.comentPhotoArr = comentsPhotoArr[indexPath.row]
                openNewVC.postId = idArr[indexPath.row]
                openNewVC.comentCreatedArr = comentsCreatedArr[indexPath.row]
                openNewVC.comentUserIdArr = comentsUserIdArr[indexPath.row]
                if activityWall {
                    openNewVC.activityId = activityId
                }
                self.view.endEditing(true)
                self.tableView.deselectRow(at: indexPath, animated: false)
                
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            } else if !postedByIdArr[indexPath.row].isEmpty && Session.sharedInstance.userToken != postedByIdArr[indexPath.row] {
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
                openNewVC.conversationReceiverId = postedByIdArr[indexPath.row]
                openNewVC.messagesWall = true
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        } else if activityWall {
            if !postedByIdArr[indexPath.row].isEmpty {
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ComentsToWallPostVC") as! ComentsToWallPostVC
                openNewVC.comentNameArr = comentsTextArr[indexPath.row]
                openNewVC.comentPhotoArr = comentsPhotoArr[indexPath.row]
                openNewVC.postId = idArr[indexPath.row]
                openNewVC.comentCreatedArr = comentsCreatedArr[indexPath.row]
                openNewVC.comentUserIdArr = comentsUserIdArr[indexPath.row]
                if activityWall {
                    openNewVC.activityId = activityId
                }
                self.view.endEditing(true)
                self.tableView.deselectRow(at: indexPath, animated: false)
                
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            } else if !postedByIdArr[indexPath.row].isEmpty && Session.sharedInstance.userToken != postedByIdArr[indexPath.row] {
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
                openNewVC.conversationReceiverId = postedByIdArr[indexPath.row]
                openNewVC.messagesWall = true
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        }
    }
}

extension MsgsWallVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc fileprivate func openLibrary(){
        DelayFunc.sharedInstance.delay(0.3) {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                self.imagePicker.navigationBar.isTranslucent = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    @objc fileprivate func openCamera(){
        DelayFunc.sharedInstance.delay(0.3) {
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.navigationBar.isTranslucent = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        let img = info[UIImagePickerControllerOriginalImage] as! UIImage
        photoString.append((img.resizeWithWidth(600)?.toBase64())!)
        showSimpleAlert(title: "Success", message: "Image added to message and waiting for send".localized())
    }
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: nil)
        photoString.append(image.resizeWithWidth(600)!.toBase64())
        showSimpleAlert(title: "Success", message: "Image added to message and waiting for send".localized())
    }
}

extension MsgsWallVC: UITextFieldDelegate {
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        postComent()
        return true
    }
}

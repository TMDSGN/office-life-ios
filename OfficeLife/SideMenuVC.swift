//
//  SideMenuVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 15.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import iOS_Slide_Menu
import SwiftLoader
import Haneke
import Localize_Swift

class SideMenuVC: UIViewController {
    
    var tableState: Int = 0
    var selectedRow: Int = 0
    
    fileprivate var mainTitleArr = ["Home".localized(), "My building".localized(), "Community".localized(), "Services".localized(), "Messages".localized(), "Log out".localized()]
    fileprivate let mainImgArr = ["home", "myBuilding", "comunity", "services", "msg", "logOut"]
    
    fileprivate let myBuildingTitleArr = ["My building".localized(), "Booking".localized(), "Meet the Team".localized(), "Directory".localized(), "About Palác Křižík".localized()]
    fileprivate let myBuildingSubtitleArr = [String(), "Get the advantage of sharing".localized(), "Who cares about your daily comfort".localized(), "All tenants in one place".localized(), "Discover the building from new perspective".localized()]
    
    fileprivate let communityTitleArr = ["Community".localized(), "Activities".localized(), "People".localized()]
    fileprivate let communitySubtitleArr = [String(), "All featured activities".localized(), "Meet the community around you".localized()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegister()
        setupScene()
        registerObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupScene()
    }
    
    @objc private func setupScene(){
        self.photoView.layer.cornerRadius = 30
        self.photoView.layer.masksToBounds = true
        self.photoIcon.layer.cornerRadius = 28
        self.photoIcon.layer.masksToBounds = true
        self.nameTitleLabel.text = Session.sharedInstance.profileFirstName + " " + Session.sharedInstance.profileSurName
        if !Session.sharedInstance.profilePhoto.isEmpty {
            let tempurl = URL(string: Session.sharedInstance.profilePhoto)
            self.photoIcon.hnk_setImage(from: tempurl)
            self.photoIcon.layer.cornerRadius = 28
            self.photoIcon.layer.masksToBounds = true
        } else {
            self.photoIcon.backgroundColor = UIColor.backgroundPhotoColor
            self.photoIcon.image = UIImage(named:"placeholder_photo")
        }
    }
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "CellWithIconView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CellWithIconVC")
        let nib2 = UINib(nibName: "CellWithSubtitleView", bundle: nil)
        tableView.register(nib2, forCellReuseIdentifier: "CellWithSubtitleVC")
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
    }
    
    fileprivate func prepareToPopToNewContainer(_ nameOfVc:String){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: nameOfVc)
        DispatchQueue.main.async(execute: { () -> Void in
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        });
    }
    
    fileprivate func reloadTable(){
        UIView.transition(with: tableView,
                          duration: 0.35,
                          options: .transitionCrossDissolve,
                          animations:
            { () -> Void in
                self.tableView.reloadData()
        },
                          completion: nil);
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupScene), name: NSNotification.Name(rawValue: "refreshHamburgerMenu"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "refreshHamburgerMenu"), object: nil)
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var nameTitleLabel: UILabel!
    @IBOutlet weak var photoIcon: UIImageView!
}

extension SideMenuVC {

    @IBAction func openProfile(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC")
        DispatchQueue.main.async(execute: { () -> Void in
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        });
    }
}

extension SideMenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableState {
        case 1:
            return myBuildingTitleArr.count
        case 2:
            return communityTitleArr.count
        default:
            return mainTitleArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cellWithIcon = self.tableView.dequeueReusableCell(withIdentifier: "CellWithIconVC") as! CellWithIconVC
        let cellWithSubtitle = self.tableView.dequeueReusableCell(withIdentifier: "CellWithSubtitleVC") as! CellWithSubtitleVC
        cellWithIcon.selectionStyle = UITableViewCellSelectionStyle.none
        cellWithIcon.selectionStyle = UITableViewCellSelectionStyle.none
        
        switch tableState {
        case 1:
            if indexPath.row == 0 {
                cellWithIcon.titleLabel.text = myBuildingTitleArr[indexPath.row]
                cellWithIcon.leftImg.image = UIImage(named: "small_arrow_back")
                return cellWithIcon
            } else {
                cellWithSubtitle.titleLabel.text = myBuildingTitleArr[indexPath.row]
                cellWithSubtitle.subTitleLabel.text = myBuildingSubtitleArr[indexPath.row]
                return cellWithSubtitle
            }
        case 2:
            if indexPath.row == 0 {
                cellWithIcon.titleLabel.text = communityTitleArr[indexPath.row]
                cellWithIcon.leftImg.image = UIImage(named: "small_arrow_back")
                return cellWithIcon
            } else {
                cellWithSubtitle.titleLabel.text = communityTitleArr[indexPath.row]
                cellWithSubtitle.subTitleLabel.text = communitySubtitleArr[indexPath.row]
                return cellWithSubtitle
            }
        default:
            cellWithIcon.titleLabel.text = mainTitleArr[indexPath.row]//
            cellWithIcon.leftImg.image = UIImage(named: mainImgArr[indexPath.row])
            cellWithIcon.blueView.isHidden = true
            cellWithIcon.rightImg.isHidden = true
            if indexPath.row == 1 || indexPath.row == 2 {
                cellWithIcon.rightImg.isHidden = false
            }
            if indexPath.row == selectedRow {
                cellWithIcon.blueView.isHidden = false
            } else {
                cellWithIcon.blueView.isHidden = true
            }
            return cellWithIcon
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 && indexPath.row < 3 && tableState == 0 {
            tableState = indexPath.row
            reloadTable()
        } else {
            if tableState == 0 {
                switch indexPath.row {
                case 0:
                    prepareToPopToNewContainer("HomeVC")
                case 3:
                    prepareToPopToNewContainer("ServiceTypeVC")
                case 4:
                    prepareToPopToNewContainer("DirectMsgsTableVC")
                case 5:
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "logOut"), object: nil)
                default:
                    break
                }
            }
            if tableState == 1 {
                switch indexPath.row {
                case 1:
                    prepareToPopToNewContainer("ReservationTableVC")
                case 2:
                    prepareToPopToNewContainer("ManagementCVC")
                case 3:
                    prepareToPopToNewContainer("OccupiersCVC")
                case 4:
                    prepareToPopToNewContainer("AboutBuildingVC")
                default:
                    break
                }
            }
            if tableState == 2 {
                switch indexPath.row {
                case 1:
                    prepareToPopToNewContainer("ActivityTableVC")
                case 2:
                    prepareToPopToNewContainer("PeopleContainerVC")
                default:
                    break
                }
            }
        }
        
        if tableState == 0  {
            selectedRow = indexPath.row
            reloadTable()
        }
        
        if indexPath.row == 0 && tableState != 0 {
            tableState = indexPath.row
            reloadTable()
        }
        
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableState == 0 {
            return 70
        } else {
            return 85
        }
    }
}

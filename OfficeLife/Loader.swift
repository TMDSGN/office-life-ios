//
//  Loader.swift
//  KZP na cesty
//
//  Created by Josef Antoni on 16.02.16.
//  Copyright © 2016 Pixelmate. All rights reserved.
//

import Foundation
import SwiftLoader
import UIKit

class Loader {
    
    /*
     *   Setup loader
     */
    func configLoader(){
        var config : SwiftLoader.Config = SwiftLoader.Config()
        config.size = 100
        config.spinnerColor = UIColor.mainBlue
        config.spinnerLineWidth = 1.5
        //pozadí
        config.backgroundColor = UIColor.clear
        config.foregroundColor = UIColor.black
        config.foregroundAlpha = 0.0
        config.cornerRadius = 50
        SwiftLoader.setConfig(config)
    }
}

//
//  DatePickerVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 24.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class DatePickerVC: UIViewController {
    
    var dateAndTimeActive = Bool()
    var dateFormat: String = ""
    var dateFrom = Bool()
    var editActive = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainContainer.alpha = 0
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        datePickerConfig()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            self.mainContainer.alpha = 1
        }) { (done) in
        }
    }
    
    fileprivate func closeView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.mainContainer.alpha = 0
        }) { (done) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    private func datePickerConfig(){
        datePicker.setValue(UIColor.black, forKeyPath: "textColor")
        datePicker.datePickerMode = UIDatePickerMode.date
        if dateAndTimeActive {
            datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        }
        // datePicker.locale = Locale(identifier: "cs")
        datePicker.subviews[0].subviews[1].isHidden = true
        datePicker.subviews[0].subviews[2].isHidden = true
    }
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet var mainContainer: UIView!
    @IBOutlet weak var outOfContainer: UIButton!
}

extension DatePickerVC {
    
    @IBAction func cancelView(_ sender: Any) {
        closeView()
    }
    
    @IBAction func pickDateBtn(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        if !dateFormat.isEmpty {
            dateFormatter.dateFormat = dateFormat
        }
        dateFormatter.timeZone = TimeZone.current
        if !editActive {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateDate"), object: ["dateStr" : dateFormatter.string(from: datePicker.date)])
        } else {
            if dateFrom {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateDate"), object: ["dateStr" : dateFormatter.string(from: datePicker.date)])
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateDate"), object: ["dateStrTo" : dateFormatter.string(from: datePicker.date)])
            }
        }
        closeView()
    }
    
    @IBAction func outOfContainerBtn(_ sender: Any) {
        closeView()
    }
}

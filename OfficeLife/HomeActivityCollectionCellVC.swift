//
//  HomeCollectionCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 14.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

class HomeActivityCollectionCellVC: UICollectionViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

}

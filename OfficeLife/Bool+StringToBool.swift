//
//  Bool+StringToBool.swift
//  OfficeLife
//
//  Created by Josef Antoni on 26.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation

extension String {
    func toBool() -> Bool {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return false
        }
    }
}

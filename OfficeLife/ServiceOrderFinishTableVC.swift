//
//  ServiceOrderFinishTableVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 03.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import KMPlaceholderTextView
import DropDown
import SwiftLoader

class ServiceOrderFinishTableVC: UIViewController {
    
    var idStr:String!
    var index: String = ""
    fileprivate let dropDown = DropDown()
    fileprivate var selectedCompanyId: String = ""
    fileprivate var isSwitcherOn = false
    
    var titleStr: String = ""
    var totalPrice: String = ""
    var serviceId: String = ""
    var numberOfItems: String = ""
    var pricePerProduct: String = ""
    var nameOfProduct: String = ""
    var accordingGateway = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        customCellRegister()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let s = Session.sharedInstance
        if idStr == "1" {
            nameDescView.isHidden = false
            tableDescView.isHidden = true
            if !s.serviceOrderEmail.isEmpty {
                emailTxtField.text! = s.serviceOrderEmail
            }
            if !s.serviceOrderNumber.isEmpty {
                phoneTxtField.text! = s.serviceOrderNumber
            }
            descTxtView.text! = s.serviceOrderDesc
            //pro objednávání pro firmu
            companyNameLabel.text = s.serviceOrderCompanyName
            companyAddressLabel.text = s.serviceOrderCompanyAddress
            companyIcoLabel.text = s.serviceOrderCompanyBid
            companyDicLabel.text = s.serviceOrderCompanyVat
            companyPscLabel.text = s.serviceOrderCompanyZip
        } else {
            nameDescView.isHidden = true
            tableDescView.isHidden = false
        }
        setupDropDown()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if idStr == "1" {
            let s = Session.sharedInstance
            s.serviceOrderIsCompanyOrder = switcherDot.alpha == 1
            s.serviceOrderEmail = emailTxtField.text!
            s.serviceOrderNumber = phoneTxtField.text!
            s.serviceOrderDesc = descTxtView.text!
            s.serviceOrderCompanyName = companyNameLabel.text!
            s.serviceOrderCompanyAddress = companyAddressLabel.text!
            s.serviceOrderCompanyBid = companyIcoLabel.text!
            s.serviceOrderCompanyVat = companyDicLabel.text!
            s.serviceOrderCompanyZip = companyPscLabel.text!
        }
    }
    
    fileprivate func setupDropDown(){
        self.dropDown.textFont = UIFont.systemFont(ofSize: 11)
        self.dropDown.backgroundColor = .white
    }
    
    fileprivate func setupViews(){
        let tempColor = UIColor.lightGray

        self.emailView.layer.borderWidth = 1
        self.emailView.layer.borderColor = tempColor.cgColor
        
        self.phoneView.layer.borderWidth = 1
        self.phoneView.layer.borderColor = tempColor.cgColor
        
        self.descView.layer.borderWidth = 1
        self.descView.layer.borderColor = tempColor.cgColor
        
        self.companyNameContainer.layer.borderWidth = 1
        self.companyNameContainer.layer.borderColor = tempColor.cgColor

        self.companyAddressContainer.layer.borderWidth = 1
        self.companyAddressContainer.layer.borderColor = tempColor.cgColor

        self.companyIcoContainer.layer.borderWidth = 1
        self.companyIcoContainer.layer.borderColor = tempColor.cgColor

        self.companyDicContainer.layer.borderWidth = 1
        self.companyDicContainer.layer.borderColor = tempColor.cgColor
        
        self.companyPscContainer.layer.borderWidth = 1
        self.companyPscContainer.layer.borderColor = tempColor.cgColor

        self.switcherContainer.layer.borderWidth = 0.5
        self.switcherContainer.layer.borderColor = UIColor.mainBlue.cgColor
        self.switcherContainer.layer.cornerRadius = 10
        
        self.switcherDot.backgroundColor = UIColor.mainBlue
        self.switcherDot.layer.cornerRadius = 8
        self.switcherDot.alpha = 0

        self.companyContainer.alpha = 0
        
        orderAsCompanyLabel.text = "I want to order as company".localized()
        checkoutLabel.text = "Checkout".localized()
        productSubTitle.text = "Product".localized()
        priceTogetherLabel.text = "Total price".localized()
        cashRegisterLabel.text = "Checkout".localized()
        phoneTxtField.placeholder = "Phone".localized()
        descTxtView.placeholder = "Order notes".localized()
        
        companyNameLabel.placeholder = "Company name".localized()
        companyAddressLabel.placeholder = "Company address".localized()
        companyIcoLabel.placeholder = "Company bid".localized()
        companyDicLabel.placeholder = "Company vat".localized()
        companyPscLabel.placeholder = "Company zip".localized()
        emailTxtField.text = Session.sharedInstance.profileEmail
        phoneTxtField.text = Session.sharedInstance.profilePhone
    }
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "MyOrderCellDetailView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MyOrderCellDetailVC")
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
    }
    
    @IBOutlet weak var phoneTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var nameDescView: UIView!
    @IBOutlet weak var tableDescView: UIView!
    @IBOutlet weak var companyContainer: UIView!
    @IBOutlet weak var companyNameContainer: UIView!
    @IBOutlet weak var companyAddressContainer: UIView!
    @IBOutlet weak var companyIcoContainer: UIView!
    @IBOutlet weak var companyDicContainer: UIView!
    @IBOutlet weak var companyNameLabel: UITextField!
    @IBOutlet weak var companyAddressLabel: UITextField!
    @IBOutlet weak var companyIcoLabel: UITextField!
    @IBOutlet weak var companyDicLabel: UITextField!
    @IBOutlet weak var productSubTitle: UILabel!
    @IBOutlet weak var companyPscLabel: UITextField!
    @IBOutlet weak var companyPscContainer: UIView!
    @IBOutlet weak var priceTogetherLabel: UILabel!
    @IBOutlet weak var cashRegisterLabel: UILabel!
    @IBOutlet weak var descTxtView: KMPlaceholderTextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var switcherContainer: UIView!
    @IBOutlet weak var switcherDot: UIView!
    @IBOutlet weak var checkoutLabel: UILabel!
    @IBOutlet weak var orderAsCompanyLabel: UILabel!
}

extension ServiceOrderFinishTableVC {

    @IBAction func dropDownBtn(_ sender: Any) {
        if dropDown.isHidden {
            dropDown.show()
        }
        self.dropDown.selectionAction = { [unowned self] (index, item) in
            //   self.companyTxtField.text = item
            Session.sharedInstance.orderPaymentMethod = item
            self.tableView.reloadData()
        }
    }
    
    @IBAction func switcherPressedBtn(_ sender: Any) {
        if isSwitcherOn {
            isSwitcherOn = false
            UIView.animate(withDuration: 1, animations: {
                self.switcherDot.alpha = 0
                self.companyContainer.alpha = 0
            })
        } else {
            isSwitcherOn = true
            UIView.animate(withDuration: 1, animations: {
                self.switcherDot.alpha = 1
                self.companyContainer.alpha = 1
            })
        }
    }
}

extension ServiceOrderFinishTableVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MyOrderCellDetailVC") as! MyOrderCellDetailVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.dropdownImg.isHidden = true
        cell.dropDownPriceLabel.isHidden = true
        if indexPath.row == 0 {
            cell.titleLabel.text = nameOfProduct
            cell.priceLabel.text = numberOfItems + "x " + pricePerProduct
            cell.priceLabel.textAlignment = .right
            if pricePerProduct == "according to order" {
                cell.priceLabel.text = pricePerProduct.localized()
                cell.priceLabel.textAlignment = .center
            }
        }
        if indexPath.row == 1{
            cell.dropdownImg.isHidden = false
            cell.dropDownPriceLabel.isHidden = false
            cell.titleLabel.text = "Payment method".localized()
            cell.dropDownPriceLabel.text = Session.sharedInstance.orderPaymentMethod
            self.dropDown.anchorView = cell
            self.dropDown.direction = .bottom
            self.dropDown.bottomOffset = CGPoint(x: 0, y:cell.bounds.height)
            if accordingGateway {
                self.dropDown.dataSource = ["Credit".localized(), "Gopay", "According Payment".localized()]
            } else {
                self.dropDown.dataSource = ["Credit".localized(), "Gopay"]
            }
        }
        if indexPath.row == 2 {
            cell.titleLabel.text = "Total price".localized()
            cell.priceLabel.text = totalPrice
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1{
            if dropDown.isHidden {
                dropDown.show()
            }
            self.dropDown.selectionAction = { [unowned self] (index, item) in
                Session.sharedInstance.orderPaymentMethodServer = ["mycred", "gopayredirbinder", "according_gateway"][index]
                Session.sharedInstance.orderPaymentMethod = item
                self.tableView.reloadData()
            }
        }
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}

//
//  LoginReq.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Localize_Swift

class LoginReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getBuilding(completionHandler: @escaping (Bool, [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/buildings"
        
        Alamofire.request(url, method: .get).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                
                var idArr = [String]()
                var nameArr = [String]()
                
                for i in 0..<json.count {
                    let id = json[i]["id"].stringValue
                    let name = json[i]["name"].stringValue
                    idArr.append(id)
                    nameArr.append(name)
                }
                
                completionHandler(true, idArr, nameArr)
            } else {
                completionHandler(false, [String](), [String]())
            }
        }
    }
    
    func getCompanies(completionHandler: @escaping (Bool, [String], [String], [String], [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/companies/v1/list"
        let lang = Localize.currentLanguage()
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "Language" : lang
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                var idArr = [String]()
                var nameArr = [String]()
                var iconsArr = [String]()
                var descArr = [String]()
                var contactNameArr = [String]()
                var contactEmailArr = [String]()
                var contactPhotoArr = [String]()
                
                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let name = json[i]["name"].stringValue.htmlDecoded.replacingOccurrences(of: "L&#8217;", with: "'")
                    let icon = json[i]["icon"].stringValue
                    let description = json[i]["description"].stringValue.htmlDecoded
                    let contactName = json[i]["contact"]["name"].stringValue.htmlDecoded.replacingOccurrences(of: "L&#8217;", with: "'")
                    let contactEmail = json[i]["contact"]["email"].stringValue
                    let contactPhoto = json[i]["contact"]["photo"].stringValue
                    
                    idArr.append(id)
                    nameArr.append(name)
                    iconsArr.append(icon)
                    descArr.append(description)
                    contactNameArr.append(contactName)
                    contactEmailArr.append(contactEmail)
                    contactPhotoArr.append(contactPhoto)
                }
                completionHandler(true, idArr, nameArr, iconsArr, descArr, contactNameArr, contactEmailArr, contactPhotoArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String](), [String](), [String]())
            }
        }
    }
    
    func simpleLogin(email:String, pass:String, completionHandler: @escaping (Bool, String, Bool) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/authenticate"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "email" : email,
            "password" : pass
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                let data = response.result.value
                let json = JSON(data!)
                let token = json["user_id"].stringValue
                let confirmed = json["confirmed"].boolValue
                if !token.isEmpty {
                    completionHandler(true, token, confirmed)
                } else {
                    completionHandler(false, String(), Bool())
                }
            } else if response.response?.statusCode == 401 {
                if let data = response.result.value {
                    let json = JSON(data)
                    if !json["errors"]["invalid_username"][0].stringValue.isEmpty {
                        completionHandler(true, String(), Bool())
                    } else if !json["errors"]["incorrect_password"][0].stringValue.isEmpty {
                        completionHandler(true, String(), Bool())
                    } else {
                        completionHandler(false, String(), Bool())
                    }
                } else {
                    completionHandler(false, String(), Bool())
                }
            } else {
                completionHandler(false, String(), Bool())
            }
        }
    }
    
    func getFacebookLogin( token:String, expiration:String, completionHandler: @escaping (Bool, String) -> ()) -> (){
        
        let url = urlDomain + "/facebook/v1/authenticate"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "facebook_token" : token,
            "expires" : expiration
        ]
        
        Alamofire.request( url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                let data = response.result.value
                let json = JSON(data!)
                let token = json["user_id"].stringValue
                if !token.isEmpty {
                    completionHandler(true, token)
                } else {
                    completionHandler(false, String())
                }
            } else {
                completionHandler(false, String())
            }
        }
    }
    
    func getLinkedinLogin( token:String, expiration:Double, completionHandler: @escaping (Bool, String) -> ()) -> (){
        
        let url = urlDomain + "/linkedin/v1/authenticate"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "linkedin_token" : token,
            "expires" : expiration,
            "ios" : true
            ] as [String : Any]

        Alamofire.request( url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                let data = response.result.value
                let json = JSON(data!)
                let token = json["user_id"].stringValue
                if !token.isEmpty {
                    completionHandler(true, token)
                } else {
                    completionHandler(false, String())
                }
            } else {
                completionHandler(false, String())
            }
        }
    }
    
    func lostPass(email:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/login/v1/forgotten_password"
        
        let para = [
            "email" : email
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default).responseJSON { response in
            if response.response?.statusCode == 201 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    
    func registration(buildingCode: String, email:String, username:String, password:String, firstName:String, lastName:String, completionHandler: @escaping (Bool, Bool) -> ()) -> (){
        
        let url = urlDomain + "/wp/v2/users"
        
        let header = [
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "email" : email,
            "username": username,
            "password": password,
            "first_name": firstName,
            "last_name": lastName,
            "roles": [
                "customer"
            ],
            "building_code": buildingCode
            ] as [String : Any]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                completionHandler(true, false)
            } else if response.response?.statusCode == 401 { //špatný kod budovy
                completionHandler(false, true)
            } else {
                completionHandler(false, false)
            }
        }
    }
    
    func checkIfUserEnteredBuildingCode(completionHandler: @escaping (Bool, Bool, String) -> ()) -> (){
        
        let url = urlDomain + "/buildings/v1/my_building"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                let buildingId = json["building_id"].stringValue
                if !buildingId.isEmpty {
                    completionHandler(true, true, String())
                } else {
                    completionHandler(true, false, String())
                }
            } else {
                completionHandler(false, false, String())
            }
        }
    }
    
    func registerUserBuildingCode(buildingSecretCode: String, completionHandler: @escaping (Bool, String) -> ()) -> (){
        
        let url = urlDomain + "/buildings/v1/code"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "code": buildingSecretCode
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                let buildingId = json["building_id"].stringValue
                completionHandler(true, buildingId)
            } else {
                completionHandler(false, String())
            }
        }
    }
    
    func checkIfUserSeenWelcomeScreen(completionHandler: @escaping (Bool, Bool) -> ()) -> (){
        
        let url = urlDomain + "/helpers/v1/evil_hacker_lind"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                let read = json["read"].stringValue.toBool()
                if read {
                    completionHandler(true, true)
                } else {
                    completionHandler(true, false)
                }
            } else {
                completionHandler(false, false)
            }
        }
    }
    
    func notifyServerThatUserSeenWelcomeScreen(completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/helpers/v1/evil_hacker_lind"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization": "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "read": true
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 204 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
}

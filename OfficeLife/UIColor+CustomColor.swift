//
//  Colors.swift
//  OfficeLife
//
//  Created by Josef Antoni on 04.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

extension UIColor {
        
    class var navBarColor: UIColor {
        return UIColor(red: 40.0/255, green: 40.0/255, blue: 46.0/255, alpha: 1)
    }
    class var lightGray: UIColor {
        return UIColor(red: 215.0/255, green: 215.0/255, blue: 215.0/255, alpha: 1)
    }
    class var mainBlue: UIColor {
        return UIColor(red: 123.0/255, green: 175.0/255, blue: 225.0/255, alpha: 1)
    }
    class var imgBorderColor: UIColor {
        return UIColor(red: 59.0/255, green: 60.0/255, blue: 64.0/255, alpha: 1)
    }
    class var backgroundPhotoColor: UIColor {
        return UIColor(red: 215.0/255, green: 215.0/255, blue: 215.0/255, alpha: 1)
    }
    //calendar
    class var cellOutOfMonth: UIColor {
        return UIColor(red: 203.0/255, green: 203.0/255, blue: 203.0/255, alpha: 1)
    }
    class var cellBg: UIColor {
        return UIColor(red: 251.0/255, green: 251.0/255, blue: 251.0/255, alpha: 1)
    }

}

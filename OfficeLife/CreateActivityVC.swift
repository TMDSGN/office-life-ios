//
//  CreateActivityVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 04.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import DropDown
import KMPlaceholderTextView
import GooglePlaces
import SwiftLoader
import Haneke

class CustomUITextField: UITextField {
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(paste(_:)) {
            return false
        }
        return true
    }
}

class CreateActivityVC: UIViewController, AlertMessage {
    
    var placesClient: GMSPlacesClient!
    fileprivate let dropDown = DropDown()
    fileprivate var imagePicker = UIImagePickerController()
    fileprivate var categoryArr = ["Sport".localized(), "Networking".localized(), "Carpool".localized(), "Culture".localized(), "Fun".localized(), "Other".localized()]
    fileprivate var categoryIdArr = ["19", "20", "22", "28", "29", "45"]
    var selectedCategory: Int = 0
    fileprivate var photoString: String = ""
    fileprivate var switcherActivityForPublicIsOn = false
    fileprivate var startAddress = false
    fileprivate var addressLatStr: String = ""
    fileprivate var addressLngStr: String = ""
    fileprivate var destinationaddressLatStr: String = ""
    fileprivate var destinationaddressLngStr: String = ""
    
    //Edit
    var canEdit = false
    var idEdit: String = ""
    var nameEdit: String = ""
    var dateEdit = Double()
    var dateToEdit = Double()
    var descEdit: String = ""
    var placeEdit: String = ""
    var onlyVisibleToPeopleFromMyCompanyEdit = Bool()
    var photoEdit: URL?
    var categoryEdit: String = ""
    var latitudeEdit: String = ""
    var longitudeEdit: String = ""
    var destinationEdit: String = ""
    var destinationLatitudeEdit: String = ""
    var destinationLongitudeEdit: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        registerObservers()
        placesClient = GMSPlacesClient.shared()
        if canEdit {
            setupEditView()
        } else {
            self.switcherDot.alpha = 0
        }
        UIView.animate(withDuration: 0.1) {
            self.destinationAdressView.alpha = 0
            self.destinationConstant.constant = 20
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDropDown()
        self.switcherContainer.layer.borderWidth = 0.5
        self.switcherContainer.layer.borderColor = UIColor.mainBlue.cgColor
        self.switcherContainer.layer.cornerRadius = 10
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.containerView.alpha = 1
        }
    }
    
    fileprivate func setupEditView(){
        activityTitletxtField.text! = nameEdit
        descTxtView.text! = descEdit
        adressTxtField.text! = placeEdit
        addressLatStr = latitudeEdit
        addressLngStr = longitudeEdit
        destinationAddressField.text = descEdit
        destinationaddressLatStr = destinationLatitudeEdit
        destinationaddressLngStr = destinationLongitudeEdit
        dateLabel.text! = DateConvertor.sharedInstance.getDateStr(timeStamp: dateEdit, format: "dd.MM.yyyy HH:mm").description
        timeLabel.text! = DateConvertor.sharedInstance.getDateStr(timeStamp: dateToEdit, format: "dd.MM.yyyy HH:mm").description
        if let temp = self.categoryIdArr.index(of: categoryEdit) {
            self.catLabel.text = self.categoryArr[temp]
            self.selectedCategory = Int(self.categoryIdArr[temp])!
        }
        if let temp = photoEdit {
            imageIcon.hnk_setImage(from: temp, placeholder: UIImage(), success: { (image) in
                self.imageIcon.image = image
                self.imageIcon.contentMode = .scaleAspectFill
                self.imageIcon.layer.masksToBounds = false
                self.imageIcon.clipsToBounds = true
                self.photoString = image!.resizeWithWidth(600)!.toBase64()
            }, failure: { (err) in })
        }
        //VLOŽENÍ INVERZNÍ HODNOTY NA SWITCH KTERÝ SE HNED PŘEPNE NA SPRÁVNOU
        switcherActivityForPublicIsOn = !onlyVisibleToPeopleFromMyCompanyEdit
        turnSwitch()
    }
    
    fileprivate func executeActivity(){
        if !activityTitletxtField.text!.isEmpty {
            if !self.dateLabel.text!.isEmpty {
                if !self.timeLabel.text!.isEmpty {
                    if selectedCategory != Int() {
                        if !capacityLabel.text!.isEmpty {
                            if !addressLatStr.isEmpty {
                                if !descTxtView.text!.isEmpty {
                                    if selectedCategory == 21 && destinationaddressLatStr.isEmpty {
                                        self.showSimpleAlert(title: "Error".localized(), message: "You have to specify destination address.".localized())
                                    } else {
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
                                        dateFormatter.timeZone = TimeZone.current
                                        if let resultFrom = dateFormatter.date(from: dateLabel.text!)?.timeIntervalSince1970 {
                                            if let resultTo = dateFormatter.date(from: timeLabel.text!)?.timeIntervalSince1970 {
                                                if canEdit {
                                                    updateActivity(resultFrom: resultFrom, resultTo: resultTo)
                                                } else {
                                                    createActivity(resultFrom: resultFrom, resultTo: resultTo)
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    self.showSimpleAlert(title: "Error".localized(), message: "Enter Activity description.".localized())
                                }
                            } else {
                                showSimpleAlert(title: "Error".localized(), message: "Enter place address.".localized())
                            }
                        } else {
                            showSimpleAlert(title: "Error".localized(), message: "Enter place how many people can join event.".localized())
                        }
                    } else {
                        showSimpleAlert(title: "Error".localized(), message: "Select category.".localized())
                    }
                } else {
                    showSimpleAlert(title: "Error".localized(), message: "Select date to".localized())
                }
            } else {
                showSimpleAlert(title: "Error".localized(), message: "Select date".localized())
            }
        } else {
            showSimpleAlert(title: "Error".localized(), message: "Enter Activity name.".localized())
        }
    }
    
    fileprivate func createActivity(resultFrom: Double, resultTo: Double){
        SwiftLoader.show(animated: true)
        ActivitiesReq().createNewActivity(titleName: activityTitletxtField.text!, date: resultFrom, dateTo: resultTo, photo: photoString, category: selectedCategory, description: descTxtView.text!, isPublic: switcherActivityForPublicIsOn, address: adressTxtField.text!, latitude: addressLatStr, longitude: addressLngStr, destinationAddress: destinationAddressField.text!, destinationlatitude: destinationaddressLatStr, destinationlongitude:destinationaddressLngStr, capacity: Int(capacityLabel.text!)!, completionHandler: { (done) in
            SwiftLoader.hide()
            if done {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd.MM.yyyy HH:mm"
                formatter.timeZone = TimeZone.current
                
                let startDate:NSDate = formatter.date(from: DateConvertor.sharedInstance.getDateStr(timeStamp: resultFrom, format: "dd.MM.yyyy HH:mm"))! as NSDate
                let endDate:NSDate = formatter.date(from: DateConvertor.sharedInstance.getDateStr(timeStamp: resultTo, format: "dd.MM.yyyy HH:mm"))! as NSDate
                CalendarWriterVC().addEventToCalendar(title: self.activityTitletxtField.text!, description: self.descTxtView.text!, startDate: startDate, endDate: endDate)
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.containerView.alpha = 0
                }) { (done) in
                    self.dismiss(animated: false, completion: {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newActivitySuccess"), object: nil)
                    })
                }
            } else {
                self.showSimpleAlert(title: "Error".localized(), message: "Server sent error.".localized())
            }
        })
    }
    
    fileprivate func updateActivity(resultFrom: Double, resultTo: Double){
        SwiftLoader.show(animated: true)
        ActivitiesReq().editActivity(activityId: idEdit, date: resultFrom, dateTo: resultTo, category: selectedCategory, titleName: activityTitletxtField.text!, address: adressTxtField.text!, description: descTxtView.text!, photo: photoString, isPublic: switcherActivityForPublicIsOn, latitude: addressLatStr, longitude: addressLngStr, completionHandler: { (done) in
            SwiftLoader.hide()
            if done {
                UIView.animate(withDuration: 0.5, animations: {
                    self.containerView.alpha = 0
                }) { (done) in
                    self.dismiss(animated: false, completion: {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadActDetail"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newActivitySuccess"), object: nil)
                    })
                }
            } else {
                self.showSimpleAlert(title: "Error".localized(), message: "Server sent error.".localized())
            }
        })
    }
    
    private func setupDropDown(){
        self.dropDown.anchorView = self.categoryView
        self.dropDown.direction = .bottom
        self.dropDown.bottomOffset = CGPoint(x: 0, y: self.categoryView.bounds.height)
        self.dropDown.dataSource = categoryArr
        self.dropDown.textFont = UIFont.systemFont(ofSize: 11)
        self.dropDown.backgroundColor = .white
    }
    
    func updateDate(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        if let dateStr = dict["dateStr"] as? String {
            self.dateLabel.text = dateStr
        }
        if let dateStr = dict["dateStrTo"] as? String {
            self.timeLabel.text = dateStr
        }
    }
    
    fileprivate func closeView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.alpha = 0
        }) { (done) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    fileprivate func presentModal(dateFrom:Bool){
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        modal.editActive = true
        modal.dateAndTimeActive = true
        modal.dateFormat = "dd.MM.yyyy HH:mm"
        modal.dateFrom = dateFrom
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: false, completion: nil)
    }
    
    private func setupView(){
        let tempColor = UIColor.lightGray
        
        self.dateView.layer.borderWidth = 1
        self.dateView.layer.borderColor = tempColor.cgColor
        
        self.timeView.layer.borderWidth = 1
        self.timeView.layer.borderColor = tempColor.cgColor
        
        self.categoryView.layer.borderWidth = 1
        self.categoryView.layer.borderColor = tempColor.cgColor
        
        self.adressView.layer.borderWidth = 1
        self.adressView.layer.borderColor = tempColor.cgColor
        
        self.destinationAdressView.layer.borderWidth = 1
        self.destinationAdressView.layer.borderColor = tempColor.cgColor
        
        self.descView.layer.borderWidth = 1
        self.descView.layer.borderColor = tempColor.cgColor
        
        self.imageViewContainer.layer.borderWidth = 1
        self.imageViewContainer.layer.borderColor = tempColor.cgColor
        
        self.capacityView.layer.borderWidth = 1
        self.capacityView.layer.borderColor = tempColor.cgColor
        
        self.containerView.alpha = 0
        
        self.activityTitletxtField.placeholder = "Activity name".localized()
        self.activityTitletxtField.setValue(UIColor.mainBlue, forKeyPath: "_placeholderLabel.textColor")
        self.boolTitle.text = "Visible only to people from my company?".localized()
        self.dateLabel.placeholder = "from".localized()
        self.timeLabel.placeholder = "to".localized()
        self.catLabel.placeholder = "Category".localized()
        self.adressTxtField.placeholder = "Address".localized()
        self.imageViewContainerPlacehollder.placeholder = "+ Activity image".localized()
        self.createBtn.setTitle("Create".localized(), for: .normal)
        self.cancelBtn.setTitle("Cancel".localized(), for: .normal)
        self.descTxtView.placeholder = "Activity description".localized()
        self.destinationAddressField.placeholder = "Destination address".localized()
        self.capacityLabel.placeholder = "NEW_ACTIVITY_CAPACITY".localized()
    }
    
    fileprivate func turnSwitch(){
        self.switcherDot.backgroundColor = UIColor.mainBlue
        self.switcherDot.layer.cornerRadius = 8
        if switcherActivityForPublicIsOn {
            switcherActivityForPublicIsOn = false
            UIView.animate(withDuration: 1, animations: {
                self.switcherDot.alpha = 0
            })
        } else {
            switcherActivityForPublicIsOn = true
            UIView.animate(withDuration: 1, animations: {
                self.switcherDot.alpha = 1
            })
        }
    }
    
    fileprivate func searchForCity(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        UINavigationBar.appearance().barTintColor = UIColor.navBarColor
        UINavigationBar.appearance().tintColor = UIColor.white
        
        UISearchBar.appearance().setTextColor(UIColor.white)
        UISearchBar.appearance().barStyle = UIBarStyle.default
        UISearchBar.appearance().setPlaceholderTextColor(.white)
        UISearchBar.appearance().setSearchImageColor(.white)
        
        self.present(autocompleteController, animated: true, completion: nil)
    }
    
    private func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(CreateActivityVC.openLibrary), name: NSNotification.Name(rawValue: "openLibrary"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateActivityVC.openCamera), name: NSNotification.Name(rawValue: "openCamera"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateActivityVC.updateDate), name: NSNotification.Name(rawValue: "updateDate"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openLibrary"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openCamera"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updateDate"), object: nil)
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var catLabel: UITextField!
    @IBOutlet weak var adressView: UIView!
    @IBOutlet weak var destinationAdressView: UIView!
    @IBOutlet weak var destinationConstant: NSLayoutConstraint!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var imageViewContainer: UIView!
    @IBOutlet weak var timeLabel: UITextField!
    @IBOutlet weak var dateLabel: UITextField!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var createBtn: UIButton!
    @IBOutlet weak var adressTxtField: UITextField!
    @IBOutlet weak var destinationAddressField: UITextField!
    @IBOutlet weak var descTxtView: KMPlaceholderTextView!
    @IBOutlet weak var switcherContainer: UIView!
    @IBOutlet weak var switcherDot: UIView!
    @IBOutlet weak var imageViewContainerPlacehollder: UITextField!
    @IBOutlet weak var boolTitle: UILabel!
    @IBOutlet weak var activityTitletxtField: UITextField!
    @IBOutlet weak var capacityView: UIView!
    @IBOutlet weak var capacityLabel: UITextField!
}

extension CreateActivityVC {
    
    @IBAction func createActivityBtn(_ sender: Any) {
        executeActivity()
    }
    
    @IBAction func pickPhotoBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "TypePickerVC") as! TypePickerVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
    
    @IBAction func searchCityBtn(_ sender: Any) {
        self.startAddress = true
        searchForCity()
    }
    
    @IBAction func searchForDestinationCityBtn(_ sender: Any) {
        self.startAddress = false
        searchForCity()
    }
    
    @IBAction func dropDownBtn(_ sender: Any) {
        if dropDown.isHidden {
            dropDown.show()
        }
        self.dropDown.selectionAction = { [unowned self] (index, item) in
            self.catLabel.text = item
            self.selectedCategory = Int(self.categoryIdArr[index])!
            if self.categoryIdArr[index] == self.categoryIdArr[2] {
                UIView.animate(withDuration: 0.5) {
                    self.destinationAdressView.alpha = 1
                    self.destinationConstant.constant = 80
                    self.view.layoutIfNeeded()
                }
            } else {
                UIView.animate(withDuration: 0.5) {
                    self.destinationAdressView.alpha = 0
                    self.destinationConstant.constant = 20
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        closeView()
    }
    
    @IBAction func outSideBoxBtn(_ sender: Any) {
        closeView()
    }
    
    @IBAction func openDateBtn(_ sender: Any) {
        presentModal(dateFrom: true)
    }
    
    @IBAction func openTimeBtn(_ sender: Any) {
        presentModal(dateFrom: false)
    }
    
    @IBAction func switcherPressedBtn(_ sender: Any) {
        turnSwitch()
    }
}

extension CreateActivityVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if self.startAddress {
            adressTxtField.text = place.formattedAddress
            addressLatStr = place.coordinate.latitude.description
            addressLngStr = place.coordinate.longitude.description
        } else {
            destinationAddressField.text = place.formattedAddress
            destinationaddressLatStr = place.coordinate.latitude.description
            destinationaddressLngStr = place.coordinate.longitude.description
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension CreateActivityVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @objc fileprivate func openLibrary(){
        DelayFunc.sharedInstance.delay(0.3) {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                self.imagePicker.navigationBar.isTranslucent = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    @objc fileprivate func openCamera(){
        DelayFunc.sharedInstance.delay(0.3) {
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.navigationBar.isTranslucent = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        imageIcon.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let img = imageIcon.image {
            imageIcon.image = img.resizeWithWidth(600)
            imageIcon.layer.borderWidth = 1.0
            imageIcon.layer.masksToBounds = false
            imageIcon.layer.borderColor = UIColor.white.cgColor
            imageIcon.clipsToBounds = true
            imageIcon.contentMode = .scaleAspectFill
            photoString = img.resizeWithWidth(600)!.toBase64()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: nil)
        
        if let img = imageIcon.image {
            imageIcon.image = img.resizeWithWidth(600)
            imageIcon.layer.borderWidth = 1.0
            imageIcon.layer.masksToBounds = false
            imageIcon.layer.borderColor = UIColor.white.cgColor
            imageIcon.clipsToBounds = true
            imageIcon.contentMode = .scaleAspectFill
            photoString = img.resizeWithWidth(600)!.toBase64()
        }
    }
}

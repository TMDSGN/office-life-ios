//
//  RegistrationVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class RegistrationVC: UIViewController, AlertMessage {
    
    fileprivate var selectedCompanyId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLabels()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    fileprivate func setupLabels(){
        registerBtn.setTitle("Register".localized(), for: .normal)
        passTxt.placeholder = "Password".localized()
        passAgainTxt.placeholder = "Retype Password".localized()
        nameTxt.placeholder = "Firstname".localized()
        surnameTxt.placeholder = "Surname".localized()
        registrationLabel.text = "Registration".localized()
        pinField.placeholder = "Entry code".localized()
        
        emailTxt.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
        passTxt.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
        passAgainTxt.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
        nameTxt.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
        surnameTxt.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
        pinField.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
    }
    
    /*
     *   Register the new user, if building code is not correct one then registration will not be completed.
     */
    fileprivate func postRegistration(){
        LoginReq().registration(buildingCode: pinField.text!, email: self.emailTxt.text!, username: self.emailTxt.text!, password: self.passTxt.text!, firstName: self.nameTxt.text!, lastName: self.surnameTxt.text!) { (done, wrongBuildingCode) in
            SwiftLoader.hide()
            if done && !wrongBuildingCode {
                Session.sharedInstance.isAfterRegistration = true
                let alert = UIAlertController(title: "Success".localized(), message: "Check your email for confirmation and then log in.".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showLoginScreen"), object: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            } else if !done && wrongBuildingCode {
                self.showSimpleAlert(title: "Error".localized(), message: "REGISTRATION_INVALID_BUILDING_CODE".localized())
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
            }
        }
    }
    
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passTxt: UITextField!
    @IBOutlet weak var passAgainTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var surnameTxt: UITextField!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var registrationLabel: UILabel!
    @IBOutlet weak var pinField: UITextField!
}

extension RegistrationVC {
    
    @IBAction func registerBtn(_ sender: Any) {
        if !self.emailTxt.text!.isEmpty {
            if !self.passTxt.text!.isEmpty {
                if !self.nameTxt.text!.isEmpty {
                    if !self.surnameTxt.text!.isEmpty {
                        // palac krizik secret = OL20032017 
                        if Reachability.isConnectedToNetwork() {
                            SwiftLoader.show(animated: true)
                            Authentization().getToken { (done) in
                                if done {
                                    self.postRegistration()
                                } else {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
                                }
                            }
                        } else {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
                        }
                    } else {
                        showSimpleAlert(title: "Error".localized(), message: "You have to enter valid PIN for building.".localized())
                    }
                }
            }
        }
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

//
//  ActivityDetailVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 23.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader
import Haneke

class ActivityDetailVC: UIViewController, AlertMessage {
    
    var activityId: String = ""
    var titleCategory: String = ""
    
    fileprivate var createdByMe = Bool()
    fileprivate var joined = Bool()
    fileprivate var joinedUsersId = [String]()
    fileprivate var joinedUsersName = [String]()
    fileprivate var joinedUsersPhoto = [String]()
    fileprivate var categoryType: String = ""
    
    fileprivate var idEdit: String = ""
    fileprivate var nameEdit: String = ""
    fileprivate var dateEdit = Double()
    fileprivate var dateToEdit = Double()
    fileprivate var descEdit: String = ""
    fileprivate var placeEdit: String = ""
    fileprivate var latitudeEdit: String = ""
    fileprivate var longitudeEdit: String = ""
    fileprivate var photoEdit: URL?
    
    fileprivate var destinationEdit: String = ""
    fileprivate var latitudeToEdit: String = ""
    fileprivate var longitudeToEdit: String = ""
    fileprivate var onlyVisibleToPeopleFromMyCompanyEdit = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObservers()
        setupLabels()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupCollectionLayout()
    }
    
    @objc fileprivate func getData(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ActivitiesReq().getActivitiesDetail(activityId: activityId, completionHandler: { (done, id, name, date, dateTo, background, type, place, desc, joined, joinedUsersId, joinedUsersName, joinedUsersPhoto, createdByMe, onlyVisibleToPeopleFromMyCompany, latitude, longitude, destination, latitudeTo, longitudeTo) in
                SwiftLoader.hide()
                if done {
                    self.titleLabel.text = name
                    self.timeLabel.text = DateConvertor.sharedInstance.getDateStr(timeStamp: date, format: "dd.MM.yyyy HH:mm")
                    self.adressLabel.text = place
                    self.desctxtView.text = desc
                    self.joined = joined
                    self.createdByMe = createdByMe
                    self.joinedUsersId = joinedUsersId
                    self.joinedUsersName = joinedUsersName
                    self.joinedUsersPhoto = joinedUsersPhoto
                    self.categoryType = type
                    self.idEdit = id
                    self.nameEdit = name
                    self.dateEdit = date
                    self.dateToEdit = dateTo
                    self.descEdit = desc
                    self.placeEdit = place
                    self.latitudeEdit = latitude
                    self.longitudeEdit = longitude
                    self.onlyVisibleToPeopleFromMyCompanyEdit = onlyVisibleToPeopleFromMyCompany
                    self.destinationEdit = destination
                    self.latitudeToEdit = latitudeTo
                    self.longitudeToEdit = longitudeTo
                    if !background.isEmpty {
                        let tempurl = URL(string: background)
                        self.imgIcon.hnk_setImage(from: tempurl)
                        self.photoEdit = tempurl
                    }
                    if joined {
                        self.blueBtn.setTitle("Joined".localized(), for: .normal)
                        self.blueJoinEditBtn.setTitle("Joined".localized(), for: .normal)
                    }
                    if !createdByMe {
                        UIView.animate(withDuration: 0.7, animations: {
                            self.editContainer.alpha = 0
                        })
                    } else {
                        UIView.animate(withDuration: 0.7, animations: {
                            self.editContainer.alpha = 1
                        })
                    }
                    self.collectionView.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func joinedActivity(){
            if Reachability.isConnectedToNetwork() {
                joined = !joined
                SwiftLoader.show(animated: true)
                ActivitiesReq().joinOrLeaveActivity(activityId: activityId, join: joined) { (done) in
                    SwiftLoader.hide()
                    if done {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd.MM.yyyy HH:mm"
                        formatter.timeZone = TimeZone.current
                        
                        let startDate:NSDate = formatter.date(from: DateConvertor.sharedInstance.getDateStr(timeStamp: self.dateEdit, format: "dd.MM.yyyy HH:mm"))! as NSDate
                        let endDate:NSDate = formatter.date(from: DateConvertor.sharedInstance.getDateStr(timeStamp: self.dateToEdit, format: "dd.MM.yyyy HH:mm"))! as NSDate
                        
                        if self.joined {
                            CalendarWriterVC().addEventToCalendar(title: self.titleLabel.text!, description: self.desctxtView.text!, startDate: startDate, endDate: endDate)
                            self.blueBtn.setTitle("Joined".localized(), for: .normal)
                            self.blueJoinEditBtn.setTitle("Joined".localized(), for: .normal)
                            let modal = self.storyboard?.instantiateViewController(withIdentifier: "GratzAlertVC") as! GratzAlertVC
                            modal.type = 0
                            modal.joined = true
                            let formatter = DateFormatter()
                            formatter.dateFormat = "dd.MM.yyyy HH:mm"
                            formatter.timeZone = TimeZone.current
                            modal.dateStr = formatter.string(from: Date())
                            
                            let navigationController = UINavigationController(rootViewController: modal)
                            navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            navigationController.isNavigationBarHidden = true
                            self.present(navigationController, animated: false, completion: nil)
                        } else {
                            self.blueBtn.setTitle("Join in".localized(), for: .normal)
                            self.blueJoinEditBtn.setTitle("Join in".localized(), for: .normal)
                        }
                    }
                }
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
            }
    }
    
    fileprivate func setupLabels(){
        title = titleCategory
        blueBtn.setTitle("Join in".localized(), for: .normal)
        blueJoinEditBtn.setTitle("Join in".localized(), for: .normal)
        aboutLabel.text = "About".localized()
        wallLabel.text = "Activity wall".localized()
        peopleJoinedLabel.text = "People joined".localized()
        self.editContainer.alpha = 0
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.getData), name: NSNotification.Name(rawValue: "reloadActDetail"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadActDetail"), object: nil)
    }
    
    @IBOutlet weak var editContainer: UIView!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var joinedPeopleView: UIView!
    
    @IBOutlet weak var blueBtn: UIButton!
    @IBOutlet weak var blueJoinEditBtn: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var wallLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var desctxtView: UITextView!
    
    @IBOutlet weak var descLeading: NSLayoutConstraint!
    @IBOutlet weak var descTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var joinedLeading: NSLayoutConstraint!
    @IBOutlet weak var joinedTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var lowerLayoutConstant: NSLayoutConstraint!
    @IBOutlet weak var higherLayoutConstant: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var peopleJoinedLabel: UILabel!
}

extension ActivityDetailVC {
    
    @IBAction func descBtn(_ sender: Any) {
        descLeading.isActive = true
        joinedLeading.isActive = false
        UIView.animate(withDuration: 0.1, animations: {
            self.joinedPeopleView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (true) in
            self.descTrailing.isActive = true
            self.joinedTrailing.isActive = false
            UIView.animate(withDuration: 0.1, animations: {
                self.descView.alpha = 1
                self.view.layoutIfNeeded()
            }) { (true) in
                self.lowerLayoutConstant.isActive = true
                self.higherLayoutConstant.isActive = false
                UIView.animate(withDuration: 0.7, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @IBAction func joinedPeopleBtn(_ sender: Any) {
        descLeading.isActive = false
        joinedLeading.isActive = true
        UIView.animate(withDuration: 0.1, animations: {
            self.descView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (true) in
            self.descTrailing.isActive = false
            self.joinedTrailing.isActive = true
            UIView.animate(withDuration: 0.1, animations: {
                self.joinedPeopleView.alpha = 1
                self.view.layoutIfNeeded()
            }) { (true) in
                self.lowerLayoutConstant.isActive = false
                self.higherLayoutConstant.isActive = true
                UIView.animate(withDuration: 0.7, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @IBAction func availableBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
        openNewVC.activityId = activityId
        openNewVC.activityWall = true
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func editBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "CreateActivityVC") as! CreateActivityVC
        modal.canEdit = true
        modal.idEdit = idEdit
        modal.nameEdit = nameEdit
        modal.descEdit = descEdit
        modal.placeEdit = placeEdit
        modal.dateEdit = dateEdit
        modal.dateToEdit = dateToEdit
        modal.categoryEdit = categoryType
        modal.latitudeEdit = latitudeEdit
        modal.longitudeEdit = longitudeEdit
        modal.onlyVisibleToPeopleFromMyCompanyEdit = onlyVisibleToPeopleFromMyCompanyEdit
        modal.destinationEdit = destinationEdit
        modal.destinationLatitudeEdit = latitudeToEdit
        modal.destinationLongitudeEdit = longitudeToEdit
        if let temp = photoEdit {
            modal.photoEdit = temp
        }
        if let temp = Int(categoryType) {
            modal.selectedCategory = temp
        }
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: false, completion: nil)
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    @IBAction func createReservationBtn(_ sender: Any) {
        joinedActivity()
    }
    
    @IBAction func joinActivityEditBtn(_ sender: Any) {
        joinedActivity()
    }
}

extension ActivityDetailVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    fileprivate func setupCollectionLayout(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 10, left: 12, bottom: 0, right: 12)
        layout.itemSize = CGSize(width: width / 2 - 35, height: 160)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        collectionView!.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PeopleCellVC", for: indexPath) as! PeopleCellVC
        cell.titleLabel.text = joinedUsersName[indexPath.row]
        
        if !joinedUsersPhoto[indexPath.row].isEmpty {
            let tempurl = URL(string:joinedUsersPhoto[indexPath.row])
            cell.imgProfile.hnk_setImage(from: tempurl, placeholder: UIImage(named:"placeholder_photo"), success: { (image) in
                cell.imgProfile.image = image
                cell.imgProfile.layer.cornerRadius = 40
                cell.imgProfile.clipsToBounds = true
            }, failure: { (err) in
                cell.imgProfile.image = UIImage(named: "placeholder_photo")
                cell.imgProfile.layer.cornerRadius = 0
                cell.imgProfile.clipsToBounds = true
                
            })
        } else {
            cell.imgProfile.image = UIImage(named: "placeholder_photo")
            cell.imgProfile.layer.cornerRadius = 0
            cell.imgProfile.clipsToBounds = true
        }
        cell.imgProfile.backgroundColor = UIColor.backgroundPhotoColor
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return joinedUsersId.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileReq().getMyProfileInfo(desiredProfileId: joinedUsersId[indexPath.row], completionHandler: { (done, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr, photoStr) in
                SwiftLoader.hide()
                if done {
                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "PeopleDetailVC") as! PeopleDetailVC
                    openNewVC.userFullName = name + " " + surname
                    openNewVC.interestStr = interestsArr.joined(separator: "")
                    openNewVC.contactStr = email
                    openNewVC.photoUser = photo
                    openNewVC.idCompany = photoStr
                    openNewVC.userId = self.joinedUsersId[indexPath.row]
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(openNewVC, animated: true)
                    });
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
}

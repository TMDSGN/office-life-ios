//
//  UserDropDownCell.swift
//  OfficeLife
//
//  Created by Josef Antoni on 13.03.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class UserDropDownCellVC: DropDownCell {

    override func awakeFromNib() {
        imageIcon.layer.cornerRadius = imageIcon.frame.height/2
        imageIcon.clipsToBounds = true
    }
    
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}

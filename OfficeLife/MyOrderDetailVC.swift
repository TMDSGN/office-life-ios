//
//  MyOrderDetailVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 27.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader

fileprivate struct MyOrderDetailData {
    
    var price, paymentMethod: String
    var addonsArr, priceArr, quantityArr, titleArr: [String]
    
    init(price: String, paymentMethod: String, addonsArr: [String], priceArr: [String], quantityArr: [String], titleArr: [String]) {
        self.price = price
        self.paymentMethod = paymentMethod
        self.addonsArr = addonsArr
        self.priceArr = priceArr
        self.quantityArr = quantityArr
        self.titleArr = titleArr
    }
}

class MyOrderDetailVC: UIViewController {
    
    var ticketNumber: String = ""
    fileprivate var myOrderDetailData = MyOrderDetailData(price: "", paymentMethod: "", addonsArr: [], priceArr: [], quantityArr: [], titleArr: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegister()
        setupLabels()
        getData()
    }
    
    fileprivate func getData(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            OrdersReq().getOrderDetail(orders_id: ticketNumber, completionHandler: { (done, paymentMethod, orderName, timeStr, status, price, addonsArr, priceArr, quantityArr, titleArr) in
                SwiftLoader.hide()
                if done {
                    self.myOrderDetailData = MyOrderDetailData(price: price, paymentMethod: paymentMethod, addonsArr: addonsArr, priceArr: priceArr, quantityArr: quantityArr, titleArr: titleArr)
                    self.tableView.reloadData()
                    self.descLabel.text = "Order n. #".localized() + orderName + " from date ".localized() + timeStr + " it is now in a state ".localized() + status.localized() + "."
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }

    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "MyOrderCellDetailView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MyOrderCellDetailVC")
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
    }
    
    fileprivate func setupLabels(){
        title = "My orders".localized()
        ticketNumberLabel.text = "Order ".localized() + ticketNumber
        productLabel.text = "Product".localized()
        priceLabel.text = "Total price".localized()
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ticketNumberLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
}

extension MyOrderDetailVC {
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

extension MyOrderDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myOrderDetailData.addonsArr.count + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MyOrderCellDetailVC") as! MyOrderCellDetailVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        if indexPath.row < myOrderDetailData.addonsArr.count {
            cell.titleLabel.text = myOrderDetailData.titleArr[indexPath.row] + " " + myOrderDetailData.quantityArr[indexPath.row] + "x"
            cell.priceLabel.text = myOrderDetailData.priceArr[indexPath.row]
        }
        if indexPath.row == myOrderDetailData.addonsArr.count{
            cell.titleLabel.text = "Payment method".localized()
            if myOrderDetailData.paymentMethod == "mycred" {
                cell.priceLabel.text = "Credit".localized()
            } else if myOrderDetailData.paymentMethod == "gopayredirbinder" {
                cell.priceLabel.text = "GoPay"
            } else {
                cell.priceLabel.text = "According Payment".localized()
            }
        }
        if indexPath.row == myOrderDetailData.addonsArr.count + 1 {
            cell.titleLabel.text = "Total price".localized()
            cell.priceLabel.text = myOrderDetailData.price
        }
        cell.dropdownImg.isHidden = true
        cell.dropDownPriceLabel.isHidden = true
        
        return cell
    }
}

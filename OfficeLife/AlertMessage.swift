//
//  AlertMessage.swift
//  Concierge
//
//  Created by Josef Antoni on 13.12.15.
//  Copyright © 2015 Pixelmate. All rights reserved.
//

import UIKit

protocol AlertMessage {}

extension AlertMessage where Self: UIViewController {
    /*
    * Show simple info alert with OK button.
    */
    func showSimpleAlert( title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}

//
//  MyEmptyCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 21.03.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class MyEmptyCellVC: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
        centerContainer.layer.borderWidth = 0.5
        centerContainer.layer.borderColor = UIColor.mainBlue.cgColor
    }

    @IBOutlet weak var centerContainer: UIView!
    
}

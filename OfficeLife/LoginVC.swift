//
//  LoginVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader
import FBSDKCoreKit
import FBSDKLoginKit
import LinkedinSwift

class LoginVC: UIViewController, AlertMessage {
    
    fileprivate let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "77athvkds1asdx", clientSecret: "yZ3aA1fp18Hfcfqy", state: "DLKDJF46ikMMZADfdfds", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "http://officelife.re/wc-api/auth/linkedin/oauth2callback"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupColorView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    /*
    *   Log in with username and password, if there is a first time user login, then move him ti Intro screen otherwise show him Home screen.
    */
    fileprivate func postSimpleLogin(){
        SwiftLoader.show(animated: true)
        LoginReq().simpleLogin(email: self.emailLabel.text!, pass: self.passLabel.text!) { (done, token, confirmed) in
            SwiftLoader.hide()
            if done {
                if token.isEmpty {
                    self.showSimpleAlert(title: "Log in failed".localized(), message: "Your email or password are not correct.".localized())
                } else {
                    if confirmed {
                        Session.sharedInstance.userToken = token
                        LoginReq().checkIfUserSeenWelcomeScreen(completionHandler: { (connectionSucess, seenTheWelcome) in
                            //connection fail?
                            if connectionSucess {
                                //seen the welcome screen?
                                if seenTheWelcome {
                                    DispatchQueue.main.async(execute: { () -> Void in
                                        self.performSegue(withIdentifier: "pushHome", sender: nil)
                                    });
                                } else {
                                    DispatchQueue.main.async(execute: { () -> Void in
                                        self.performSegue(withIdentifier: "socialSiteLoginPickCompany", sender: nil)
                                    });
                                }
                            } else {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
                            }
                        })
                    } else {
                        self.showSimpleAlert(title: "Check your email".localized(), message: "In order to activate your account, you have to activate link in your email.".localized())
                    }
                }
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
            }
        }
    }
    
    /*
     *   Log in with Facebook, if there is a first time user login, then move him ti Intro screen otherwise show him Home screen.
     */
    fileprivate func postFacebookLogin(token: String, expiration: String){
        LoginReq().getFacebookLogin(token: token, expiration: expiration, completionHandler: { (done, token) in
            if done {
                Session.sharedInstance.userToken = token
                Session.sharedInstance.facebookAcc = true
                self.socialSiteLogin()
            } else {
                SwiftLoader.hide()
                self.showSimpleAlert(title: "Error".localized(), message: "Login via Facebook failed, please try it later.".localized())
            }
        })
    }
    
    /*
     *   Log in with Linkedin, if there is a first time user login, then move him ti Intro screen otherwise show him Home screen.
     */
    fileprivate func postLinkedinLogin(token: String, expiration: Double){
        LoginReq().getLinkedinLogin(token: token, expiration: expiration, completionHandler: { (done, token) in
            if done {
                Session.sharedInstance.userToken = token
                Session.sharedInstance.facebookAcc = true
                self.socialSiteLogin()
            } else {
                SwiftLoader.hide()
                self.showSimpleAlert(title: "Error".localized(), message: "Login via Linkedin failed, please try it later.".localized())
            }
        })
    }
    
    /*
    *   Zkontrolování zda user viděl jak intro screen tak pak jestli zadal platný kod do budovy.
    */
    fileprivate func socialSiteLogin(){
        LoginReq().checkIfUserSeenWelcomeScreen(completionHandler: { (connectionSucess, seenTheWelcome) in
            //connection fail?
            if connectionSucess {
                //seen the welcome screen?
                if seenTheWelcome {
                    LoginReq().checkIfUserEnteredBuildingCode(completionHandler: { (connectionSucess, enteredCode, idOfBuilding) in
                        SwiftLoader.hide()
                        //connection fail?
                        if connectionSucess {
                            if enteredCode {
                                DispatchQueue.main.async(execute: { () -> Void in
                                    self.performSegue(withIdentifier: "pushHome", sender: nil)
                                });
                            } else {
                                DispatchQueue.main.async(execute: { () -> Void in
                                    self.performSegue(withIdentifier: "socialSiteLoginPickCompany", sender: nil)
                                });
                            }
                        } else {
                            SwiftLoader.hide()
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
                        }
                    })
                } else {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.performSegue(withIdentifier: "socialSiteLoginPickCompany", sender: nil)
                    });
                }
            } else {
                SwiftLoader.hide()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
            }
        })
    }
    
    fileprivate func setupColorView(){
        emailLabel.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
        passLabel.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
        passLabel.placeholder = "Password".localized()
        signInBtn.setTitle("Log in".localized(), for: .normal)
        signInLabel.text = "Log in".localized()
        lostPassLabel.text = "Lost password?".localized()
    }
    
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var passLabel: UITextField!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var lostPassLabel: UILabel!
}

extension LoginVC {
    
    @IBAction func loginBtn(_ sender: Any) {
        self.view.endEditing(true)
        if Reachability.isConnectedToNetwork() {
            if !emailLabel.text!.isEmpty {
                if !passLabel.text!.isEmpty {
                    if !Session.sharedInstance.securityToken.isEmpty {
                        self.postSimpleLogin()
                    } else {
                        SwiftLoader.show(animated: true)
                        Authentization().getToken { (done) in
                            SwiftLoader.hide()
                            if done {
                                self.postSimpleLogin()
                            } else {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
                            }
                        }
                    }
                } else {
                    showSimpleAlert(title: "Error".localized(), message: "Fill password".localized())
                }
            } else {
                showSimpleAlert(title: "Error".localized(), message: "Fill email".localized())
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    /*
    *   Use Facebook SDK to generate token and use to login.
    */
    @IBAction func facebookLoginBtn(_ sender: Any) {
        self.view.endEditing(true)
        let login : FBSDKLoginManager = FBSDKLoginManager()
        //login.loginBehavior = FBSDKLoginBehavior.native
        login.loginBehavior = FBSDKLoginBehavior.native
        login.logIn(withReadPermissions: ["public_profile", "email"], from: self, handler: {(result, error) in
            if error != nil {
                self.dismiss(animated: true, completion: {
                    self.showSimpleAlert(title: "Error".localized(), message: "Login via Facebook failed, please try it later.".localized())
                })
            } else {
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email, id"]).start(completionHandler: {(connection, result, error) -> Void in
                    if let accessToken = FBSDKAccessToken.current(){
                        SwiftLoader.show(animated: true)
                        Authentization().getToken { (done) in
                            if done {
                                self.postFacebookLogin(token: accessToken.tokenString, expiration: String(accessToken.expirationDate.timeIntervalSince1970))
                            } else {
                                SwiftLoader.hide()
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
                            }
                        }
                    }
                })
            }
        })
    }
    
    /*
     *   Use Linkedin SDK to generate token and use to login.
     */
    @IBAction func linkedLoginBtn(_ sender: Any) {
        self.view.endEditing(true)
        linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
            SwiftLoader.show(animated: true)
            Authentization().getToken { (done) in
                if done {
                    self.postLinkedinLogin(token: lsToken.accessToken, expiration: lsToken.expireDate.timeIntervalSince1970)
                } else {
                    SwiftLoader.hide()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
            }, error: { [unowned self] (error) -> Void in
                self.showSimpleAlert(title: "Error".localized(), message: "Login via Linkedin failed, please try it later.".localized())
            }, cancel: { [unowned self] () -> Void in
                self.view.endEditing(true)
        })
    }
    
    /*
     *   Push to forgotten pass screen.
     */
    @IBAction func forgottenPassBtn(_ sender: Any) {
        self.view.endEditing(true)
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotenPassVC")
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC!, animated: true)
        });
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

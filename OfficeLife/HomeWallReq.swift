//
//  HomeWallReq.swift
//  OfficeLife
//
//  Created by Josef Antoni on 10.02.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class HomeWallReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getPosts(page:String, completionHandler: @escaping (Bool, Int, Int, [String], [String], [String], [String], [String], [String], [String], [String], [String], [[String]], [Bool], [Bool], [Bool], [Bool], [[String]], [[String]], [[String]], [[String]], [String]) -> ()) -> (){
        
        let url = urlDomain + "/home/v1/homewall/\(page)"
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                var dateArr = [String]()
                var idArr = [String]()
                var nameArr = [String]()
                var tagArr = [String]()
                var tagNameArr = [String]()
                var textArr = [String]()
                var activityIdArr = [String]()
                var postedByIdArr = [String]()
                var postedByNameArr = [String]()
                var postedByPhotoArr = [String]()
                var attachmentPhotoArr = [[String]]()
                var attachmentComentsTextArr = [[String]]()
                var attachmentComentsPhotoArr = [[String]]()
                var attachmentComentsCreatedArr = [[String]]()
                var attachmentComentsUserIdArr = [[String]]()
                var pinnedArr = [Bool]()
                var canPinArr = [Bool]()
                var canPemoveArr = [Bool]()
                var myPostArr = [Bool]()
                
                let currentPage = json["current_page"].intValue
                let totalPage = json["page_number"].intValue

                for i in 0..<json["list"].count{
                    let date = json["list"][i]["date"].doubleValue
                    dateArr.append(DateConvertor.sharedInstance.timeAgoSinceDate(date: DateConvertor.sharedInstance.getDate(timeStamp: date, format: "dd.MM.yyyy HH:mm"), numericDates: true))
                    
                    //string
                    let id = json["list"][i]["id"].stringValue
                    let name = json["list"][i]["name"].stringValue
                    let tag = json["list"][i]["tag"].stringValue
                    let tagName = json["list"][i]["tag_name"].stringValue
                    let text = json["list"][i]["text"].stringValue
                    let postedById = json["list"][i]["posted_by"]["user_id"].stringValue
                    let postedByName = json["list"][i]["posted_by"]["name"].stringValue
                    let postedByPhoto = json["list"][i]["posted_by"]["photo"].stringValue
                    let activityId = json["list"][i]["activity"]["id"].stringValue
                    
                    idArr.append(id)
                    nameArr.append(name)
                    tagArr.append(tag)
                    tagNameArr.append(tagName)
                    textArr.append(text)
                    postedByIdArr.append(postedById)
                    postedByNameArr.append(postedByName)
                    postedByPhotoArr.append(postedByPhoto)
                    activityIdArr.append(activityId)
                    
                    //bool
                    let pinned = json["list"][i]["pinned"].boolValue
                    let canPin = json["list"][i]["can_pin"].boolValue
                    let canPemove = json["list"][i]["can_remove"].boolValue
                    let myPost = json["list"][i]["my_post"].boolValue
                    
                    pinnedArr.append(pinned)
                    canPinArr.append(canPin)
                    canPemoveArr.append(canPemove)
                    myPostArr.append(myPost)
                    
                    var tempPhotoArr = [String]()
                    for j in 0..<json["list"][i]["photos"].count {
                        let photos = json["list"][i]["photos"][j].stringValue
                        tempPhotoArr.append(photos)
                    }
                    attachmentPhotoArr.append(tempPhotoArr)
                    
                    var comTextArr = [String]()
                    var comPhotoArr = [String]()
                    var comUserIdArr = [String]()
                    var comCreatedArr = [String]()
                    
                    for j in 0..<json["list"][i]["comments"].count{
                        let text = json["list"][i]["comments"][j]["text"].stringValue
                        let created = json["list"][i]["comments"][j]["created"].doubleValue
                        let userId = json["list"][i]["comments"][j]["user_id"].stringValue
                        let photo = json["list"][i]["comments"][j]["user_photo"].stringValue
                        
                        comTextArr.append(text)
                        comCreatedArr.append(DateConvertor.sharedInstance.timeAgoSinceDate(date: DateConvertor.sharedInstance.getDate(timeStamp: created, format: "dd.MM.yyyy HH:mm"), numericDates: true))
                        comUserIdArr.append(userId)
                        comPhotoArr.append(photo)
                    }
                    attachmentComentsTextArr.append(comTextArr)
                    attachmentComentsPhotoArr.append(comPhotoArr)
                    attachmentComentsCreatedArr.append(comCreatedArr)
                    attachmentComentsUserIdArr.append(comUserIdArr)
                    
                }
                
                completionHandler(true, currentPage, totalPage, dateArr, idArr, nameArr, tagArr, tagNameArr, textArr, postedByIdArr, postedByNameArr, postedByPhotoArr, attachmentPhotoArr, pinnedArr, canPinArr, canPemoveArr, myPostArr, attachmentComentsTextArr, attachmentComentsPhotoArr, attachmentComentsCreatedArr, attachmentComentsUserIdArr, activityIdArr)
            } else {
                completionHandler(false, Int(), Int(), [String](), [String](), [String](), [String](), [String](), [String](), [String](), [String](), [String](), [[String]](), [Bool](), [Bool](), [Bool](), [Bool](), [[String]](), [[String]](), [[String]](), [[String]](), [String]())
            }
        }
    }
    
    func createNewWallPost(text:String, photo:[String], completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/home/v1/homewall"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        var para = [
            "text" : text
            ] as [String : Any]
        if photo != [String]() {
            para = [
                "text" : text,
                "photos" : photo
            ]
        }
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 204 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func createNewComent(text:String, postId:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/home/v1/homewall/\(postId)"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "text" : text
        ]
        
        Alamofire.request(url, method: .put, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 204 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
}

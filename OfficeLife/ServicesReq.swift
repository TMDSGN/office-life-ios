//
//  ServicesReq.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

/*
 
 Services types
 Type       cz	Id
 Food       Jídlo a pití	1
 Other	Ostatní	2Food types
 Type	cz	Id
 All	Všechno	1
 Express	Expres	2
 Regular	Pravidelné	2Other types
 Type	cz	Id
 All	Všechno	1
 Wash	Mytí aut	2
 Flowers	Květiny	3
 Servis	Servis	4
 Cleaning	Čištění oděvů	5
 
 */

import Foundation
import Alamofire
import SwiftyJSON
import Localize_Swift

class ServicesReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getServicesPreview(completionHandler: @escaping (Bool, [String], [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/service/v1/home"
        let lang = Localize.currentLanguage()
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "Language" : lang
        ]
        
        let parameter = [
            "number" : 6
        ]
        
        Alamofire.request(url, method: .get, parameters: parameter, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                var idArr = [String]()
                var nameArr = [String]()
                var priceArr = [String]()
                var backgroundArr = [String]()
                var typeArr = [String]()
                
                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let name = json[i]["name"].stringValue
                    let price = json[i]["price"].stringValue
                    let background = json[i]["photos"].stringValue
                    let type = json[i]["type"].stringValue
                    
                    idArr.append(id)
                    nameArr.append(name)
                    priceArr.append(price)
                    backgroundArr.append(background)
                    typeArr.append(type)
                }
                
                completionHandler(true, idArr, nameArr, priceArr, backgroundArr, typeArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String]())
            }
        }
    }
    
    func getServicesList(serviceId:String, completionHandler: @escaping (Bool, [String], [String], [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/service/v1/list/\(serviceId)"
        let lang = Localize.currentLanguage()
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "Language" : lang
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                
                var idArr = [String]()
                var photoArr = [String]()
                var nameArr = [String]()
                var supplierArr = [String]()
                var priceArr = [String]()
                var tagArr = [String]()

                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let photo = json[i]["photos"].stringValue
                    let name = json[i]["name"].stringValue//timestamp
                    let supplier = json[i]["supplier"].stringValue
                    let price = json[i]["price"].stringValue
                    let tag = json[i]["tag"].stringValue
                    idArr.append(id)
                    photoArr.append(photo)
                    nameArr.append(name)
                    supplierArr.append(supplier)
                    priceArr.append(price)
                    tagArr.append(tag)
                }
                
                completionHandler(true, idArr, photoArr, nameArr, supplierArr , priceArr, tagArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String](), [String]())
            }
        }
    }
    
    func getServicesDetail(serviceId:String, completionHandler: @escaping (Bool, String, String, String, String, String, String, String, String, String, String, [String], [String], Bool) -> ()) -> (){
        
        let url = urlDomain + "/service/v1/\(serviceId)"
        let lang = Localize.currentLanguage()
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "Language" : lang
        ]
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                let photo = json["photos"].stringValue
                let name = json["name"].stringValue//timestamp
                let description = json["description"].stringValue
                let price = json["price"].stringValue
                let supplierStreet = json["supplier_details"]["street"].stringValue
                let supplierCity = json["supplier_details"]["city"].stringValue
                let supplierPhone = json["supplier_details"]["phone"].stringValue
                let supplierEmail = json["supplier_details"]["email"].stringValue
                let accordingGateway = json["according_gateway"].boolValue

                var addonsName: String = ""
                var typeOfView: String = ""
                var labelArr = [String]()
                var priceArr = [String]()
                
                for _ in 0..<json["addons"].count{
                    addonsName = json["addons"][0]["name"].stringValue
                    typeOfView = json["addons"][0]["type"].stringValue
                    
                    for j in 0..<json["addons"][0]["options"].count{
                        let label = json["addons"][0]["options"][j]["label"].stringValue
                        let price = json["addons"][0]["options"][j]["price"].stringValue
                        labelArr.append(label)
                        priceArr.append(price)
                    }
                    
                }
                
                completionHandler(true, photo, name, description, supplierStreet , supplierCity, supplierPhone, supplierEmail, price, addonsName, typeOfView, labelArr, priceArr, accordingGateway)
            } else {
                completionHandler(false, String(), String(), String(), String(), String(), String(), String(), String(), String(), String(), [String](), [String](), Bool())
            }
        }
    }
    
    func orderService(serviceTypeBig:Bool, service_id:String, pieces:String, phone:String, descNote:String, paymentMethod:String, productTitleForSentArray:[String], productPriceForSentArray:[Double], productQuantityForSentArray:[Int], returnUrlForPay: Bool, email: String, isCompanyOrder: Bool, companyName:String, companyBid: String, companyVat:String, companyZip: String, companyAddress: String, completionHandler: @escaping (Bool, String) -> ()) -> (){
        
        let url = urlDomain + "/service/v1/order_service/\(service_id)"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        var para: [String: Any] = [
            "pieces" : pieces,
            "payment_method" : paymentMethod,
            "url" : returnUrlForPay
        ]
        
        if !phone.isEmpty || !descNote.isEmpty {
            var personalInfo: [String : Any] = [
                "phone" : phone,
                "notes" : descNote,
                "email" : email
            ]
            if isCompanyOrder {
                personalInfo["company_order"] = true
                personalInfo["company_name"] = companyName
                personalInfo["company_bid"] = companyBid
                personalInfo["company_vat"] = companyVat
                personalInfo["company_zip"] = companyZip
                personalInfo["company_address"] = companyAddress
            }
            para["personal_info"] = personalInfo
        }
        
        if serviceTypeBig {
            var addons: [[String: Any]] = []
            for i in 0..<productTitleForSentArray.count {
                let price = round(100 * productPriceForSentArray[i]) / 100
                let addon: [String: Any] = [
                    "name":productTitleForSentArray[i],
                    "price":price,
                    "quantity":productQuantityForSentArray[i]
                ]
                //append a single addon to our array prepared before the loop
                addons.append(addon)
            }
            para["addons"] = addons
        }
        //print(header)
        //print(para)
        
        Alamofire.request(url, method: .post, parameters: para as [String: Any], encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                if let tempData = data {
                    let json = JSON(tempData)
                    let url = json["url"].stringValue
                    completionHandler(true, url)
                } else {
                    completionHandler(false, String())
                }
            } else if response.response?.statusCode == 201 {
                completionHandler(true, String())
            } else if response.response?.statusCode == 400 {
                completionHandler(false, "NOT_ENOUGHT_CREDITS".localized())
            } else {
                completionHandler(false, String())
            }
        }
    }
}

//
//  ImagePresenterVC.swift
//  CPF jobs
//
//  Created by Josef Antoni on 04.10.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import Haneke

class ImagePresenterVC: UIViewController {
    
    var imgString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = UIColor.black
        let temp = URL(string: imgString)
        imageIcon.hnk_setImage(from: temp)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.shouldSupportAllOrientation = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.shouldSupportAllOrientation = false
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
    
    @IBOutlet var imageIcon: UIImageView!
}

extension ImagePresenterVC {

    @IBAction func backBrn(_ sender: AnyObject) {
        self.dismiss(animated: false) { () -> Void in
            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }
    }
}

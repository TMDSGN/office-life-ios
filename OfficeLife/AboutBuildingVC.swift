//
//  AboutBuildingVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 28.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import ImageSlideshow
import Localize_Swift

class AboutBuildingVC: HamburgerContainerVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIcon")
        title = "About building".localized()
        imageSlideshow.setImageInputs([
            ImageSource(image: UIImage(named: "budovaTEST1")!),
            ImageSource(image: UIImage(named: "budovaTEST2")!),
            ImageSource(image: UIImage(named: "budovaTEST3")!)
            ])
        
        imageSlideshow.backgroundColor = UIColor.clear
        imageSlideshow.slideshowInterval = 5.0
        imageSlideshow.pageControlPosition = PageControlPosition.insideScrollView
        imageSlideshow.pageControl.currentPageIndicatorTintColor = UIColor.mainBlue;
        imageSlideshow.pageControl.pageIndicatorTintColor = UIColor.white;
        imageSlideshow.contentScaleMode = UIViewContentMode.scaleAspectFill

        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        style.lineSpacing = 10
        
        let attributesBold = [
            NSFontAttributeName : UIFont(name: "HelveticaNeue-Medium", size: 12.0)!,
            NSForegroundColorAttributeName : UIColor.black,
            NSParagraphStyleAttributeName : style
        ] as [String : Any]
        let attributesThin = [
            NSFontAttributeName : UIFont(name: "HelveticaNeue", size: 12.0)!,
            NSForegroundColorAttributeName : UIColor.black,
            NSParagraphStyleAttributeName : style
            ] as [String : Any]
        
        let separator = NSAttributedString(string: "\n. . .\n", attributes: attributesBold)
        let separator2 = NSAttributedString(string: "\n\n", attributes: attributesBold)
        let atriStringTitle = NSAttributedString(string: "ABOUT_BUILDING_SUBTITLE0".localized(), attributes: attributesBold)
        let atriString1 = NSAttributedString(string: "ABOUT_BUILDING_SUBTITLE1".localized(), attributes: attributesThin)
        let atriString2 = NSAttributedString(string: "ABOUT_BUILDING_SUBTITLE2".localized(), attributes: attributesThin)
        let atriString3 = NSAttributedString(string: "ABOUT_BUILDING_SUBTITLE3".localized(), attributes: attributesThin)
        let atriString4 = NSAttributedString(string: "ABOUT_BUILDING_SUBTITLE4".localized(), attributes: attributesThin)
        let atriString5 = NSAttributedString(string: "ABOUT_BUILDING_SUBTITLE5".localized(), attributes: attributesThin)
        let atriString6 = NSAttributedString(string: "ABOUT_BUILDING_SUBTITLE6".localized(), attributes: attributesThin)
        let atriString7 = NSAttributedString(string: "ABOUT_BUILDING_SUBTITLE7".localized(), attributes: attributesThin)
        let atriStringL = NSAttributedString(string: "ABOUT_BUILDING_LOCATION".localized(), attributes: attributesBold)

        let combination = NSMutableAttributedString()
        combination.append(atriStringTitle)
        combination.append(atriString1)
        combination.append(separator)
        combination.append(atriString2)
        combination.append(separator)
        combination.append(atriString3)
        combination.append(separator)
        combination.append(atriString4)
        combination.append(separator)
        combination.append(atriString5)
        combination.append(separator)
        combination.append(atriString6)
        combination.append(separator2)
        combination.append(atriStringL)
        combination.append(atriString7)
        
        descTxtView.attributedText = combination
    }

    @IBOutlet weak var imageSlideshow: ImageSlideshow!
    @IBOutlet weak var descTxtView: UITextView!
}

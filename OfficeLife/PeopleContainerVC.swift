//
//  PeopleContainerVC
//  OfficeLife
//
//  Created by Josef Antoni on 30.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import EMPageViewController

class PeopleContainerVC: HamburgerContainerVC {
    
    var pageViewController: EMPageViewController?
    var currentPage: Int = 0
    var viewsArr: [String] = ["1", "2"]

    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        setupPageVC()
        setupLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIcon")
        title = "People".localized()
    }
    
    fileprivate func getData(){
        let s = Session.sharedInstance
        if s.idOfCompaniesArr.count == 0 {
            if Reachability.isConnectedToNetwork() {
                LoginReq().getCompanies(completionHandler: { (done, idArr, nameArr, iconsArr, descArr, contactNameArr, contactEmailArr, contactPhotoArr) in
                    if done {
                        let s = Session.sharedInstance
                        s.idOfCompaniesArr = idArr
                        s.nameOfCompaniesArr = nameArr
                        s.iconsOfCompaniesArr = iconsArr
                        s.descOfCompaniesArr = descArr
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadDataInCells"), object: nil)
                    }
                })
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
            }
        }
    }
    
    fileprivate func showAllAnimation(){
        if currentPage == 1 {
            vsechnoLeading.isActive = true
            spolecnostLeading.isActive = false
            self.spolecnostTrailing.isActive = false
            self.vsechnoTrailing.isActive = true
            UIView.animate(withDuration: 0.0, animations: {
                self.view.layoutIfNeeded()
            }) { (true) in
            }
        }
    }
    
    fileprivate func showCompanyAnimation(){
        if currentPage == 0 {
            vsechnoTrailing.isActive = false
            spolecnostTrailing.isActive = true
            self.vsechnoLeading.isActive = false
            self.spolecnostLeading.isActive = true
            UIView.animate(withDuration: 0, animations: {
                self.view.layoutIfNeeded()
            }) { (true) in
            }
        }
    }
    
    fileprivate func scrollAnimation(_ currentPage:Int){
        let selectedIndex = self.index(of: self.pageViewController!.selectedViewController as! PeopleCVC)!
        let direction:EMPageViewControllerNavigationDirection = currentPage > selectedIndex ? .forward : .reverse
        self.currentPage = currentPage
        let viewController = self.viewController(at: currentPage)!
        self.pageViewController!.selectViewController(viewController, direction: direction, animated: true, completion: nil)
    }
    
    fileprivate func setupPageVC(){
        let pageViewController = EMPageViewController()
        pageViewController.dataSource = self
        pageViewController.delegate = self
        let currentViewController = self.viewController(at: 0)!
        pageViewController.selectViewController(currentViewController, direction: .forward, animated: false, completion: nil)
        self.addChildViewController(pageViewController)
        self.view.insertSubview(pageViewController.view, at: 0)
        pageViewController.didMove(toParentViewController: self)
        self.pageViewController = pageViewController
    }
    
    fileprivate func setupLabels(){
        self.everyThingLabel.text = "All".localized()
        self.companyLabel.text = "Company".localized()
    }
    
    @IBOutlet weak var vsechnoLeading: NSLayoutConstraint!
    @IBOutlet weak var vsechnoTrailing: NSLayoutConstraint!
    @IBOutlet weak var spolecnostLeading: NSLayoutConstraint!
    @IBOutlet weak var spolecnostTrailing: NSLayoutConstraint!
    @IBOutlet weak var everyThingLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
}

extension PeopleContainerVC {

    @IBAction func showAllBtn(_ sender: Any) {
        showAllAnimation()
        scrollAnimation(0)
    }
    
    @IBAction func showCompanyBtn(_ sender: Any) {
        showCompanyAnimation()
        scrollAnimation(1)
    }
}

extension PeopleContainerVC: EMPageViewControllerDataSource, EMPageViewControllerDelegate {
    
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! PeopleCVC) {
            let beforeViewController = self.viewController(at: viewControllerIndex - 1)
            switch viewControllerIndex {
            case 0:
                showAllAnimation()
            case 1:
                showCompanyAnimation()
            default:
                break
            }
            currentPage = viewControllerIndex
            return beforeViewController
        } else {
            return nil
        }
    }
    
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! PeopleCVC) {
            let afterViewController = self.viewController(at: viewControllerIndex + 1)
            switch viewControllerIndex {
            case 0:
                showAllAnimation()
            case 1:
                showCompanyAnimation()
            default:
                break
            }
            currentPage = viewControllerIndex
            return afterViewController
        } else {
            return nil
        }
    }
    
    func viewController(at index: Int) -> PeopleCVC? {
        if (self.viewsArr.count == 0) || (index < 0) || (index >= self.viewsArr.count) {
            return nil
        }
        
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "PeopleCVC") as! PeopleCVC
        viewController.idStr = self.viewsArr[index]
        return viewController
    }
    
    func index(of viewController: PeopleCVC) -> Int? {
        if let greeting: String = viewController.idStr {
            return self.viewsArr.index(of: greeting)
        } else {
            return nil
        }
    }
}

//
//  HomeTableCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 14.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class HomeActivityTableCellVC: UITableViewCell {
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(HomeActivityTableCellVC.reloadCollection), name: NSNotification.Name(rawValue: "reloadActivityCollectionView"), object: nil)
        let nib = UINib(nibName: "HomeActivityCollectionCellView", bundle: nil)
        activityCollectionView.register(nib, forCellWithReuseIdentifier: "HomeActivityCollectionCell")
    }
    
    func reloadCollection(){
        activityCollectionView.reloadData()
    }
    
    @IBOutlet public weak var activityCollectionView: UICollectionView!
}

extension HomeActivityTableCellVC {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        activityCollectionView.delegate = dataSourceDelegate
        activityCollectionView.dataSource = dataSourceDelegate
        activityCollectionView.tag = 1
        activityCollectionView.setContentOffset(activityCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        activityCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { activityCollectionView.contentOffset.x = newValue }
        get { return activityCollectionView.contentOffset.x }
    }
}

//
//  GratzAlertVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 29.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import Localize_Swift

class GratzAlertVC: UIViewController, AlertMessage {
    
    var type: Int = 0
    var joined: Bool = false
    var orderType: String = ""
    var dateStr: String = ""
    var priceServiceOrder: String = ""
    fileprivate var countNumber: Int = 0
    fileprivate var totalPrice: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainContainer.alpha = 0
        navigationController?.isNavigationBarHidden = true
        count("+")
    }
    
    /*
     *  Accoring to variable type change scene look.
    */
    override func viewWillAppear(_ animated: Bool) {
        switch type {
        case 0:
            imgIcon.isHidden = false
            doneIcon.isHidden = false
            subTitleLabel.isHidden = true
            countContainer.isHidden = true
            titleBlueLabel.isHidden = false
            titleLabel.text = dateStr
            if joined {
                titleBlueLabel.text = "Activity joined".localized()
            } else {
                titleBlueLabel.text = "Activity left".localized()
            }
            blueBtn.setTitle("Back".localized(), for: .normal)
            
        case 1:
            imgIcon.isHidden = true
            doneIcon.isHidden = false
            titleLabel.isHidden = true
            countContainer.isHidden = true
            subTitleLabel.isHidden = false
            titleBlueLabel.isHidden = false
            subTitleLabel.text = orderType
            titleBlueLabel.text = "Congratulations on order".localized()
            blueBtn.setTitle("Back".localized(), for: .normal)
        case 2:
            countContainer.isHidden = false
            titleBlueLabel.isHidden = true
            subTitleLabel.isHidden = true
            titleLabel.isHidden = true
            doneIcon.isHidden = true
            imgIcon.isHidden = true
            quantitTitleLabel.text = "Quantity".localized()
            priceTitleLabel.text = "Price".localized()
            blueBtn.setTitle("Order".localized(), for: .normal)
        case 3:
            imgIcon.isHidden = false
            doneIcon.isHidden = false
            subTitleLabel.isHidden = true
            countContainer.isHidden = true
            titleBlueLabel.isHidden = false
            titleLabel.text = dateStr
            titleBlueLabel.text = "Congratulations on reservation".localized()
            blueBtn.setTitle("Back".localized(), for: .normal)
        case 4:
            imgIcon.isHidden = false
            doneIcon.isHidden = false
            subTitleLabel.isHidden = true
            countContainer.isHidden = true
            titleBlueLabel.isHidden = false
            titleLabel.text = dateStr
            titleBlueLabel.text = "Congratulations, you created activity".localized()
            blueBtn.setTitle("Back".localized(), for: .normal)
            
        default:
            break
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.1, animations: {
            self.heightConstant.constant = self.mainContainer.frame.size.width
            self.view.layoutIfNeeded()
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            self.mainContainer.alpha = 1
            self.view.layoutIfNeeded()
        })
    }
    
    /*
     *   Counting final price for product + diplaying it
     */
    fileprivate func count(_ char:String){
        if countNumber >= 0 {
            switch char {
            case "+":
                countNumber += 1
            case "-":
                if countNumber > 0 {
                    countNumber -= 1
                }
            default:
                break
            }
            counterLabel.text = String(countNumber)
            if !priceServiceOrder.isEmpty {
                let arr = priceServiceOrder.components(separatedBy: " ")
                if let temp = Int(arr[0]) {
                    totalPrice = String(temp*countNumber) + " \(arr[1])"
                    self.priceLabel.text = String(temp*countNumber) + " \(arr[1])"
                } else if let temp = Double(arr[0].replacingOccurrences(of: ",", with: ".")) {
                    totalPrice = String(temp*Double(countNumber)) + " \(arr[1])"
                    self.priceLabel.text = String(temp*Double(countNumber)) + " \(arr[1])"
                }
            }
        }
    }
    
    /*
    *   Return to previous screen
    */
    fileprivate func backBtn(){
        if type == 1 {
            UIView.animate(withDuration: 0.3, animations: {
                self.mainContainer.alpha = 0
                self.view.layoutIfNeeded()
            })
            DelayFunc.sharedInstance.delay(0.3) {
                self.dismiss(animated: false, completion: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderFinished"), object: nil)
                })
            }
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.mainContainer.alpha = 0
                self.view.layoutIfNeeded()
            })
            DelayFunc.sharedInstance.delay(0.3) {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    @IBOutlet weak var blueBtn: UIButton!
    @IBOutlet weak var titleBlueLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var doneIcon: UIImageView!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var countContainer: UIView!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    @IBOutlet weak var quantitTitleLabel: UILabel!
    @IBOutlet weak var priceTitleLabel: UILabel!
}

extension GratzAlertVC {

    @IBAction func plusBtn(_ sender: Any) {
        count("+")
    }
    
    @IBAction func minusBtn(_ sender: Any) {
        count("-")
    }

    @IBAction func backBtn(_ sender: Any) {
        if type != 2 {
            backBtn()
        } else {
            if countNumber > 0 {
                backBtn()
                DelayFunc.sharedInstance.delay(0.5, closure: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "executePushOrderToFinish"), object: ["numberOfItems" : self.counterLabel.text, "totalPriceOfProduct" : self.totalPrice])
                })
            } else {
                showSimpleAlert(title: "Error".localized(), message: "Select atleast 1 item.".localized())
            }
        }
    }
    
    @IBAction func tapOutsideBtn(_ sender: Any) {
        backBtn()
    }
}

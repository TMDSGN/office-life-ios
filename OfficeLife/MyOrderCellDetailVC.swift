//
//  MyOrderCellDetailVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 27.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit

class MyOrderCellDetailVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dropdownImg: UIImageView!
    @IBOutlet weak var dropDownPriceLabel: UILabel!
}

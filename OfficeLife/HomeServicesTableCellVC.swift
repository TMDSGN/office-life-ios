//
//  HomeservicesTableCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 14.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class HomeServicesTableCellVC: UITableViewCell {
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(HomeServicesTableCellVC.reloadCollection), name: NSNotification.Name(rawValue: "reloadServicesCollectionView"), object: nil)
        let nib = UINib(nibName: "HomeServicesCollectionCellView", bundle: nil)
        servicesCollectionView.register(nib, forCellWithReuseIdentifier: "HomeServicesCollectionCell")
    }
    
    func reloadCollection(){
        servicesCollectionView.reloadData()
    }
    
    @IBOutlet public weak var servicesCollectionView: UICollectionView!
    
}

extension HomeServicesTableCellVC {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        servicesCollectionView.delegate = dataSourceDelegate
        servicesCollectionView.dataSource = dataSourceDelegate
        servicesCollectionView.tag = 2
        servicesCollectionView.setContentOffset(servicesCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        servicesCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { servicesCollectionView.contentOffset.x = newValue }
        get { return servicesCollectionView.contentOffset.x }
    }
}

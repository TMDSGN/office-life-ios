//
//  PeopleDetailVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 30.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import Haneke

class PeopleDetailVC: UIViewController, AlertMessage {
    
    var userId: String = ""
    var userFullName: String = ""
    var interestStr: String = ""
    var contactStr: String = ""
    var photoUser: String = ""
    var idCompany: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "People".localized()
        self.interestsLabel.text = "Interests:".localized()
        self.contactlabel.text = "Contact:".localized()
        titleLabel.text = userFullName
        interestLabel.text = interestStr
        contactLabel.text = contactStr
        msgBtn.setTitle("Message".localized(), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let i = Session.sharedInstance.idOfCompaniesArr.index(of: idCompany) {
            let tempurl = URL(string:Session.sharedInstance.iconsOfCompaniesArr[i])
            companyImg.hnk_setImage(from: tempurl)
        } else {
            let tempurl = URL(string:idCompany)
            companyImg.hnk_setImage(from: tempurl)
        }
        
        self.profilePhotoContainer.layer.cornerRadius = self.profilePhotoContainer.frame.size.height/2
        self.profilePhotoContainer.clipsToBounds = true
        if !photoUser.isEmpty {
            let tempurl = URL(string:photoUser)
            iconPhoto.hnk_setImage(from: tempurl, placeholder: UIImage(named:"placeholder_photo"), success: { (image) in
                self.iconPhoto.image = image
                self.iconPhoto.layer.cornerRadius = self.iconPhoto.frame.size.height/2
                self.iconPhoto.clipsToBounds = true
            }, failure: { (err) in
                self.iconPhoto.backgroundColor = UIColor.backgroundPhotoColor
                self.iconPhoto.image = UIImage(named: "placeholder_photo")
                self.iconPhoto.layer.cornerRadius = 0
                self.iconPhoto.clipsToBounds = true
                
            })
        } else {
            iconPhoto.backgroundColor = UIColor.backgroundPhotoColor
            iconPhoto.image = UIImage(named: "placeholder_photo")
            iconPhoto.layer.cornerRadius = 0
            iconPhoto.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var msgBtn: UIButton!
    @IBOutlet weak var iconPhoto: UIImageView!
    @IBOutlet weak var companyImg: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var interestLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var profilePhotoContainer: UIView!
    @IBOutlet weak var interestsLabel: UILabel!
    @IBOutlet weak var contactlabel: UILabel!
    
    @IBOutlet weak var topC: NSLayoutConstraint!
    @IBOutlet weak var leadC: NSLayoutConstraint!
    @IBOutlet weak var trailC: NSLayoutConstraint!
    @IBOutlet weak var botC: NSLayoutConstraint!
}

extension PeopleDetailVC {

    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    @IBAction func whiteMsgsBtn(_ sender: Any) {
        if userId != Session.sharedInstance.userToken {
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
            openNewVC.conversationReceiverId = userId
            openNewVC.messagesWall = true
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
        }
    }
}

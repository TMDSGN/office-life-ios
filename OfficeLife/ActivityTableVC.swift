//
//  ActivityTableVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 23.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader
import Haneke

fileprivate struct ActivityData {
    
    var idArr, nameArr, dateArr, backgroundArr, typeArr, placeArr, createdByIdArr, createdByNameArr, createdByPhotoArr: [String]
    
    init(idArr: [String], nameArr: [String], dateArr: [String], backgroundArr: [String], typeArr: [String], placeArr: [String], createdByIdArr: [String], createdByNameArr: [String], createdByPhotoArr: [String]) {
        self.idArr = idArr
        self.nameArr = nameArr
        self.dateArr = dateArr
        self.backgroundArr = backgroundArr
        self.typeArr = typeArr
        self.placeArr = placeArr
        self.createdByIdArr = createdByIdArr
        self.createdByNameArr = createdByNameArr
        self.createdByPhotoArr = createdByPhotoArr
    }
}

class ActivityTableVC: HamburgerContainerVC {
    
    var refreshControl: UIRefreshControl!
    var storedOffsets = [Int: CGFloat]()
    var categoryArr = ["All", "Sport", "Fun", "Carpool", "Culture", "Networking", "Other"]
    var categoryIdArr = ["1", "19", "28", "22", "29", "20", "45"]
    
    fileprivate var selectedCategory = "1"
    fileprivate var loaderActive = true
    fileprivate var activityData = ActivityData(idArr: [], nameArr: [], dateArr: [], backgroundArr: [], typeArr: [], placeArr: [], createdByIdArr: [], createdByNameArr: [], createdByPhotoArr: [])

    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegister()
        registerObservers()
        setupPullToReload()
        title = "Activities".localized()
        getData(selectedCat: categoryIdArr[0])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIcon")
    }
    
    func refresh(sender:AnyObject) {
        self.loaderActive = false
        self.getData(selectedCat: self.selectedCategory)
    }
    
    func newActivitySuccess(){
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "GratzAlertVC") as! GratzAlertVC
        modal.type = 4
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy HH:mm"
        formatter.timeZone = TimeZone.current
        modal.dateStr = formatter.string(from: Date())
        
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: false, completion: nil)
        
        self.getData(selectedCat: self.selectedCategory)
    }
    
    fileprivate func getData(selectedCat:String){
        if Reachability.isConnectedToNetwork() {
            if loaderActive {
                SwiftLoader.show(animated: true)
            }
            ActivitiesReq().getActivitiesSpecificType(activityType: selectedCat, completionHandler: { (done, idArr, nameArr, dateArr, backgroundArr, typeArr, placeArr, createdByNameArr, createdByPhotoArr, createdByIdArr) in
                self.loaderActive = true
                self.refreshControl!.endRefreshing()
                SwiftLoader.hide()
                if done {
                    self.activityData = ActivityData(idArr: idArr, nameArr: nameArr, dateArr: dateArr, backgroundArr: backgroundArr, typeArr: typeArr, placeArr: placeArr, createdByIdArr: createdByIdArr, createdByNameArr: createdByNameArr, createdByPhotoArr: createdByPhotoArr)
                    self.reservationTable.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "MyActivityCellView", bundle: nil)
        reservationTable.register(nib, forCellReuseIdentifier: "MyActivityCell")
        reservationTable.estimatedRowHeight = 200
        reservationTable.separatorStyle = .none
        tableView.separatorStyle = .none
    }
    
    fileprivate func setupPullToReload(){
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        reservationTable.addSubview(refreshControl)
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(ActivityTableVC.newActivitySuccess), name: NSNotification.Name(rawValue: "newActivitySuccess"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "newActivitySuccess"), object: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reservationTable: UITableView!
}

extension ActivityTableVC {
    
    @IBAction func createNewActivityBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "CreateActivityVC") as! CreateActivityVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: false, completion: nil)
    }
}

extension ActivityTableVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        if tableView == reservationTable {
            let cell = self.reservationTable.dequeueReusableCell(withIdentifier: "MyActivityCell") as! MyActivityCellVC
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.titleLabel.text = activityData.nameArr[indexPath.row]
            cell.userNameLabel.text = activityData.createdByNameArr[indexPath.row]
            cell.userDateLabel.text = activityData.dateArr[indexPath.row]
            cell.userStreetLabel.text = activityData.placeArr[indexPath.row]
            if !activityData.createdByPhotoArr[indexPath.row].isEmpty {
                let tempurlC = URL(string: activityData.createdByPhotoArr[indexPath.row])
                cell.userProfileIcon.hnk_setImage(from: tempurlC)
                cell.userProfileIcon.layer.cornerRadius = cell.userProfileIcon.frame.size.height/2
                cell.userProfileIcon.clipsToBounds = true
            } else {
                cell.userProfileIcon.backgroundColor = UIColor.backgroundPhotoColor
                cell.userProfileIcon.image = UIImage(named: "placeholder_photo")
            }
            
            if !activityData.backgroundArr[indexPath.row].isEmpty {
                let tempurl = URL(string: activityData.backgroundArr[indexPath.row])
                cell.mainIcon.hnk_setImage(from: tempurl)
            } else {
                cell.mainIcon.backgroundColor = UIColor.clear
                cell.mainIcon.image = UIImage(named: "placeholder")
            }
            cell.activityIcon.image = UIImage(named: "activityType" + activityData.typeArr[indexPath.row])
            
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ActivityCategoryTableCellVC") as! ActivityCategoryTableCellVC
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == reservationTable {
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ActivityDetailVC") as! ActivityDetailVC
            openNewVC.activityId = activityData.idArr[indexPath.row]
            openNewVC.titleCategory = title!
            
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
            self.reservationTable.deselectRow(at: indexPath, animated: false)
        } else {
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView != reservationTable {
            guard let tableViewCell = cell as? ReservationCategoryVC else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView != reservationTable {
            guard let tableViewCell = cell as? ReservationCategoryVC else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == reservationTable {
            return self.activityData.idArr.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == reservationTable {
            return 280
        } else {
            return 50
        }
    }
}

extension ActivityTableVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 50
        var width:CGFloat = 50
        width = 100
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! ActivityCategoryCollectionCell
        
        cell.titleLabel.text = categoryArr[indexPath.item].localized()
        if selectedCategory == categoryIdArr[indexPath.row] {
            UIView.animate(withDuration: 0.1) {
                cell.underLineView.isHidden = false
            }
        } else {
            UIView.animate(withDuration: 0.1) {
                cell.underLineView.isHidden = true
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategory = categoryIdArr[indexPath.row]
        self.reservationTable.setContentOffset(CGPoint.zero, animated: false)
        DelayFunc.sharedInstance.delay(0.1) {
            collectionView.reloadData()
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.getData(selectedCat: self.selectedCategory)
        }
    }
}

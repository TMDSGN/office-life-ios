//
//  MsgsCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 27.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit

class MsgsCellVC: UITableViewCell {
    
    var imgArr = [String]()
    var userId: String = ""
    var detailId: String = ""
    var activityCell = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(MsgsCellVC.reload), name: NSNotification.Name(rawValue: "reload"), object: nil)
        // Initialization code
        blueContainerView.layer.cornerRadius = 5
        imgIcon.layer.cornerRadius = 20
        imgIcon.layer.borderWidth = 2
        imgIcon.layer.borderColor = UIColor.imgBorderColor.cgColor
        blueContainerView.backgroundColor = UIColor.mainBlue
        collectionVIew.delegate = self
        collectionVIew.dataSource = self
        imgIcon.clipsToBounds = true
        descriptionTextView.sizeToFit()
        let nib = UINib(nibName: "MsgsWallCollectionCellView", bundle: nil)
        collectionVIew.register(nib, forCellWithReuseIdentifier: "MsgsWallCollectionCell")
        collectionVIew.reloadData()
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reload"), object: nil)
    }
    
    func reload(){
        collectionVIew.reloadData()
    }
    
    @IBAction func openProfilBtn(_ sender: Any) {
        if !userId.isEmpty {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openPeopleProfile"), object: ["userId" : userId])
        }
    }
    
    @IBAction func openInDetail(_ sender: Any) {
        if !detailId.isEmpty && activityCell {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openActivityDetail"), object: ["detailId" : detailId])
        }
    }
    
    @IBOutlet weak var inboxMsgIcon: UIImageView!
    
    @IBOutlet weak var msgsCountLabel: UILabel!
    @IBOutlet weak var msgsCountIcon: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var blueContainerView: UIView!
    @IBOutlet weak var blueContainerTitleLabel: UILabel!
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgIconHeight: NSLayoutConstraint!
    
    @IBOutlet weak var collectionVIew: UICollectionView!
}

// MARK:- UICollectionView DataSource

extension MsgsCellVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MsgsWallCollectionCell",for:indexPath) as! MsgsWallCollectionCellVC
        if imgArr[indexPath.row].isEmpty {
            cell.imgIcon.image = UIImage()
        } else {
            let tempurl = URL(string: imgArr[indexPath.row])
            cell.imgIcon.hnk_setImage(from: tempurl)
        }
        return cell
    }
    
}

extension MsgsCellVC : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if imgArr.count > indexPath.row {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "showSelectedPhoto"), object: ["imgStr" : imgArr[indexPath.row]])
        }
    }
}

extension MsgsCellVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let size = self.frame.size.width/2-100
        let length = CGSize(width: size, height: size)
        return length
    }
}


extension MsgsCellVC {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionVIew.delegate = dataSourceDelegate
        collectionVIew.dataSource = dataSourceDelegate
        collectionVIew.tag = 1
        collectionVIew.setContentOffset(collectionVIew.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionVIew.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionVIew.contentOffset.x = newValue }
        get { return collectionVIew.contentOffset.x }
    }
}

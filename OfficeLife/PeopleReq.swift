//
//  PeopleReq.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

/*
 
 Type       cz          Id
 All        Všechno     1
 Company	Společnost	2
 Interests	Zájmy       3
 
 */

import Foundation
import Alamofire
import SwiftyJSON

class PeopleReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getPeople(company:String,interest:String, completionHandler: @escaping (Bool, [String], [String], [String], [String], [String], [String], [String]) -> ()) -> (){
        
        var url = urlDomain + "/users/v2/list"
        if !company.isEmpty {
            url = url + "/?company=\(company)"
        }
        if !interest.isEmpty {
            var newInterest = interest.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
            newInterest = newInterest.replacingOccurrences(of: ",", with: " ", options: .literal, range: nil)
            newInterest = newInterest.addingPercentEncoding( withAllowedCharacters: .urlHostAllowed)!
            url = url + "/?interest=\(newInterest)"
        }
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                var idArr = [String]()
                var nameArr = [String]()
                var surnameArr = [String]()
                var photoArr = [String]()
                var companyIdArr = [String]()
                var contactArr = [String]()
                var interestsArr = [String]()

                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let name = json[i]["name"].stringValue
                    let surname = json[i]["surname"].stringValue
                    let photo = json[i]["photo"].stringValue
                    let companyId = json[i]["company"].stringValue
                    let contact = json[i]["email"].stringValue

                    var interest: String = ""
                    var temp = [String]()
                    for j in 0..<json[i]["interests"].count {
                        if j == 0 {
                            temp.append(json[i]["interests"][j].stringValue)
                        } else {
                            temp.append("" + json[i]["interests"][j].stringValue)
                        }
                    }
                    interest = temp.joined(separator: ", ")

                    idArr.append(id)
                    nameArr.append(name)
                    surnameArr.append(surname)
                    photoArr.append(photo)
                    companyIdArr.append(companyId)
                    contactArr.append(contact)
                    interestsArr.append(interest)

                }
                
                completionHandler(true, idArr, nameArr, surnameArr, photoArr, companyIdArr, contactArr, interestsArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String](), [String](), [String]())
            }
        }
    }
    
    func getPeopleStaff(completionHandler: @escaping (Bool, [String], [String], [String], [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/users/v2/managment/"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                var idArr = [String]()
                var firstnameArr = [String]()
                var surnameArr = [String]()
                var emailArr = [String]()
                var phoneArr = [String]()
                var positionArr = [String]()
                var photoArr = [String]()

                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let firstname = json[i]["name"].stringValue
                    let surname = json[i]["surname"].stringValue
                    let email = json[i]["email"].stringValue
                    let phone = json[i]["phone"].stringValue
                    let position = json[i]["position"].stringValue
                    let photo = json[i]["photo"].stringValue

                    idArr.append(id)
                    firstnameArr.append(firstname)
                    surnameArr.append(surname)
                    emailArr.append(email)
                    phoneArr.append(phone)
                    positionArr.append(position)
                    photoArr.append(photo)
                }
                
                completionHandler(true, idArr, firstnameArr, surnameArr, emailArr, phoneArr, positionArr, photoArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String](), [String](), [String]())
            }
        }
    }

}

//
//  HomeMyActivitiesTableCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 14.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class HomeMyActivitiesTableCellVC: UITableViewCell {
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(HomeMyActivitiesTableCellVC.reloadCollection), name: NSNotification.Name(rawValue: "reloadMyActivityCollectionView"), object: nil)
        let nib = UINib(nibName: "HomeMyActivityCollectionCellView", bundle: nil)
        myActivityCollectionView.register(nib, forCellWithReuseIdentifier: "HomeMyActivityCollectionCell")
    }
    
    func reloadCollection(){
        myActivityCollectionView.reloadData()
    }
    
    @IBOutlet public weak var myActivityCollectionView: UICollectionView!
    
}

extension HomeMyActivitiesTableCellVC {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        myActivityCollectionView.delegate = dataSourceDelegate
        myActivityCollectionView.dataSource = dataSourceDelegate
        myActivityCollectionView.tag = 3
        myActivityCollectionView.setContentOffset(myActivityCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        myActivityCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { myActivityCollectionView.contentOffset.x = newValue }
        get { return myActivityCollectionView.contentOffset.x }
    }
}

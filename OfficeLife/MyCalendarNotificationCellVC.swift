//
//  MyCalendarNotificationCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 05.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

class MyCalendarNotificationCellVC: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        blueDot.layer.cornerRadius = blueDot.frame.size.height/2
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    @IBOutlet weak var blueDot: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

}

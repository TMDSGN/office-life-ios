//
//  UIImage+Resize.swift
//  OfficeLife
//
//  Created by Josef Antoni on 11.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

extension UIImage{
    func resizeWithWidth(_ width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    func toBase64() -> String{
        let imageData = UIImagePNGRepresentation(self)!
        return imageData.base64EncodedString(options: .lineLength64Characters)
    }
}

//
//  MyProfileVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 22.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import iOS_Slide_Menu
import SwiftLoader

class MyProfileVC: HamburgerContainerVC {
    
    fileprivate var imagePicker = UIImagePickerController()
    fileprivate var photoString: String = ""
    fileprivate let s = Session.sharedInstance
    
    override func viewDidLoad() {
        DelayFunc.sharedInstance.delay(0.1) {
            self.setupLabels()
            self.setupPhoto()
        }
        self.registerNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupLabels()
        setupConstants()
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIcon")
        self.adjustPhoto()
        getCredit()
    }
    
    fileprivate func getCredit(){
        if Reachability.isConnectedToNetwork() {
            ProfileReq().getCreditState { (done, credit) in
                self.creditLabel.text = credit.description
                self.s.profilePoints = credit.description
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func setupLabels(){
        creditLabelTitle.text = "Credit:".localized()
        interestsLabelTitle.text = "Interests:".localized()
        contactLabelTitle.text = "Contact:".localized()

        title = "My Profile".localized()
        
        self.nameLabel.text = s.profileFirstName + " " + s.profileSurName
        self.contactLabel.text = s.profileEmail
        self.creditLabel.text = s.profilePoints
        self.interestsLabel.text = s.profileInterest
        
        myActivitiesBtn.setTitle("My Activities".localized(), for: .normal)
        myReservationBtn.setTitle("My Reservations".localized(), for: .normal)
    }
    
    fileprivate func setupConstants(){
        self.heightOfWhiteBoxConstant.constant = self.whiteBoxView.frame.size.width-40
        self.firstViewHeightConstant.constant = (self.heightOfWhiteBoxConstant.constant/3)*2
        self.secondViewHeightConstant.constant = (self.heightOfWhiteBoxConstant.constant/3)
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0, animations: {
            self.imageIconWidth.constant = self.imageIconContainer.frame.size.height
            self.view.layoutIfNeeded()
        })
        self.imageIconContainer.layer.cornerRadius = self.imageIconContainer.frame.size.width/2
        self.imageIconContainer.clipsToBounds = true
    }
    
    //photo
    fileprivate func setupPhoto(){
        if !s.profilePhoto.isEmpty {
            let tempurl = URL(string: s.profilePhoto)
            self.imageIcon.hnk_setImage(from: tempurl)
            adjustPhoto()
        } else {
            self.imageIcon.backgroundColor = UIColor.backgroundPhotoColor
            self.imageIcon.image = UIImage(named:"placeholder_photo")
        }
    }
    
    fileprivate func adjustPhoto(){
        imgTop.constant = 0
        imgBot.constant = 0
        imgLeading.constant = 0
        imgTrailing.constant = 0
        self.imageIcon.contentMode = .scaleAspectFill
        self.imageIcon.layer.cornerRadius = self.imageIconContainer.frame.size.width/2
        self.imageIcon.layer.masksToBounds = true
    }
    
    fileprivate func pushToTables(_ isRezervation:Bool){
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileActivityTableVC") as! MyProfileActivityTableVC
        openNewVC.rezervationBool = isRezervation
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    fileprivate func sendPhoto(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileReq().putPhoto(photoStr: photoString) { (done) in
                if done {
                    ProfileReq().getMyProfileInfo(desiredProfileId: String(), completionHandler: { (done, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr, photoStr) in
                        SwiftLoader.hide()
                        if done {
                            let s = Session.sharedInstance
                            s.profileFirstName = name
                            s.profileSurName = surname
                            s.profileEmail = email
                            s.profilePhone = phone
                            s.profileCompany = company
                            s.profileFeedFromMe = feedFromMe
                            s.profilePhoto = photo
                            s.profileInterest = interestsArr.joined(separator: "")
                            self.setupLabels()
                            self.setupPhoto()
                            self.adjustPhoto()
                        } else {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                        }
                    })
                } else {
                    SwiftLoader.hide()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.openLibrary), name: NSNotification.Name(rawValue: "openLibrary"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openCamera), name: NSNotification.Name(rawValue: "openCamera"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openLibrary"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openCamera"), object: nil)
    }
    
    @IBOutlet weak var imageIconContainer: UIView!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var whiteBoxView: UIView!
    @IBOutlet weak var imageIconWidth: NSLayoutConstraint!
    @IBOutlet weak var heightOfWhiteBoxConstant: NSLayoutConstraint!
    @IBOutlet weak var firstViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var secondViewHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var interestsLabel: UILabel!
    
    @IBOutlet weak var imgTop: NSLayoutConstraint!
    @IBOutlet weak var imgTrailing: NSLayoutConstraint!
    @IBOutlet weak var imgBot: NSLayoutConstraint!
    @IBOutlet weak var imgLeading: NSLayoutConstraint!
    
    @IBOutlet weak var myActivitiesBtn: UIButton!
    @IBOutlet weak var myReservationBtn: UIButton!
    
    @IBOutlet weak var creditLabelTitle: UILabel!
    @IBOutlet weak var interestsLabelTitle: UILabel!
    @IBOutlet weak var contactLabelTitle: UILabel!
    
}

extension MyProfileVC {

    @IBAction func prefferencesBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfilePreferencesVC") as! MyProfilePreferencesVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func ordersBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersTableVC") as! MyOrdersTableVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func activitiesBtn(_ sender: Any) {
        pushToTables(false)
    }
    
    @IBAction func rezervationsBtn(_ sender: Any) {
        pushToTables(true)
    }
    
    @IBAction func addCreditsBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "AddCreditVC") as! AddCreditVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: false, completion: nil)
    }
    
    @IBAction func pickPhotoBtn(_ sender: Any) {
        if !Session.sharedInstance.facebookAcc {
            let modal = self.storyboard?.instantiateViewController(withIdentifier: "TypePickerVC") as! TypePickerVC
            let navigationController = UINavigationController(rootViewController: modal)
            navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(navigationController, animated: false, completion: nil)
        }
    }
}

extension MyProfileVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @objc fileprivate func openLibrary(){
        DelayFunc.sharedInstance.delay(0.3) {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                // self.imagePicker.navigationBar.barTintColor = self.orange
                self.imagePicker.navigationBar.isTranslucent = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    @objc fileprivate func openCamera(){
        DelayFunc.sharedInstance.delay(0.3) {
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.navigationBar.isTranslucent = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        imageIcon.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let img = imageIcon.image {
            imageIcon.image = img.resizeWithWidth(600)
            imageIcon.layer.borderWidth = 1.0
            imageIcon.layer.masksToBounds = false
            imageIcon.layer.borderColor = UIColor.white.cgColor
            imageIcon.clipsToBounds = true
            imageIcon.contentMode = .scaleAspectFill
            photoString = img.resizeWithWidth(600)!.toBase64()
            sendPhoto()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: nil)
        
        if let img = imageIcon.image {
            imageIcon.image = img.resizeWithWidth(600)
            imageIcon.layer.borderWidth = 1.0
            imageIcon.layer.masksToBounds = false
            imageIcon.layer.borderColor = UIColor.white.cgColor
            imageIcon.clipsToBounds = true
            imageIcon.contentMode = .scaleAspectFill
            photoString = img.resizeWithWidth(600)!.toBase64()
            sendPhoto()
        }
    }
    
}

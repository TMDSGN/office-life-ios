//
//  Calendar.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CalendarReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getMonth(from:String, to:String, completionHandler: @escaping (Bool, [String], [String], [String], [String], [String], [Bool], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/calendar/v1/list"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "from" : from,
            "to" : to
        ]
        
        Alamofire.request(url, method: .get, parameters:para, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                var toArr = [String]()
                var titleArr = [String]()
                var typeArr = [String]()
                var fromDateArr = [String]()
                var fromTimeArr = [String]()
                var idArr = [String]()
                var readArr = [Bool]()
                var parrentIdArr = [String]()

                for i in 0..<json.count{
                    let title = json[i]["title"].stringValue
                    let type = json[i]["type"].stringValue
                    let from = json[i]["from"].doubleValue
                    let to = json[i]["to"].doubleValue
                    let id = json[i]["id"].stringValue
                    let read = json[i]["read"].boolValue
                    let parretnId = json[i]["parent_id"].stringValue

                    toArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: to, format: "dd:MM"))
                    fromDateArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: from, format: "d.M.yyyy"))
                    fromTimeArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: from, format: "dd:MM"))
                    typeArr.append(type)
                    titleArr.append(title)
                    idArr.append(id)
                    readArr.append(read)
                    parrentIdArr.append(parretnId)
                }
                
                completionHandler(true, fromDateArr, fromTimeArr, toArr, titleArr, typeArr, readArr, idArr, parrentIdArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String](), [Bool](), [String](), [String]())
            }
        }
    }
    
    func readDay(id:String, parrentID:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/calendar/v1/\(id)"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        var para = [String:Any]()
        if !parrentID.isEmpty {
            para = [
                "parent_id":parrentID
            ]
        }
        Alamofire.request(url, method: .put, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 204 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
}

//
//  HamburgerContainerCVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 28.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import iOS_Slide_Menu

class HamburgerContainerCVC: UICollectionViewController {

    var isMenuOpen = false
    var chatImage: String = ""
    var notifiImage: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        registerObservers()
    }
    
    func setupNavBar(_ chatImage: String, _ notifiImage:String){
        var temp: String = chatImage
        if Session.sharedInstance.newFeedNotification {
            temp = temp + "New"
        }
        let leftCustomBtn1 = UIButton(type: .custom)
        leftCustomBtn1.setImage(UIImage(named: temp), for: .normal)
        leftCustomBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        leftCustomBtn1.addTarget(self, action: #selector(openMsgsTableView), for: .touchUpInside)
        let msgsBtn = UIBarButtonItem(customView: leftCustomBtn1)
        self.chatImage = temp
        
        temp = notifiImage
        if Session.sharedInstance.newListNotification {
            temp = temp + "New"
        }
        let leftCustomBtn2 = UIButton(type: .custom)
        leftCustomBtn2.setImage(UIImage(named: temp), for: .normal)
        leftCustomBtn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        leftCustomBtn2.addTarget(self, action: #selector(openNotificationView), for: .touchUpInside)
        let notifyBtn = UIBarButtonItem(customView: leftCustomBtn2)
        self.notifiImage = temp
        
        navigationItem.rightBarButtonItems = [notifyBtn, msgsBtn]
    }
    
    func refreshNavBar(){
        var temp: String = chatImage
        if Session.sharedInstance.newFeedNotification {
            temp = temp + "New"
        }
        let leftCustomBtn1 = UIButton(type: .custom)
        leftCustomBtn1.setImage(UIImage(named: temp), for: .normal)
        leftCustomBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        leftCustomBtn1.addTarget(self, action: #selector(openMsgsTableView), for: .touchUpInside)
        let msgsBtn = UIBarButtonItem(customView: leftCustomBtn1)
        
        temp = notifiImage
        if Session.sharedInstance.newListNotification {
            temp = temp + "New"
        }
        let leftCustomBtn2 = UIButton(type: .custom)
        leftCustomBtn2.setImage(UIImage(named: temp), for: .normal)
        leftCustomBtn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        leftCustomBtn2.addTarget(self, action: #selector(openNotificationView), for: .touchUpInside)
        let notifyBtn = UIBarButtonItem(customView: leftCustomBtn2)
        
        navigationItem.rightBarButtonItems = [notifyBtn, msgsBtn]
    }
    
    func openNotificationView(){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC")
        
        DispatchQueue.main.async(execute: { () -> Void in
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        });
    }
    
    func openMsgsTableView(){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "MsgsWallVC")
        
        DispatchQueue.main.async(execute: { () -> Void in
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        });
    }
    
    func iconBtn(){
        SlideNavigationController.sharedInstance().leftBarButtonItem = nil
        DelayFunc.sharedInstance.delay(0.1) {
            if self.isMenuOpen {
                let leftCustomBtn = UIButton(type: .custom)
                leftCustomBtn.setImage(UIImage(named: "Burger"), for: .normal)
                leftCustomBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                SlideNavigationController.sharedInstance().leftBarButtonItem = UIBarButtonItem(customView: leftCustomBtn)
                
            } else {
                let leftCustomBtn = UIButton(type: .custom)
                leftCustomBtn.setImage(UIImage(named: "Close"), for: .normal)
                leftCustomBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                SlideNavigationController.sharedInstance().leftBarButtonItem = UIBarButtonItem(customView: leftCustomBtn)
            }
            self.isMenuOpen = !self.isMenuOpen
        }
    }
    
    func setupDefaultMenuButton(){
        let leftCustomBtn = UIButton(type: .custom)
        leftCustomBtn.setImage(UIImage(named: "Burger"), for: .normal)
        leftCustomBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        leftCustomBtn.addTarget(SlideNavigationController.sharedInstance(), action: #selector(toggleLeftMenu), for: .touchUpInside)
        leftCustomBtn.addTarget(self, action: #selector(iconBtn), for: .touchUpInside)
        SlideNavigationController.sharedInstance().leftBarButtonItem = UIBarButtonItem(customView: leftCustomBtn)
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    func toggleLeftMenu() {
        self.toggleLeftMenu()
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshNavBar), name: NSNotification.Name(rawValue: "refreshNavBar"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadMyReservation"), object: nil)
    }
}

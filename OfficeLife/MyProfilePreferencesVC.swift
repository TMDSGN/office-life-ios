//
//  RezervationpreferencesVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 22.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader
import DropDown

class MyProfilePreferencesVC: UIViewController, AlertMessage {
    
    fileprivate var isSwitcherOn = true
    fileprivate var selectedCompanyId: String = ""
    
    fileprivate let dropDown = DropDown()
    fileprivate let s = Session.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLabels()
        getCompanies()
        nameField.text = s.profileFirstName
        surnameField.text = s.profileSurName
        mailField.text = s.profileEmail
        phoneField.text = s.profilePhone
        interestText.text = s.profileInterest
        saveBtn.setTitle("Save".localized(), for: .normal)
        if s.profileFeedFromMe {
            isSwitcherOn = true
            self.switcherDot.alpha = 1
        } else {
            isSwitcherOn = false
            self.switcherDot.alpha = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDropDown()
    }
    
    fileprivate func getCompanies(){
        if self.s.nameOfCompaniesArr.count != 0 {
            self.dropDown.dataSource = self.s.nameOfCompaniesArr
            if let i = self.s.idOfCompaniesArr.index(of: self.s.profileCompany) {
                self.companyLabel.text = self.s.nameOfCompaniesArr[i]
                self.selectedCompanyId = self.s.profileCompany
            }
        } else {
            getCompaniesAgain()
        }
    }
    
    fileprivate func getCompaniesAgain(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            LoginReq().getCompanies { (done, idArr, nameArr, iconsArr, descArr, contactNameArr, contactEmailArr, contactPhotoArr) in
                SwiftLoader.hide()
                if done {
                    let s = Session.sharedInstance
                    s.idOfCompaniesArr = idArr
                    s.nameOfCompaniesArr = nameArr
                    s.iconsOfCompaniesArr = iconsArr
                    s.descOfCompaniesArr = descArr
                    self.dropDown.dataSource = nameArr
                    print(self.s.profileCompany)
                    print(self.s.idOfCompaniesArr)
                    if let i = self.s.idOfCompaniesArr.index(of: self.s.profileCompany) {
                        self.companyLabel.text = self.s.nameOfCompaniesArr[i]
                        self.selectedCompanyId = self.s.profileCompany
                    }
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func back(){
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    fileprivate func setupDropDown(){
        self.dropDown.anchorView = self.companyView
        self.dropDown.direction = .bottom
        self.dropDown.bottomOffset = CGPoint(x: 0, y:self.companyView.bounds.height)
        self.dropDown.textFont = UIFont.systemFont(ofSize: 11)
        self.dropDown.backgroundColor = .white
    }
    
    fileprivate func setupLabels(){
        let tempColor = UIColor.mainBlue
        
        self.nameView.layer.borderWidth = 1
        self.nameView.layer.borderColor = tempColor.cgColor
        
        self.surnameView.layer.borderWidth = 1
        self.surnameView.layer.borderColor = tempColor.cgColor
        
        self.emailView.layer.borderWidth = 1
        self.emailView.layer.borderColor = tempColor.cgColor
        
        self.phoneView.layer.borderWidth = 1
        self.phoneView.layer.borderColor = tempColor.cgColor
        
        self.companyView.layer.borderWidth = 1
        self.companyView.layer.borderColor = tempColor.cgColor
        
        self.interestsView.layer.borderWidth = 1
        self.interestsView.layer.borderColor = tempColor.cgColor
        
        self.switcherContainer.layer.borderWidth = 0.5
        self.switcherContainer.layer.borderColor = tempColor.cgColor
        self.switcherContainer.layer.cornerRadius = 10
        
        self.switcherDot.backgroundColor = tempColor
        self.switcherDot.layer.cornerRadius = 8
        
        title = "My profile - Setings".localized()
        titleLabel.text = "Preferences".localized()
        allowFeedBtn.text = "Allow feed only from joined activites".localized()
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var allowFeedBtn: UILabel!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var surnameView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var interestsView: UIView!
    @IBOutlet weak var switcherContainer: UIView!
    @IBOutlet weak var switcherDot: UIView!
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var surnameField: UITextField!
    @IBOutlet weak var mailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var companyLabel: UITextField!
    @IBOutlet weak var interestText: UITextField!
    
    @IBOutlet weak var saveBtn: UIButton!
}

extension MyProfilePreferencesVC {
    
    @IBAction func updateProfileBtn(_ sender: Any) {
        self.view.endEditing(true)
        let tempStr = interestText.text!.replacingOccurrences(of: " ,", with: ",").replacingOccurrences(of: ", ", with: ",").replacingOccurrences(of: " , ", with: ",").replacingOccurrences(of: " ", with: ",")
        let tempArr:[String] = tempStr.components(separatedBy: ",")
        if Reachability.isConnectedToNetwork() {
            if !selectedCompanyId.isEmpty {
                SwiftLoader.show(animated: true)
                ProfileReq().updateMyProfileInfo(name: self.nameField.text!, surname: self.surnameField.text!, email: self.mailField.text!, phone: self.phoneField.text!, company: selectedCompanyId, feedFromMe: isSwitcherOn, interestsArr: tempArr) { (done, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr) in
                    SwiftLoader.hide()
                    if done {
                        let s = Session.sharedInstance
                        s.profileFirstName = name
                        s.profileSurName = surname
                        s.profileEmail = email
                        s.profilePhone = phone
                        s.profileCompany = company
                        s.profileFeedFromMe = feedFromMe
                        s.profilePhoto = photo
                        s.profilePoints = points
                        s.profileInterest = interestsArr.joined(separator: "")
                        
                        self.nameField.text = name
                        self.surnameField.text = surname
                        self.mailField.text = email
                        self.phoneField.text = phone
                        if let i = self.s.idOfCompaniesArr.index(of: company) {
                            self.companyLabel.text = self.s.nameOfCompaniesArr[i]
                        }
                        self.interestText.text = interestsArr.joined(separator: "")
                        if feedFromMe {
                            self.isSwitcherOn = true
                            self.switcherDot.alpha = 1
                        } else {
                            self.isSwitcherOn = false
                            self.switcherDot.alpha = 0
                        }
                        
                        let alert = UIAlertController(title: "Done".localized(), message: "Profile information has been updated".localized(), preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                            self.back()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                    }
                }
            } else {
                self.showSimpleAlert(title: "Error".localized(), message: "Select your company.".localized())
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @IBAction func switcherPressedBtn(_ sender: Any) {
        if isSwitcherOn {
            isSwitcherOn = false
            UIView.animate(withDuration: 1, animations: {
                self.switcherDot.alpha = 0
            })
        } else {
            isSwitcherOn = true
            UIView.animate(withDuration: 1, animations: {
                self.switcherDot.alpha = 1
            })
        }
    }
    
    @IBAction func dropDownBtn(_ sender: Any) {
        if dropDown.isHidden {
            dropDown.show()
        }
        self.dropDown.selectionAction = { [unowned self] (index, item) in
            self.companyLabel.text = item
            self.selectedCompanyId = self.s.idOfCompaniesArr[index]
        }
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        back()
    }
}

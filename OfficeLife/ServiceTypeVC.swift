//
//  ServiceTypeVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 02.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import EMPageViewController

class ServiceTypeVC: HamburgerContainerVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIcon")
    }
    
    fileprivate func push(_ isFoodView:Bool){
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ServiceContainerVC") as! ServiceContainerVC
        openNewVC.isFoodView = isFoodView
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    fileprivate func setupLabels(){
        title = "Services".localized()
        foodLabel.text = "Food and beverages".localized()
        otherLabel.text = "Other".localized()
    }
    
    @IBOutlet weak var foodLabel: UILabel!
    @IBOutlet weak var otherLabel: UILabel!
}

extension ServiceTypeVC {

    @IBAction func foodBtn(_ sender: Any) {
        push(true)
    }
    
    @IBAction func otherBtn(_ sender: Any) {
        push(false)
    }
}

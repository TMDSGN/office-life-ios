//
//  OccupiersCVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 28.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader
import Haneke

fileprivate struct OccupiersData {
    
    var idArr, nameArr, iconsArr, descArr, contactNameArr, contactPhotoArr, contactEmailArr: [String]
    
    init(idArr: [String], nameArr: [String], iconsArr: [String], descArr: [String], contactNameArr: [String], contactPhotoArr: [String], contactEmailArr: [String]) {
        self.idArr = idArr
        self.nameArr = nameArr
        self.iconsArr = iconsArr
        self.descArr = descArr
        self.contactNameArr = contactNameArr
        self.contactPhotoArr = contactPhotoArr
        self.contactEmailArr = contactEmailArr
    }
}

class OccupiersCVC: HamburgerContainerCVC {
    
    fileprivate var occupiersData = OccupiersData(idArr: [], nameArr: [], iconsArr: [], descArr: [], contactNameArr: [], contactPhotoArr: [], contactEmailArr: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaultMenuButton()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar("chatIcon", "notificationIcon")
        setupCollectionLayout()
        title = "Tenants".localized()
    }
    
    fileprivate func getData(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            LoginReq().getCompanies(completionHandler: { (done, idArr, nameArr, iconsArr, descArr, contactNameArr, contactEmailArr, contactPhotoArr) in
                SwiftLoader.hide()
                if done {
                    //set value to occupies
                    self.occupiersData = OccupiersData(idArr: idArr, nameArr: nameArr, iconsArr: iconsArr, descArr: descArr, contactNameArr: contactNameArr, contactPhotoArr: contactPhotoArr, contactEmailArr: contactEmailArr)
                    
                    //save global variable to company
                    let s = Session.sharedInstance
                    s.idOfCompaniesArr = idArr
                    s.nameOfCompaniesArr = nameArr
                    s.iconsOfCompaniesArr = iconsArr
                    s.descOfCompaniesArr = descArr
                    
                    self.collectionView?.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
        
    }
    
    fileprivate func setupCollectionLayout(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15)
        layout.itemSize = CGSize(width: width / 2 - 23, height: 300)
        layout.minimumInteritemSpacing = 15
        layout.minimumLineSpacing = 15
        collectionView!.collectionViewLayout = layout
    }
}

extension OccupiersCVC {
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OccupiersCell", for: indexPath) as! OccupiersCellVC
        cell.descLabel.text = self.occupiersData.descArr[indexPath.row].firstCharacterUpperCase()
        cell.userName.text = self.occupiersData.contactNameArr[indexPath.row]
        cell.email.text = self.occupiersData.contactEmailArr[indexPath.row]
        if !self.occupiersData.contactPhotoArr[indexPath.row].isEmpty {
            let tempurl = URL(string: self.occupiersData.contactPhotoArr[indexPath.row])
            cell.userIcon.hnk_setImage(from: tempurl)
        } else {
            cell.userIcon.image = UIImage(named: "placeholder_photo")
        }
        cell.userIcon.layer.cornerRadius = cell.userIcon.frame.size.height/2
        cell.userIcon.clipsToBounds = true
        if !self.occupiersData.iconsArr[indexPath.row].isEmpty {
            let tempurl = URL(string: self.occupiersData.iconsArr[indexPath.row])
            cell.imgIcon.hnk_setImage(from: tempurl)
        } else {
            cell.imgIcon.image = UIImage(named: "placeholder_photo")
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.occupiersData.idArr.count
    }
}

extension String {
    func firstCharacterUpperCase() -> String {
        if let firstCharacter = characters.first, characters.count > 0 {
            return replacingCharacters(in: startIndex ..< index(after: startIndex), with: String(firstCharacter).uppercased())
        }
        return self
    }
}

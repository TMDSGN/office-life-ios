//
//  WebViewGoPayVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 16.03.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class WebViewGoPayVC: UIViewController {
    
    var targetUrl: String = ""
    
    override func viewDidLoad() {
        navigationController?.isNavigationBarHidden = false
        title = "GoPay"
        webView.delegate = self
        if let url = URL(string: targetUrl) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
     
    @IBOutlet weak var webView: UIWebView!
}

extension WebViewGoPayVC {

    @IBAction func backBtn(_ sender: Any) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

extension WebViewGoPayVC: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if request.url?.description.range(of:"officelife.re") != nil{
            //pokud platba proběhla tak nás to hodí na officeLife doménu a v tu chvilku se vracíme z webview zpátky do appky
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "iPaidAndWantToGoBack"), object: nil)
            if let navigationController = self.navigationController {
                navigationController.popViewController(animated: true)
            }
        }
        return true
    }
}

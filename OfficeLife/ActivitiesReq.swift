//
//  ActivitiesPreview.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

/*
 
 Category       cz              id
 MyActivites	Moje aktivity	0
 All            Všechno         1
 Sport          Sport           2
 Networking     Networking      3
 Carpool        Spolujízda      5
 Culture        Kultura         6
 Fun            Zábava          7
 Other          Ostatní         8
 
 */

import Foundation
import Alamofire
import SwiftyJSON
import Localize_Swift

class ActivitiesReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getActivitiesPreview(type:String, completionHandler: @escaping (Bool, [String], [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/activity/v1/home/\(type)"
        let lang = Localize.currentLanguage()
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "Language" : lang
        ]
        let para = [
            "number" : 6
        ]
        
        Alamofire.request(url, method: .get, parameters:para, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                var idArr = [String]()
                var nameArr = [String]()
                var dateArr = [String]()
                var backgroundArr = [String]()
                var typeArr = [String]()
                
                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let name = json[i]["name"].stringValue
                    let date = json[i]["date"].doubleValue
                    let background = json[i]["background"].stringValue
                    let type = json[i]["type"].stringValue
                    
                    idArr.append(id)
                    nameArr.append(name)
                    dateArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: date, format: "dd.MM.yyyy HH:mm"))
                    backgroundArr.append(background)
                    typeArr.append(type)
                }
                
                completionHandler(true, idArr, nameArr, dateArr, backgroundArr, typeArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String]())
            }
        }
    }
    
    func getActivitiesSpecificType(activityType:String, completionHandler: @escaping (Bool, [String], [String], [String], [String], [String], [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/activity/v1/type/\(activityType)"
        let lang = Localize.currentLanguage()
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "Language" : lang
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                var idArr = [String]()
                var nameArr = [String]()
                var dateArr = [String]()
                var backgroundArr = [String]()
                var typeArr = [String]()
                
                var placeArr = [String]()
                var createdByIdArr = [String]()
                var createdByNameArr = [String]()
                var createdByPhotoArr = [String]()
                
                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let name = json[i]["name"].stringValue
                    let date = json[i]["date"].doubleValue
                    let background = json[i]["background"].stringValue
                    let type = json[i]["type"].stringValue
                    let place = json[i]["place"].stringValue
                    let createdById = json[i]["created_by"]["user_id"].stringValue
                    let createdByName = json[i]["created_by"]["name"].stringValue
                    let createdByPhoto = json[i]["created_by"]["photo"].stringValue
                    
                    idArr.append(id)
                    nameArr.append(name)
                    dateArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: date, format: "dd.MM.yyyy HH:mm"))
                    backgroundArr.append(background)
                    typeArr.append(type)
                    placeArr.append(place)
                    createdByIdArr.append(createdById)
                    createdByNameArr.append(createdByName)
                    createdByPhotoArr.append(createdByPhoto)
                }
                
                completionHandler(true, idArr, nameArr, dateArr, backgroundArr, typeArr, placeArr, createdByNameArr, createdByPhotoArr, createdByIdArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String](), [String](), [String](), [String](), [String]())
            }
        }
    }
    
    func getActivitiesDetail(activityId:String, completionHandler: @escaping (Bool, String, String, Double, Double, String, String, String, String, Bool, [String], [String], [String], Bool, Bool, String, String, String, String, String) -> ()) -> (){
        
        let url = urlDomain + "/activity/v1/detail/\(activityId)"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                let id = json["id"].stringValue
                let name = json["name"].stringValue
                let date = json["date"].doubleValue
                let dateTo = json["date_to"].doubleValue
                let background = json["background"].stringValue
                let type = json["type"].stringValue
                let place = json["place"].stringValue
                let desc = json["description"].stringValue
                let joined = json["joined"].boolValue
                let latitude = json["latitude"].stringValue
                let longitude = json["longitude"].stringValue
                let createdUserId = json["created_by"]["user_id"].stringValue
                let onlyVisibleToPeopleFromMyCompany = json["public"].boolValue
                var createdByMe = false
                if Session.sharedInstance.userToken == createdUserId {
                    createdByMe = true
                }
                let destination = json["destination"].stringValue
                let latitudeTo = json["latitude_to"].stringValue
                let longitudeTo = json["longitude_to"].stringValue

                var joinedUsersId = [String]()
                var joinedUsersName = [String]()
                var joinedUsersPhoto = [String]()
                
                for i in 0..<json["users_in"].count{
                    let id = json["users_in"][i]["user_id"].stringValue
                    let name = json["users_in"][i]["name"].stringValue
                    let photo = json["users_in"][i]["photo"].stringValue
                    joinedUsersId.append(id)
                    joinedUsersName.append(name)
                    joinedUsersPhoto.append(photo)
                }
                
                completionHandler(true, id, name, date, dateTo, background, type, place, desc, joined, joinedUsersId, joinedUsersName, joinedUsersPhoto, createdByMe, onlyVisibleToPeopleFromMyCompany, latitude, longitude, destination, latitudeTo, longitudeTo)
            } else {
                completionHandler(false, String(), String(), Double(), Double(), String(), String(), String(), String(), Bool(), [String](), [String](), [String](), false, false, String(), String(), String(), String(), String())
            }
        }
    }
    
    func joinOrLeaveActivity(activityId:String, join:Bool, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/activity/v1/join/\(activityId)"
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "join" : join
        ]
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func getActivitiesWall(activityId:String, completionHandler: @escaping (Bool, [String], [String], [String], [String], [Bool], [Date], [String], [[String]], [[String]], [[String]], [[String]], [[String]]) -> ()) -> (){
        
        let url = urlDomain + "/activity/v1/activity_wall/\(activityId)"
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                var idArr = [String]()
                var postedByIdArr = [String]()
                var postedByNameArr = [String]()
                var postedByPhotoArr = [String]()
                var myPostArr = [Bool]()
                var dateArr = [Date]()
                var textArr = [String]()
                var comentsPhotoArr = [[String]]()
                var attachmentComentsTextArr = [[String]]()
                var attachmentComentsPhotoArr = [[String]]()
                var attachmentComentsCreatedArr = [[String]]()
                var attachmentComentsUserIdArr = [[String]]()

                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let postedById = json[i]["posted_by"]["user_id"].stringValue
                    let postedByName = json[i]["posted_by"]["name"].stringValue
                    let postedByPhoto = json[i]["posted_by"]["photo"].stringValue
                    let myPost = json[i]["my_post"].boolValue
                    let date = json[i]["date"].doubleValue
                    let text = json[i]["text"].stringValue

                    idArr.append(id)
                    postedByIdArr.append(postedById)
                    postedByNameArr.append(postedByName)
                    postedByPhotoArr.append(postedByPhoto)
                    myPostArr.append(myPost)
                    dateArr.append(DateConvertor.sharedInstance.getDate(timeStamp: date, format: "dd.MM.yyyy HH:mm"))
                    textArr.append(text)

                    var comPhotoArr = [String]()

                    for j in 0..<json[i]["photos"].count{
                        let photo = json[i]["photos"][j].stringValue
                        comPhotoArr.append(photo)
                    }
                    
                    var tempcomTextArr = [String]()
                    var tempcomPhotoArr = [String]()
                    var comUserIdArr = [String]()
                    var comCreatedArr = [String]()
                    
                    for j in 0..<json[i]["comments"].count{
                        let text = json[i]["comments"][j]["text"].stringValue
                        let photo = json[i]["comments"][j]["user_photo"].stringValue
                        let created = json[i]["comments"][j]["created"].doubleValue
                        let userId = json[i]["comments"][j]["user_id"].stringValue

                        tempcomTextArr.append(text)
                        tempcomPhotoArr.append(photo)
                        comCreatedArr.append(DateConvertor.sharedInstance.timeAgoSinceDate(date: DateConvertor.sharedInstance.getDate(timeStamp: created, format: "dd.MM.yyyy HH:mm"), numericDates: true))
                        comUserIdArr.append(userId)
                    }
                    attachmentComentsTextArr.append(tempcomTextArr)
                    attachmentComentsPhotoArr.append(tempcomPhotoArr)
                    attachmentComentsCreatedArr.append(comCreatedArr)
                    attachmentComentsUserIdArr.append(comUserIdArr)

                    comentsPhotoArr.append(comPhotoArr)
                    
                }
                
                completionHandler(true, idArr, postedByIdArr, postedByNameArr, postedByPhotoArr, myPostArr, dateArr, textArr, comentsPhotoArr, attachmentComentsTextArr, attachmentComentsPhotoArr, attachmentComentsCreatedArr, attachmentComentsUserIdArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [Bool](), [Date](), [String](), [[String]](), [[String]](), [[String]](), [[String]](), [[String]]())
            }
        }
    }
    
    func createNewActivityWallPost(activityId:String, text:String, photo:[String], completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/activity/v1/activity_wall/\(activityId)"
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        var para = [
            "text" : text
        ] as [String : Any]
        if photo != [String]() {
            para = [
                "text" : text,
                "photos" : photo
            ]
        }
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func createNewActivityWallComent(activityId:String, postId:String, text:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/activity/v1/activity_wall/\(activityId)/\(postId)"
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "text" : text
        ]
        
        Alamofire.request(url, method: .put, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func createNewActivity(titleName: String, date: Double, dateTo: Double, photo: String, category: Int, description: String, isPublic: Bool, address: String, latitude: String, longitude: String, destinationAddress: String, destinationlatitude: String, destinationlongitude: String, capacity: Int, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/activity/v1/new/"
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        var para = [
            "title" : titleName,
            "date" : date,
            "date_to" : dateTo,
            "category" : category,
            "photo" : photo,
            "description" : description,
            "public": isPublic.description,
            "address" : address,
            "latitude" : latitude,
            "longitude" : longitude,
            "capacity" : capacity
        ] as [String : Any]
        
        if !destinationAddress.isEmpty {
            para["destination"] = destinationAddress
            para["latitude_to"] = destinationlatitude
            para["longitude_to"] = destinationlongitude
        }
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func editActivity(activityId:String, date:Double, dateTo:Double, category:Int, titleName:String, address:String, description:String, photo:String, isPublic:Bool, latitude:String, longitude:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/activity/v1/detail/\(activityId)"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "title" : titleName,
            "date" : date,
            "dateTo" : dateTo,
            "category" : category,
            "photo" : photo,
            "description" : description,
            "public": isPublic.description,
            "address" : address,
            "latitude" : latitude,
            "longitude" : longitude
            ] as [String : Any]

        Alamofire.request(url, method: .put, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
}

//
//  ServiceCategoryVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 20.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

class ServiceCategoryVC: UITableViewCell {
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(HomeActivityTableCellVC.reloadCollection), name: NSNotification.Name(rawValue: "reloadServiceCell"), object: nil)
    }
    
    func reloadCollection(){
        collectionView.reloadData()
    }
    
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    
}

extension ServiceCategoryVC {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}

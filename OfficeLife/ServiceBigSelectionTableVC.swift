//
//  ServiceBigSelectionTableVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 04.03.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class ServiceBigSelectionTableVC: UIViewController, AlertMessage {
    
    var showCheckBoxView = false
    var addOnTitle: [String] = []
    var addonPrice: [String] = []
    var addonsName: String = ""
    var totalProducts: Double = 0.0
    
    var productTitleForSentArray: [String] = []
    var productPriceForSentArray: [Double] = []
    var productQuantityForSentArray: [Int] = []
    
    fileprivate var countNumber: Double = 0.0
    fileprivate var totalPrice: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObservers()
        customCellRegister()
        titleOfScreen.text = addonsName
        orderBtn.setTitle("Order".localized(), for: .normal)
        self.navigationController?.navigationBar.isHidden = true
        tableView.reloadData()
    }
    
    @objc fileprivate func countMultiplier(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let titleOfProduct: String = dict["title"] as! String
        var prizePerProduct: Double = dict["prizePerProduct"] as! Double
        let quantityOfProduct: Int = dict["quantity"] as! Int
        let adding: Bool = dict["adding"] as! Bool
        
        let oldValue = countNumber
        self.countNumber = prizePerProduct + oldValue
        
        if prizePerProduct < 0.0 {
            prizePerProduct = prizePerProduct * (-1)
        }
        if adding {
            if let index = productTitleForSentArray.index(of: titleOfProduct) {
                productTitleForSentArray[index] = titleOfProduct
                productPriceForSentArray[index] = prizePerProduct
                productQuantityForSentArray[index] = quantityOfProduct
            } else {
                productTitleForSentArray.append(titleOfProduct)
                productPriceForSentArray.append(prizePerProduct)
                productQuantityForSentArray.append(quantityOfProduct)
            }
        } else {
            if let index = productTitleForSentArray.index(of: titleOfProduct) {
                productTitleForSentArray[index] = titleOfProduct
                productPriceForSentArray[index] = prizePerProduct
                productQuantityForSentArray[index] = quantityOfProduct
                
                if quantityOfProduct == 0 {
                    productTitleForSentArray.remove(at: index)
                    productPriceForSentArray.remove(at: index)
                    productQuantityForSentArray.remove(at: index)
                }
            }
        }
        
        totalProducts = Double(productPriceForSentArray.count)
        if totalProducts == 0 {
            self.countNumber = 0
        }
        tableView.reloadData()
    }
    
    @objc fileprivate func count(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let priceTemp: Double = dict["prizePerProduct"] as! Double
        let tempTotalProducts: Double = dict["totalProducts"] as! Double
        let tempProductTitle: String = dict["title"] as! String
        let tempSelected: Bool = dict["selected"] as! Bool
        let tempQuantity: Int = dict["quantity"] as! Int
        
        if tempSelected {
            productTitleForSentArray.append(tempProductTitle)
            productPriceForSentArray.append(priceTemp)
            productQuantityForSentArray.append(tempQuantity)
        } else {
            if let index = productTitleForSentArray.index(of: tempProductTitle) {
                productTitleForSentArray.remove(at: index)
                productPriceForSentArray.remove(at: index)
                productQuantityForSentArray.remove(at: index)
            }
        }
        let oldValue = countNumber
        self.countNumber = priceTemp + oldValue
        totalProducts = totalProducts + tempTotalProducts
        
        if totalProducts == 0 {
            self.countNumber = 0
        }
        tableView.reloadData()
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.count), name: NSNotification.Name(rawValue: "countFinalPrice"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.countMultiplier), name: NSNotification.Name(rawValue: "countFinalPriceMultiplier"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "countFinalPrice"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "countFinalPriceMultiplier"), object: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleOfScreen: UILabel!
    @IBOutlet weak var orderBtn: UIButton!
}

extension ServiceBigSelectionTableVC {
    
    @IBAction func orderBtn(_ sender: Any) {
        if countNumber > 0 {
            self.dismiss(animated: false, completion: nil)
            DelayFunc.sharedInstance.delay(0.5, closure: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "executePushOrderToFinish"), object: ["simpleView": true, "numberOfItems" : Int(self.totalProducts).description, "totalPriceOfProduct":self.totalPrice, "productTitleForSentArray":self.productTitleForSentArray, "productPriceForSentArray":self.productPriceForSentArray, "productQuantityForSentArray":self.productQuantityForSentArray])
            })
        } else {
            showSimpleAlert(title: "Error".localized(), message: "Select atleast 1 item.".localized())
        }
    }
    
    @IBAction func dismissViewBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension ServiceBigSelectionTableVC: UITableViewDataSource, UITableViewDelegate {
    
    fileprivate func customCellRegister(){
        tableView.register(UINib(nibName: "ServiceBigSelectionCellView", bundle: nil), forCellReuseIdentifier: "countcell")
        tableView.register(UINib(nibName: "ServiceBigCheckBoxCellView", bundle: nil), forCellReuseIdentifier: "checkboxcell")
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addOnTitle.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        if showCheckBoxView {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "checkboxcell") as! ServiceBigCheckBoxCellVC
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if addOnTitle.count == indexPath.row {
                cell.titleLabel.text = "Total price".localized()
                if countNumber.truncatingRemainder(dividingBy: 1) == 0 {
                    //it's an integer
                    cell.finalPriceCountLabel.text = Int(countNumber).description
                } else {
                    cell.finalPriceCountLabel.text = countNumber.description
                }
                cell.container.isHidden = true
            } else {
                cell.titleLabel.text = "\(addOnTitle[indexPath.row]) (\(addonPrice[indexPath.row]))"
                cell.prizePerProduct = addonPrice[indexPath.row]
                cell.finalPriceCountLabel.text = ""
                cell.container.isHidden = false
                
                cell.productTitle = addOnTitle[indexPath.row]
                cell.productPricePerUnit = addonPrice[indexPath.row]
            }
            totalPrice = cell.finalPriceCountLabel.text!
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "countcell") as! ServiceBigSelectionCellVC
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if addOnTitle.count == indexPath.row {
                cell.titleLabel.text = "Total price".localized()
                if countNumber.truncatingRemainder(dividingBy: 1) == 0 {
                    //it's an integer
                    cell.finalPriceCountLabel.text = Int(countNumber).description
                } else {
                    cell.finalPriceCountLabel.text = countNumber.description
                }
                cell.countContainer.isHidden = true
            } else {
                cell.titleLabel.text = "\(addOnTitle[indexPath.row]) (\(addonPrice[indexPath.row]))"
                cell.prizePerProduct = addonPrice[indexPath.row]
                cell.finalPriceCountLabel.text = ""
                cell.countContainer.isHidden = false
                
                cell.productTitle = addOnTitle[indexPath.row]
                cell.productPricePerUnit = addonPrice[indexPath.row]
            }
            totalPrice = cell.finalPriceCountLabel.text!
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}

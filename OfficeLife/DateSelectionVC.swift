//
//  DateSelectionVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 04.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import JTAppleCalendar

class DateSelectionVC: UIViewController {

    @IBOutlet weak var calendarView: JTAppleCalendarView!

    let formatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        formatter.timeZone = TimeZone.current

        calendarView.delegate = self
        calendarView.dataSource = self
        
        calendarView.registerCellViewXib(file: "CalendarCellView")
        calendarView.cellInset = CGPoint(x: 0, y: 0)
        
        calendarView.visibleDates { (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
        }
        separatorView.backgroundColor = UIColor.cellBg
        self.darkBg.alpha = 0
        self.calendarContainer.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.1, animations: {
            self.calendarHeightConstant.constant = self.calendarContainer.frame.size.width
            self.view.layoutIfNeeded()
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.darkBg.alpha = 1
            self.calendarContainer.alpha = 1
        }
    }
    
    func dismissView(){
        UIView.animate(withDuration: 0.5) {
            self.darkBg.alpha = 0
            self.calendarContainer.alpha = 0
        }
        DelayFunc.sharedInstance.delay(0.5) {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func handleCellSelection(_ view: JTAppleDayCellView?, cellState: CellState) {
        guard let myCustomCell = view as? CalendarCellVC  else {
            return
        }
        if cellState.isSelected {
            myCustomCell.selectedView.isHidden = false
        } else {
            myCustomCell.selectedView.isHidden = true
        }
    }
    
    func handleCellTextColor(_ view: JTAppleDayCellView?, cellState: CellState) {
        
        guard let myCustomCell = view as? CalendarCellVC  else {
            return
        }
        
        if cellState.isSelected {
            myCustomCell.dayLabel.textColor = UIColor.white
        } else {
            if cellState.dateBelongsTo == .thisMonth {
                if cellState.date == Date() {
                    myCustomCell.dayLabel.textColor = UIColor.white
                } else {
                    myCustomCell.dayLabel.textColor = UIColor.black
                }
            } else {
                myCustomCell.dayLabel.textColor = UIColor.gray
            }
        }
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first else {
            return
        }
        let month = Calendar.current.dateComponents([.month], from: startDate).month!
        var monthTemp:String?
        switch month {
        case 1:
            monthTemp = "January".localized()
        case 2:
            monthTemp = "February".localized()
        case 3:
            monthTemp = "March".localized()
        case 4:
            monthTemp = "April".localized()
        case 5:
            monthTemp = "May".localized()
        case 6:
            monthTemp = "June".localized()
        case 7:
            monthTemp = "July".localized()
        case 8:
            monthTemp = "August".localized()
        case 9:
            monthTemp = "September".localized()
        case 10:
            monthTemp = "October".localized()
        case 11:
            monthTemp = "November".localized()
        case 12:
            monthTemp = "December".localized()
        default:
            break
        }
        monthLabel.text = monthTemp
        let year = Calendar.current.component(.year, from: startDate)
        yearLabel.text = year.description
    }

    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var calendarHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var calendarContainer: UIView!
    @IBOutlet weak var darkBg: UIView!
    
}

extension DateSelectionVC {

    @IBAction func next(_ sender: UIButton) {
        self.calendarView.scrollToSegment(.next) {
            self.calendarView.visibleDates({ (visibleDates: DateSegmentInfo) in
                self.setupViewsOfCalendar(from: visibleDates)
            })
        }
    }
    
    @IBAction func previous(_ sender: UIButton) {
        self.calendarView.scrollToSegment(.previous) {
            self.calendarView.visibleDates({ (visibleDates: DateSegmentInfo) in
                self.setupViewsOfCalendar(from: visibleDates)
            })
        }
    }
    
    @IBAction func outsideContainerBtn(_ sender: Any) {
        dismissView()
    }
}

extension DateSelectionVC: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        let startDate = Date()
        let endDate = formatter.date(from: "01 01 2026")!
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: 5,
                                                 calendar: Calendar.current,
                                                 generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfGrid,
                                                 firstDayOfWeek: .monday)
        
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplayCell cell: JTAppleDayCellView, date: Date, cellState: CellState) {
        
        let myCustomCell = cell as! CalendarCellVC
        myCustomCell.dayLabel.text = cellState.text
        
        if Calendar.current.isDateInToday(date) {
            myCustomCell.backgroundColor = UIColor.navBarColor
        } else {
            myCustomCell.backgroundColor = UIColor.white
        }
        
        handleCellSelection(cell, cellState: cellState)
        handleCellTextColor(cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        handleCellSelection(cell, cellState: cellState)
        handleCellTextColor(cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        handleCellSelection(cell, cellState: cellState)
        handleCellTextColor(cell, cellState: cellState)
        let calendar = Calendar.current
        
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let temp = "\(day).\(month).\(year)"
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateDay"), object: ["dayStr" : String(describing: temp)])
        dismissView()
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates)
    }
    
    func scrollDidEndDecelerating(for calendar: JTAppleCalendarView) {
        self.setupViewsOfCalendar(from: calendarView.visibleDates())
    }
    
}

//
//  MyOrdersTableVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 27.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader

fileprivate struct MyOrdersData {
    
    var idArr, orderNameArr, timeArr, statusArr, priceArr: [String]
    
    init(idArr: [String], orderNameArr: [String], timeArr: [String], statusArr: [String], priceArr: [String]) {
        self.idArr = idArr
        self.orderNameArr = orderNameArr
        self.timeArr = timeArr
        self.statusArr = statusArr
        self.priceArr = priceArr
    }
}

class MyOrdersTableVC: UIViewController {
    
    fileprivate var myOrdersData = MyOrdersData(idArr: [], orderNameArr: [], timeArr: [], statusArr: [], priceArr: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegister()
        setupLabels()
        getData()
    }
    
    fileprivate func getData(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            OrdersReq().getMyOrders { (done, idArr, orderNameArr, timeArr, statusArr, priceArr) in
                SwiftLoader.hide()
                if done {
                    self.myOrdersData = MyOrdersData(idArr: idArr, orderNameArr: orderNameArr, timeArr: timeArr, statusArr: statusArr, priceArr: priceArr)
                    self.tableView.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "MyOrdersCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MyOrdersCellVC")
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
    }
    
    fileprivate func setupLabels(){
        title = "My profile".localized()
        titleLabel.text = "My orders".localized()
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
}

extension MyOrdersTableVC {
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

extension MyOrdersTableVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myOrdersData.idArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MyOrdersCellVC") as! MyOrdersCellVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.numberLabel.text = "#" + myOrdersData.orderNameArr[indexPath.row]
        cell.infoLabel.text = myOrdersData.statusArr[indexPath.row].localized()
        cell.feeLabel.text = myOrdersData.priceArr[indexPath.row]
        cell.monthLabel.text = myOrdersData.timeArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderDetailVC") as! MyOrderDetailVC
        openNewVC.ticketNumber = myOrdersData.idArr[indexPath.row]
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}

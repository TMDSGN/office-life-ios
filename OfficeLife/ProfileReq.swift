//
//  ProfileReq.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ProfileReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getMyProfileInfo( desiredProfileId:String, completionHandler: @escaping (Bool, String, String, String, String, String, Bool, String, String, [String], String) -> ()) -> (){
        
        var url = urlDomain + "/users/v2/profile"
        
        if !desiredProfileId.isEmpty {
            url = url + "/\(desiredProfileId)"
        }
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                let name = json["name"].stringValue
                let surname = json["surname"].stringValue
                let email = json["email"].stringValue
                let phone = json["phone"].stringValue//timestamp
                let company = json["company"].stringValue
                let feedFromMe = json["feed_from_me"].boolValue
                let photoStr = json["company_photo"].stringValue
                let photo = json["photo"].stringValue
                let points = json["points"].stringValue

                Session.sharedInstance.profileWriteHomeWallPermission = json["write_homewall_permission"].boolValue
                
                var interestsArr = [String]()
                for i in 0..<json["interests"].count {
                    if i == 0 {
                        interestsArr.append(json["interests"][i].stringValue)
                    } else {
                        interestsArr.append(", " + json["interests"][i].stringValue)
                    }
                }
                
                completionHandler(true, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr, photoStr)
            } else {
                completionHandler(false, String(), String(), String(), String(), String(), Bool(), String(), String(), [String](), String())
            }
        }
    }
    
    func updateMyProfileInfo(name:String, surname:String, email:String, phone:String, company:String, feedFromMe:Bool, interestsArr:[String], completionHandler: @escaping (Bool, String, String, String, String, String, Bool, String, String, [String]) -> ()) -> (){
        
        let url = urlDomain + "/users/v2/profile"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        var para = [String : Any]()
        
        if !email.isEmpty {
            para["email"] = email
        }
        if !name.isEmpty {
            para["name"] = name
        }
        if !surname.isEmpty {
            para["surname"] = surname
        }
        if !phone.isEmpty {
            para["phone"] = phone
        }
        if !company.isEmpty {
            para["company"] = Int(company)!
        }
        if interestsArr != [String](){
            para["interests"] = interestsArr
        }
        para["feed_from_me"] = feedFromMe
        Alamofire.request(url, method: .put, parameters:para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                let name = json["name"].stringValue
                let surname = json["surname"].stringValue
                let email = json["email"].stringValue
                let phone = json["phone"].stringValue//timestamp
                let company = json["company"].stringValue
                let feedFromMe = json["feed_from_me"].boolValue
                let photo = json["photo"].stringValue
                let points = json["points"].stringValue
                
                var interestsArr = [String]()
                for i in 0..<json["interests"].count {
                    if i == 0 {
                        interestsArr.append(json["interests"][i].stringValue)
                    } else {
                        interestsArr.append(", " + json["interests"][i].stringValue)
                    }
                }
                
                completionHandler(true, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr)
            } else {
                completionHandler(false, String(), String(), String(), String(), String(), Bool(), String(), String(), [String]())
            }
        }
    }
    
    func putPhoto( photoStr:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/users/v2/profile/photo"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para = [
            "photo": photoStr
            ] as [String : Any]
        
        Alamofire.request(url, method: .put, parameters:para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                let photo = json["photo"].stringValue
                Session.sharedInstance.profilePhone = photo
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func getCreditState(completionHandler: @escaping (Bool, String) -> ()) -> (){
        
        let url = urlDomain + "/credit/v1/user"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                let credit = json["credit"].stringValue
                
                completionHandler(true, credit)
            } else {
                completionHandler(false, String())
            }
        }
    }
}

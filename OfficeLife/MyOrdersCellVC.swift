//
//  MyOrdersCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 27.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit

class MyOrdersCellVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.containerView.layer.borderWidth = 2
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
}

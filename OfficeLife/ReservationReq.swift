//
//  ReservationReq.swift
//  OfficeLife
//
//  Created by Josef Antoni on 13.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Localize_Swift

class ReservationReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getReservationTypeList( type:String, completionHandler: @escaping (Bool, [String], [String], [String], [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/reservations/v1/list/\(type)"
        let lang = Localize.currentLanguage()
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "Language" : lang
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)

                var idArr = [String]()
                var nameArr = [String]()
                var photoArr = [String]()
                var podlaziArr = [String]()
                var fromDateArr = [String]()
                var fromTimeArr = [String]()
                var fromTimestampForDeleteArr = [String]()
                
                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let name = json[i]["name"].stringValue
                    let photo = json[i]["photo"].stringValue//timestamp
                    let podlazi = json[i]["podlazi"].stringValue
                    let from = json[i]["from"].doubleValue
                    let fromStr = json[i]["created"].stringValue

                    idArr.append(id)
                    nameArr.append(name)
                    photoArr.append(photo)
                    podlaziArr.append(podlazi)
                    fromDateArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: from, format: "dd.MM.yyyy"))
                    fromTimeArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: from, format: "HH:mm"))
                    fromTimestampForDeleteArr.append(fromStr)
                }
                
                completionHandler(true, idArr, nameArr, photoArr, podlaziArr, fromDateArr, fromTimeArr, fromTimestampForDeleteArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String](), [String](), [String]())
            }
        }
    }
    
    func getReservationDetail( type:String, completionHandler: @escaping (Bool, String, String, String) -> ()) -> (){
        
        let url = urlDomain + "/reservations/v1/\(type)"
        let lang = Localize.currentLanguage()
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)",
            "Language" : lang
        ]

        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                let description = json["description"].stringValue
                let name = json["name"].stringValue
                let photo = json["photo"].stringValue//timestamp
                
                completionHandler(true, name, description, photo)
            } else {
                completionHandler(false, String(), String(), String())
            }
        }
    }
    
    func getReservationAvailibility( type:String, daytimestamp:String, completionHandler: @escaping (Bool, [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/reservations/v1/\(type)/availability"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        let para  = [
            "day" : daytimestamp
        ]
        
        Alamofire.request(url, method: .get, parameters:para, headers: header).responseJSON { response in
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                var hourFromArr = [String]()
                var minuteFromArr = [String]()
                var hourToArr = [String]()
                var minuteToArr = [String]()

                for i in 0..<json.count{
                    let from = json[i]["from"].doubleValue
                    let to = json[i]["to"].doubleValue
                    hourFromArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: from, format: "HH"))
                    minuteFromArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: from, format: "mm"))
                    
                    hourToArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: to, format: "HH"))
                    minuteToArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: to, format: "mm"))
                }
                completionHandler(true, hourFromArr, minuteFromArr, hourToArr, minuteToArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String]())
            }
        }
    }
    
    func createReservation( reservationId:String, from:String, to:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/reservations/v1/\(reservationId)"
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "from": from,
            "to" : to
        ]
        
        Alamofire.request(url, method: .post, parameters:para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func deleteReservation( reservationId:String, created:String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/reservations/v1/\(reservationId)"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "created": created
        ]
        Alamofire.request(url, method: .delete, parameters:para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.result.isSuccess {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
}

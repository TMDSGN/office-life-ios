//
//  DateConvertor.swift
//  OfficeLife
//
//  Created by Josef Antoni on 12.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation

class DateConvertor {

    static let sharedInstance = DateConvertor()
    fileprivate init() {}
    
    func getDateStr(timeStamp:Double, format:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(identifier: "UTC")
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateString = formatter.string(from: date as Date)
        return dateString
    }
    
    func getDate(timeStamp:Double, format:String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(identifier: "UTC")
        let date = Date(timeIntervalSince1970: timeStamp)
        return date
    }
    
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let now = NSDate()
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now as Date, options: [])
        
        if (components.year! >= 2) {
            return "\(components.year!)" + " years ago".localized()
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago".localized()
            } else {
                return "Last year".localized()
            }
        } else if (components.month! >= 2) {
            return "\(components.month!)" + " months ago".localized()
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago".localized()
            } else {
                return "Last month".localized()
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)" + " weeks ago".localized()
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago".localized()
            } else {
                return "Last week".localized()
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)" + " days ago".localized()
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago".localized()
            } else {
                return "Yesterday".localized()
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)" + " hours ago".localized()
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago".localized()
            } else {
                return "An hour ago".localized()
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!)" + " minutes ago".localized()
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago".localized()
            } else {
                return "A minute ago".localized()
            }
        } else if (components.second! >= 3) {
            return "\(components.second!)" + " seconds ago".localized()
        } else {
            return "Just now".localized()
        }
    }
    
    func getDayOfWeek(date: Date) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM"
        formatter.timeZone = TimeZone.current
        
        let month = formatter.string(from: date as Date)
        let cal = Calendar(identifier: .gregorian)
        let dayName = cal.component(.weekday, from: date)
        let dayOfWeek = cal.component(.day, from: date)
        
        let result = "\(dayOfWeek).\(month)"
        var tempNameOfDay: String = ""
        switch dayName {
        case 1:
            tempNameOfDay = "Sunday".localized()
        case 2:
            tempNameOfDay = "Monday".localized()
        case 3:
            tempNameOfDay = "Tuesday".localized()
        case 4:
            tempNameOfDay = "Wednesday".localized()
        case 5:
            tempNameOfDay = "Thursday".localized()
        case 6:
            tempNameOfDay = "Friday".localized()
        case 7:
            tempNameOfDay = "Saturday".localized()
        default:
            return nil
        }
        
        return "\(tempNameOfDay) " + result
    }
    
}

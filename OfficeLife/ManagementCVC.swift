//
//  ManagementVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 28.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader

fileprivate struct ManagementData {
    
    var idArr, firstnameArr, surnameArr, emailArr, phoneArr, positionArr, photoArr: [String]
    
    init(idArr: [String], firstnameArr: [String], surnameArr: [String], emailArr: [String], phoneArr: [String], positionArr: [String], photoArr: [String]) {
        self.idArr = idArr
        self.firstnameArr = firstnameArr
        self.surnameArr = surnameArr
        self.emailArr = emailArr
        self.phoneArr = phoneArr
        self.positionArr = positionArr
        self.photoArr = photoArr
    }
}

class ManagementCVC: HamburgerContainerCVC, AlertMessage {
    
    fileprivate var managementData = ManagementData(idArr: [], firstnameArr: [], surnameArr: [], emailArr: [], phoneArr: [], positionArr: [], photoArr: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObserver()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIcon")
        setupCollectionLayout()
        title = "Meet the Team".localized()
    }
    
    @objc fileprivate func contactUser(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let userId = dict["userId"] as! String
        if userId != Session.sharedInstance.userToken {
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
            openNewVC.conversationReceiverId = userId
            openNewVC.messagesWall = true
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
        }
    }
    
    fileprivate func getData(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            PeopleReq().getPeopleStaff { (done, idArr, firstnameArr, surnameArr, emailArr, phoneArr, positionArr, photoArr)in
                SwiftLoader.hide()
                if done {
                    self.managementData = ManagementData(idArr: idArr, firstnameArr: firstnameArr, surnameArr: surnameArr, emailArr: emailArr, phoneArr: phoneArr, positionArr: positionArr, photoArr: photoArr)
                    self.collectionView?.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func setupCollectionLayout(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15)
        layout.itemSize = CGSize(width: width / 2 - 23, height: 250)
        layout.minimumInteritemSpacing = 15
        layout.minimumLineSpacing = 15
        collectionView!.collectionViewLayout = layout
    }
    
    fileprivate func registerObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(ManagementCVC.contactUser), name: NSNotification.Name(rawValue: "contactUser"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "contactUser"), object: nil)
    }
}

extension ManagementCVC {
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ManagementCell", for: indexPath) as! ManagementCellVC
        if !managementData.photoArr[indexPath.row].isEmpty {
            let tempurl = URL(string: managementData.photoArr[indexPath.row])
            cell.imgIcon.hnk_setImage(from: tempurl, placeholder: UIImage(named:"placeholder_photo"), success: { (image) in
                cell.imgIcon.image = image
                cell.imgIcon.layer.cornerRadius = 40
                cell.imgIcon.clipsToBounds = true
            }, failure: { (err) in
                cell.imgIcon.backgroundColor = UIColor.backgroundPhotoColor
                cell.imgIcon.image = UIImage(named: "placeholder_photo")
                cell.imgIcon.layer.cornerRadius = 0
                cell.imgIcon.clipsToBounds = true
            })
        } else {
            cell.imgIcon.backgroundColor = UIColor.backgroundPhotoColor
            cell.imgIcon.image = UIImage(named: "placeholder_photo")
            cell.imgIcon.layer.cornerRadius = 0
            cell.imgIcon.clipsToBounds = true
        }
        
        cell.nameLabel.text = managementData.firstnameArr[indexPath.row] + " " + managementData.surnameArr[indexPath.row]
        cell.serviceLabel.text = managementData.positionArr[indexPath.row]
        // cell.emailBtn.setTitle(emailArr[indexPath.row], for: .normal)
        cell.phoneBtn.text = managementData.phoneArr[indexPath.row]
        cell.userId = managementData.idArr[indexPath.row]
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.managementData.idArr.count
    }
}

//
//  HomeVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 12.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader
import iOS_Slide_Menu
import Firebase

fileprivate struct ServicesData {
    
    var servicesIdArr, servicesNameArr, servicesPriceArr, servicesBackgroundArr, servicesTypeArr: [String]
    
    init(servicesIdArr: [String], servicesNameArr: [String], servicesPriceArr: [String], servicesBackgroundArr: [String], servicesTypeArr: [String]) {
        self.servicesIdArr = servicesIdArr
        self.servicesNameArr = servicesNameArr
        self.servicesPriceArr = servicesPriceArr
        self.servicesBackgroundArr = servicesBackgroundArr
        self.servicesTypeArr = servicesTypeArr
    }
}

class HomeVC: HamburgerContainerVC {
    
    var storedOffsets = [Int: CGFloat]()
    
    fileprivate var activitiesIdArr: [String] = []
    fileprivate var activitiesNameArr: [String] = []
    fileprivate var activitiesDateArr: [String] = []
    fileprivate var activitiesBackgroundArr: [String] = []
    fileprivate var activitiesTypeArr: [String] = []
    
    fileprivate var myActivitiesIdArr: [String] = []
    fileprivate var myActivitiesNameArr: [String] = []
    fileprivate var myActivitiesDateArr: [String] = []
    fileprivate var myActivitiesBackgroundArr: [String] = []
    fileprivate var myActivitiesTypeArr: [String] = []
    
    fileprivate var servicesData = ServicesData(servicesIdArr: [], servicesNameArr: [], servicesPriceArr: [], servicesBackgroundArr: [], servicesTypeArr: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityTable.separatorStyle = .none
        self.servicesTable.separatorStyle = .none
        self.myActivityTable.separatorStyle = .none
        self.activityTitleLabel.text = "Activities".localized()
        self.servicesTitleLabel.text = "Services".localized()
        self.myActivityTitleLabel.text = "My Activities".localized()
        
        self.activityAllLabel.text = "all".localized()
        self.serviciesAllLabel.text = "all".localized()
        self.myActivityAllLabel.text = "all".localized()
        self.view.layoutIfNeeded()
        
        DelayFunc.sharedInstance.delay(0.3) {
            self.getActivitiesData(type: "0")
            self.getActivitiesData(type: "1")
            self.getServices()
            if Session.sharedInstance.profileEmail.isEmpty {
                self.getData()
            }
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        Session.sharedInstance.isAfterRegistrationSelectedCompany = true
        Session.sharedInstance.isAfterRegistration = true
        Session.sharedInstance.saveCurrentSession()
        registerToGcm()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIcon")
    }
    
    fileprivate func registerToGcm(){
        if let temp = FIRInstanceID.instanceID().token() {
            if !temp.description.isEmpty && !Session.sharedInstance.userToken.isEmpty && !Session.sharedInstance.securityToken.isEmpty {
                Authentization().registerGcmToken(token: temp, completionHandler: { (done) in
                })
            }
        }
    }
    
    @objc fileprivate func getData(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileReq().getMyProfileInfo(desiredProfileId: String(), completionHandler: { (done, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr, photoStr) in
                SwiftLoader.hide()
                if done {
                    let s = Session.sharedInstance
                    s.profileFirstName = name
                    s.profileSurName = surname
                    s.profileEmail = email
                    s.profilePhone = phone
                    s.profileCompany = company
                    s.profileFeedFromMe = feedFromMe
                    s.profilePhoto = photo
                    s.profileInterest = interestsArr.joined(separator: "")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshHamburgerMenu"), object: nil)

                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func prepareToPopToNewContainer(_ nameOfVc:String){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: nameOfVc)
        DispatchQueue.main.async(execute: { () -> Void in
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        });
    }
    
    fileprivate func getActivitiesData(type:String){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ActivitiesReq().getActivitiesPreview( type: type,completionHandler: { (done, idArr, nameArr, dateArr, backgroundArr, typeArr) in
                SwiftLoader.hide()
                if done {
                    if type == "0"{
                        self.activitiesIdArr = idArr
                        self.activitiesNameArr = nameArr
                        self.activitiesDateArr = dateArr
                        self.activitiesBackgroundArr = backgroundArr
                        self.activitiesTypeArr = typeArr
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadActivityCollectionView"), object: nil)
                    } else {
                        var tempID = idArr
                        tempID.append("0")
                        self.myActivitiesIdArr = tempID
                        self.myActivitiesNameArr = nameArr
                        self.myActivitiesDateArr = dateArr
                        self.myActivitiesBackgroundArr = backgroundArr
                        self.myActivitiesTypeArr = typeArr
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMyActivityCollectionView"), object: nil)
                    }
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func getServices(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ServicesReq().getServicesPreview(completionHandler: { (done, idArr, nameArr, priceArr, backgroundArr, typeArr) in
                SwiftLoader.hide()
                if done {
                    self.servicesData = ServicesData(servicesIdArr: idArr, servicesNameArr: nameArr, servicesPriceArr: priceArr, servicesBackgroundArr: backgroundArr, servicesTypeArr: typeArr)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadServicesCollectionView"), object: nil)
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @IBOutlet weak var activityTable: UITableView!
    @IBOutlet weak var servicesTable: UITableView!
    @IBOutlet weak var myActivityTable: UITableView!
    @IBOutlet weak var activityTitleLabel: UILabel!
    @IBOutlet weak var servicesTitleLabel: UILabel!
    @IBOutlet weak var myActivityTitleLabel: UILabel!
    @IBOutlet weak var myActivityContainer: UIView!
    @IBOutlet weak var activityAllLabel: UILabel!
    @IBOutlet weak var serviciesAllLabel: UILabel!
    @IBOutlet weak var myActivityAllLabel: UILabel!
    @IBOutlet weak var activityContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var activityContainer: UIView!
}

extension HomeVC {

    @IBAction func activitiesBtn(_ sender: Any) {
        prepareToPopToNewContainer("ActivityTableVC")
    }
    
    @IBAction func servicesBtn(_ sender: Any) {
        prepareToPopToNewContainer("ServiceTypeVC")
    }
    
    @IBAction func myActivitiesBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileActivityTableVC") as! MyProfileActivityTableVC
        openNewVC.rezervationBool = false
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
}

extension HomeVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case activityTable:
            return activityTable.frame.size.height
        case servicesTable:
            return servicesTable.frame.size.height
        case myActivityTable:
            return myActivityTable.frame.size.height
        default:
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        switch tableView {
        case activityTable:
            let cell = self.activityTable.dequeueReusableCell(withIdentifier: "activityTableCell") as! HomeActivityTableCellVC
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        case servicesTable:
            let cell = self.servicesTable.dequeueReusableCell(withIdentifier: "servicesTableCell")
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            return cell!
        case myActivityTable:
            let cell = self.myActivityTable.dequeueReusableCell(withIdentifier: "myActivityTableCell")
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            return cell!
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch tableView {
        case activityTable:
            guard let tableViewCell = cell as? HomeActivityTableCellVC else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        case servicesTable:
            guard let tableViewCell = cell as? HomeServicesTableCellVC else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        case myActivityTable:
            guard let tableViewCell = cell as? HomeMyActivitiesTableCellVC else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.activityTable.deselectRow(at: indexPath, animated: false)
        self.servicesTable.deselectRow(at: indexPath, animated: false)
        self.myActivityTable.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch tableView {
        case activityTable:
            guard let tableViewCell = cell as? HomeActivityTableCellVC else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        case servicesTable:
            guard let tableViewCell = cell as? HomeActivityTableCellVC else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        case myActivityTable:
            guard let tableViewCell = cell as? HomeMyActivitiesTableCellVC else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        default:
            break
        }
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = CGSize(width:((self.view.frame.width/2)-18), height: activityTable.frame.size.height)
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.invalidateLayout()
        }
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = CGSize(width:((self.view.frame.width/2)-18), height: servicesTable.frame.size.height)
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.invalidateLayout()
        }
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = CGSize(width:((self.view.frame.width/2)-18), height: myActivityTable.frame.size.height)
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.invalidateLayout()
        }
        if collectionView.tag == 1 {
            return activitiesIdArr.count
        } else if collectionView.tag == 3 {
            return myActivitiesIdArr.count
        } else {
            return servicesData.servicesIdArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeActivityCollectionCell", for: indexPath) as! HomeActivityCollectionCellVC
            if !activitiesBackgroundArr[indexPath.row].isEmpty {
                let tempurl = URL(string: activitiesBackgroundArr[indexPath.row])
                cell.imgIcon.hnk_setImage(from: tempurl)
            } else {
                cell.imgIcon.backgroundColor = UIColor.backgroundPhotoColor
                cell.imgIcon.image = UIImage(named: "placeholder")
            }
            cell.titleLabel.text = activitiesNameArr[indexPath.row]
            cell.subtitleLabel.text = activitiesDateArr[indexPath.row]
            return cell
        } else if collectionView.tag == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeServicesCollectionCell", for: indexPath) as! HomeServicesCollectionCellVC
            if !servicesData.servicesBackgroundArr[indexPath.row].isEmpty {
                let tempurl = URL(string: servicesData.servicesBackgroundArr[indexPath.row])
                cell.imgIcon.hnk_setImage(from: tempurl)
            } else {
                cell.imgIcon.backgroundColor = UIColor.backgroundPhotoColor
                cell.imgIcon.image = UIImage(named: "placeholder")
            }
            cell.titleLabel.text = servicesData.servicesNameArr[indexPath.row]
            cell.subtitleLabel.text = servicesData.servicesPriceArr[indexPath.row]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeMyActivityCollectionCell", for: indexPath) as! HomeMyActivitiesCollectionCellVC
            if myActivitiesIdArr.count == indexPath.row + 1 {
                cell.noActivityContainer.isHidden = false
            } else {
                cell.noActivityContainer.isHidden = true
                if !myActivitiesBackgroundArr[indexPath.row].isEmpty {
                    let tempurl = URL(string: myActivitiesBackgroundArr[indexPath.row])
                    cell.imgIcon.hnk_setImage(from: tempurl)
                } else {
                    cell.imgIcon.backgroundColor = UIColor.backgroundPhotoColor
                    cell.imgIcon.image = UIImage(named: "placeholder")
                }
                cell.titleLabel.text = myActivitiesNameArr[indexPath.row]
                cell.subtitleLabel.text = myActivitiesDateArr[indexPath.row]
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ActivityDetailVC") as! ActivityDetailVC
            openNewVC.activityId = activitiesIdArr[indexPath.row]
            openNewVC.titleCategory = "Activities".localized()
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
        } else if collectionView.tag == 2 {
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ServiceDetailVC") as! ServiceDetailVC
            openNewVC.serviceId = servicesData.servicesIdArr[indexPath.row]
            openNewVC.titleCategory = "Services".localized()
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
        } else {
            if myActivitiesIdArr.count == indexPath.row + 1 {
                prepareToPopToNewContainer("ActivityTableVC")
            } else {
                collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ActivityDetailVC") as! ActivityDetailVC
                openNewVC.activityId = myActivitiesIdArr[indexPath.row]
                openNewVC.titleCategory = "My Activities".localized()
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        }
    }
}


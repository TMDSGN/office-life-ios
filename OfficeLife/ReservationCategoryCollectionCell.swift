//
//  ReservationCategoryCollectionCell.swift
//  OfficeLife
//
//  Created by Josef Antoni on 10.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

class ReservationCategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var underLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        underLineView.backgroundColor = UIColor.mainBlue
    }
}

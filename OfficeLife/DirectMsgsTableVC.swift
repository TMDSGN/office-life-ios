//
//  DirectMsgsTableVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 30.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader

fileprivate struct ReceiverData {
    
    var receiverIdArr, receiverNameArr, receiverPhotoArr: [String]
    
    init(receiverIdArr: [String], receiverNameArr: [String], receiverPhotoArr: [String]) {
        self.receiverIdArr = receiverIdArr
        self.receiverNameArr = receiverNameArr
        self.receiverPhotoArr = receiverPhotoArr
    }
}

fileprivate struct ConversationData {
    
    var conversationReadArr: [Bool]
    var conversationPreviewTextArr, conversationReceiverIdArr, conversationReceiverNameArr, conversationReceiverPhotoArr: [String]
    
    init(conversationReadArr: [Bool], conversationPreviewTextArr: [String], conversationReceiverIdArr: [String], conversationReceiverNameArr: [String], conversationReceiverPhotoArr: [String]) {
        self.conversationReadArr = conversationReadArr
        self.conversationPreviewTextArr = conversationPreviewTextArr
        self.conversationReceiverIdArr = conversationReceiverIdArr
        self.conversationReceiverNameArr = conversationReceiverNameArr
        self.conversationReceiverPhotoArr = conversationReceiverPhotoArr
    }
}

class DirectMsgsTableVC: HamburgerContainerVC {
    
    fileprivate var receiverData = ReceiverData(receiverIdArr: [], receiverNameArr: [], receiverPhotoArr: [])
    fileprivate var conversationData = ConversationData(conversationReadArr: [], conversationPreviewTextArr: [], conversationReceiverIdArr: [], conversationReceiverNameArr: [], conversationReceiverPhotoArr: [])

    fileprivate var filterdReceiverNameArr: [String] = []
    fileprivate var selectedNewUser: String = ""
    fileprivate var loaderActive = true
    fileprivate var refreshControl: UIRefreshControl!
    //notification info
    var notificationConversationReceiverId: String = ""
    var conversationReceiverId: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegister()
        self.title = "Messages".localized()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        addBtn.backgroundColor = UIColor.mainBlue
        addBtn.tintColor = UIColor.white
        autocompleteTextField.placeholder = "Tap to pick user".localized()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        getReceivers()
        autocompleteTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationForCurrentThreadCheck), name: NSNotification.Name(rawValue: "notificationForCurrentThreadCheck"), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        autocompleteTableViewHeightConstant.constant = 0
        getConversations()
        setupDefaultMenuButton()
        //Zkontrolovat zda v současném aktivní messageWALL okně je konverzace na kterou přišla notifikace
        if notificationConversationReceiverId == conversationReceiverId && !conversationReceiverId.isEmpty {
            Session.sharedInstance.newListNotification = false
        }
        setupNavBar("chatIcon", "notificationIcon")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    /*
     *  Zkontrolovat zda v současném aktivní messageWALL okně je konverzace na kterou přišla notifikace
     */
    @objc fileprivate func notificationForCurrentThreadCheck(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        if let receiverId = dict["receiverId"] as? String {
            notificationConversationReceiverId = receiverId
        }
    }
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "DirectMsgsNView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "DirectMsgsNVC")
        let nib2 = UINib(nibName: "UserDropDownCell", bundle: nil)
        tableView.register(nib2, forCellReuseIdentifier: "UserDropDownCellVC")
        tableView.estimatedRowHeight = 200
        tableView.separatorStyle = .none
        autocompleteTable.separatorStyle = .none
    }
    
    fileprivate func getConversations() {
        if Reachability.isConnectedToNetwork() {
            if loaderActive {
                SwiftLoader.show(animated: true)
            }
            MsgsReq().getConversations { (done, readArr, previewTextArr, receiverIdArr, receiverNameArr, receiverPhotoArr) in
                self.loaderActive = true
                self.refreshControl!.endRefreshing()
                self.conversationData = ConversationData(conversationReadArr: readArr, conversationPreviewTextArr: previewTextArr, conversationReceiverIdArr: receiverIdArr, conversationReceiverNameArr: receiverNameArr, conversationReceiverPhotoArr: receiverPhotoArr)
                self.tableView.reloadData()
                SwiftLoader.hide()
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func getReceivers(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            MsgsReq().getReceivers { (done, idArr, nameArr, photoArr) in
                self.receiverData = ReceiverData(receiverIdArr: idArr, receiverNameArr: nameArr, receiverPhotoArr: photoArr)
                self.autocompleteTable.reloadData()
                SwiftLoader.hide()
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @objc fileprivate func refresh(sender:AnyObject) {
        self.loaderActive = false
        self.getConversations()
    }
    
    @IBAction func addNewConversationBtn(_ sender: Any) {
        if !self.autocompleteTextField.text!.isEmpty {
            if selectedNewUser != Session.sharedInstance.userToken {
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
                openNewVC.conversationReceiverId = selectedNewUser
                self.conversationReceiverId = selectedNewUser
                openNewVC.messagesWall = true
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        } else {
            showSimpleAlert(title: "Choose user".localized(), message: "Tap on the field and choose desired user.".localized())
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notificationForCurrentThreadCheck"), object: nil)
    }
    
    @IBOutlet weak var newUserContainer: UIView!
    @IBOutlet weak var autocompleteTextField: UITextField!
    @IBOutlet weak var autocompleteTableViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var autocompleteTable: UITableView!
}

extension DirectMsgsTableVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView != autocompleteTable {
            return self.conversationData.conversationReceiverIdArr.count
        } else {
            return self.filterdReceiverNameArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView != autocompleteTable {
            return 70
        } else {
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        if tableView != autocompleteTable {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "DirectMsgsNVC") as! DirectMsgsNVC
            cell.titleLabel.text = conversationData.conversationReceiverNameArr[indexPath.row]
            cell.redDot.isHidden = false
            //tady je to ! dokud nebudou chtít aby to bylo přečtené
            if !conversationData.conversationReadArr[indexPath.row] {
                cell.redDot.isHidden = true
            }
            cell.imgIcon.backgroundColor = UIColor.clear
            cell.imgIcon.image = UIImage(named: "placeholder_photo")
            cell.imgIcon.layer.cornerRadius = cell.imgIcon.frame.height/2
            cell.imgIcon.clipsToBounds = true
            if !conversationData.conversationReceiverPhotoArr[indexPath.row].isEmpty {
                let tempurl = URL(string: conversationData.conversationReceiverPhotoArr[indexPath.row])
                cell.imgIcon.hnk_setImage(from: tempurl)
            }
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "UserDropDownCellVC") as! UserDropDownCellVC
            cell.titleLabel.text = filterdReceiverNameArr[indexPath.row]
            cell.imageIcon.image = UIImage(named: "placeholder_photo")
            if let temp = receiverData.receiverNameArr.index(of: filterdReceiverNameArr[indexPath.row]) {
                if !receiverData.receiverPhotoArr[temp].isEmpty {
                    let tempurl = URL(string: receiverData.receiverPhotoArr[temp])
                    cell.imageIcon.hnk_setImage(from: tempurl)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView != autocompleteTable {
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
            openNewVC.conversationReceiverId = conversationData.conversationReceiverIdArr[indexPath.row]
            self.conversationReceiverId = conversationData.conversationReceiverIdArr[indexPath.row]
            openNewVC.messagesWall = true
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
        } else {
            autocompleteTextField.text! = filterdReceiverNameArr[indexPath.row]
            if let temp = receiverData.receiverNameArr.index(of: filterdReceiverNameArr[indexPath.row]) {
                selectedNewUser = receiverData.receiverIdArr[temp]
                UIView.animate(withDuration: 0.3, animations: {
                    self.autocompleteTableViewHeightConstant.constant = 0
                })
            }
        }
    }
}

extension DirectMsgsTableVC: UITextFieldDelegate {
    
    func textFieldDidChange(_ textField: UITextField) {
        func filterContentForSearchText(searchText: String) {
            filterdReceiverNameArr = receiverData.receiverNameArr.filter { term in
                return term.lowercased().contains(searchText.lowercased())
            }
        }
        filterContentForSearchText(searchText: autocompleteTextField.text!)
        self.autocompleteTable.reloadData()
        var newHeight: CGFloat = 0
        switch filterdReceiverNameArr.count {
        case 0:
            newHeight = 0
        case 1:
            newHeight = 45
        case 2:
            newHeight = 90
        default:
            newHeight = 130
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.autocompleteTableViewHeightConstant.constant = newHeight
        })
    }
}

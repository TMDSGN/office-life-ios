//
//  RezervationActivityTableVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 22.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import iOS_Slide_Menu
import SwiftLoader
import Haneke

fileprivate struct ReservationData {
    
    var idArr, nameArr, photoArr, levelArr, fromDateArr, fromTimeArr, fromTimestampForDeleteArr: [String]
    
    init(idArr: [String], nameArr: [String], photoArr: [String], levelArr: [String], fromDateArr: [String], fromTimeArr: [String], fromTimestampForDeleteArr: [String]) {
        self.idArr = idArr
        self.nameArr = nameArr
        self.photoArr = photoArr
        self.levelArr = levelArr
        self.fromDateArr = fromDateArr
        self.fromTimeArr = fromTimeArr
        self.fromTimestampForDeleteArr = fromTimestampForDeleteArr
    }
}

fileprivate struct ActivityData {
    
    var idArr, nameArr, dateArr, backgroundArr, typeArr, placeArr, createdByNameArr, createdByPhotoArr: [String]
    
    init(idArr: [String], nameArr: [String], dateArr: [String], backgroundArr: [String], typeArr: [String], placeArr: [String], createdByNameArr: [String], createdByPhotoArr: [String]) {
        self.idArr = idArr
        self.nameArr = nameArr
        self.dateArr = dateArr
        self.backgroundArr = backgroundArr
        self.typeArr = typeArr
        self.placeArr = placeArr
        self.createdByNameArr = createdByNameArr
        self.createdByPhotoArr = createdByPhotoArr
    }
}

class MyProfileActivityTableVC: UIViewController {
    
    var rezervationBool = false
    fileprivate var doneWithLoadingRequests = false
    
    fileprivate var reservationData = ReservationData(idArr: [], nameArr: [], photoArr: [], levelArr: [], fromDateArr: [], fromTimeArr: [], fromTimestampForDeleteArr: [])
    fileprivate var activityData = ActivityData(idArr: [], nameArr: [], dateArr: [], backgroundArr: [], typeArr: [], placeArr: [], createdByNameArr: [], createdByPhotoArr: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObservers()
        customCellRegister()
        setupLabels()
    }
    
    @objc private func getReservationData(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ReservationReq().getReservationTypeList(type: "0", completionHandler: { (done, idArr, nameArr, photoArr, levelArr, fromDateArr, fromTimeArr, fromTimestampForDeleteArr) in
                SwiftLoader.hide()
                self.doneWithLoadingRequests = true
                if done {
                    self.reservationData = ReservationData(idArr: idArr, nameArr: nameArr, photoArr: photoArr, levelArr: levelArr, fromDateArr: fromDateArr, fromTimeArr: fromTimeArr, fromTimestampForDeleteArr: fromTimestampForDeleteArr)
                    self.tableView.setContentOffset(CGPoint.zero, animated: false)
                    DelayFunc.sharedInstance.delay(0.1, closure: {
                        self.tableView.reloadData()
                    })
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    private func getActivitiesData(){
        SwiftLoader.show(animated: true)
        if Reachability.isConnectedToNetwork() {
            ActivitiesReq().getActivitiesSpecificType(activityType: "0") { (done, idArr, nameArr, dateArr, backgroundArr, typeArr, placeArr, createdByNameArr, createdByPhotoArr, createdByIdArr) in
                SwiftLoader.hide()
                self.doneWithLoadingRequests = true
                if done {
                    self.activityData = ActivityData(idArr: idArr, nameArr: nameArr, dateArr: dateArr, backgroundArr: backgroundArr, typeArr: typeArr, placeArr: placeArr, createdByNameArr: createdByNameArr, createdByPhotoArr: createdByPhotoArr)
                    self.tableView.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func prepareToPopToNewContainer(_ nameOfVc:String){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: nameOfVc)
        DispatchQueue.main.async(execute: { () -> Void in
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        });
    }
    
    fileprivate func setupLabels(){
        if rezervationBool {
            title = "My Reservations".localized()
            getReservationData()
        } else {
            title = "My Activities".localized()
            getActivitiesData()
        }
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(MyProfileActivityTableVC.getReservationData), name: NSNotification.Name(rawValue: "reloadMyReservation"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadMyReservation"), object: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
}

extension MyProfileActivityTableVC {
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
}

extension MyProfileActivityTableVC: UITableViewDelegate, UITableViewDataSource {
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "MyRezervationCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MyRezervationCellVC")
        let nib2 = UINib(nibName: "MyActivityCellView", bundle: nil)
        tableView.register(nib2, forCellReuseIdentifier: "MyActivityCell")
        let nib3 = UINib(nibName: "MyEmptyCell", bundle: nil)
        tableView.register(nib3, forCellReuseIdentifier: "myEmptyCell")
        tableView.estimatedRowHeight = 200
        tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if rezervationBool {
            if reservationData.idArr.count == 0 && self.doneWithLoadingRequests {
                return 1
            } else {
                return reservationData.idArr.count
            }
        } else {
            if activityData.idArr.count == 0 && self.doneWithLoadingRequests {
                return 1
            } else {
                return activityData.idArr.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        if activityData.idArr.count == 0 && reservationData.idArr.count == 0 {
            let emptyCell = self.tableView.dequeueReusableCell(withIdentifier: "myEmptyCell") as! MyEmptyCellVC
            return emptyCell
        } else {
            if rezervationBool {
                let cellRezervation = self.tableView.dequeueReusableCell(withIdentifier: "MyRezervationCellVC") as! MyRezervationCellVC
                cellRezervation.selectionStyle = UITableViewCellSelectionStyle.none
                cellRezervation.titleLabel.text = reservationData.nameArr[indexPath.row]
                cellRezervation.descLabel.text = reservationData.levelArr[indexPath.row]
                cellRezervation.date.text = reservationData.fromDateArr[indexPath.row]
                cellRezervation.time.text = reservationData.fromTimeArr[indexPath.row]
                cellRezervation.reservationId = reservationData.idArr[indexPath.row]
                cellRezervation.created = reservationData.fromTimestampForDeleteArr[indexPath.row]
                if !reservationData.photoArr[indexPath.row].isEmpty {
                    let tempurl = URL(string: reservationData.photoArr[indexPath.row])
                    cellRezervation.mainIcon.hnk_setImage(from: tempurl)
                } else {
                    cellRezervation.mainIcon.backgroundColor = UIColor.black
                    cellRezervation.mainIcon.image = UIImage(named: "placeholder")
                }
                return cellRezervation
            } else {
                let cellActivity = self.tableView.dequeueReusableCell(withIdentifier: "MyActivityCell") as! MyActivityCellVC
                cellActivity.selectionStyle = UITableViewCellSelectionStyle.none
                cellActivity.userNameLabel.text = activityData.createdByNameArr[indexPath.row]
                cellActivity.userStreetLabel.text = activityData.placeArr[indexPath.row]
                cellActivity.userDateLabel.text = activityData.dateArr[indexPath.row]
                cellActivity.titleLabel.text = activityData.nameArr[indexPath.row]
                cellActivity.userProfileIcon.layer.cornerRadius = 20
                cellActivity.userProfileIcon.layer.masksToBounds = true
                cellActivity.activityIcon.image = UIImage(named: "activityType" + activityData.typeArr[indexPath.row])
                
                if !activityData.backgroundArr[indexPath.row].isEmpty {
                    let tempurl = URL(string: activityData.backgroundArr[indexPath.row])
                    cellActivity.mainIcon.hnk_setImage(from: tempurl)
                } else {
                    cellActivity.mainIcon.backgroundColor = UIColor.black
                    cellActivity.mainIcon.image = UIImage(named: "placeholder")
                }
                if !activityData.backgroundArr[indexPath.row].isEmpty {
                    let tempurl = URL(string: activityData.createdByPhotoArr[indexPath.row])
                    cellActivity.userProfileIcon.hnk_setImage(from: tempurl)
                } else {
                    cellActivity.userProfileIcon.backgroundColor = UIColor.black
                    cellActivity.userProfileIcon.image = UIImage(named: "placeholder_photo")
                }
                return cellActivity
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if activityData.idArr.count == 0 && reservationData.idArr.count == 0 {
            if rezervationBool {
                prepareToPopToNewContainer("ReservationTableVC")
            } else {
                prepareToPopToNewContainer("ActivityTableVC")
            }
        } else {
            if rezervationBool {
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ReservationDetailVC") as! ReservationDetailVC
                openNewVC.idOfObject = reservationData.idArr[indexPath.row]
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            } else {
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ActivityDetailVC") as! ActivityDetailVC
                openNewVC.activityId = activityData.idArr[indexPath.row]
                openNewVC.titleCategory = title!
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            }
        }
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}

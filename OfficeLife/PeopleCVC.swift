//
//  PeopleVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 30.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader

fileprivate struct PeopleData {
    
    var idArr, firstnameArr, surnameArr, photoArr, companyIdArr, contactArr, interestsArr: [String]
    
    init(idArr: [String], firstnameArr: [String], surnameArr: [String], photoArr: [String], companyIdArr: [String], contactArr: [String], interestsArr: [String]) {
        self.idArr = idArr
        self.firstnameArr = firstnameArr
        self.surnameArr = surnameArr
        self.photoArr = photoArr
        self.companyIdArr = companyIdArr
        self.contactArr = contactArr
        self.interestsArr = interestsArr
    }
}

class PeopleCVC:HamburgerContainerCVC {
    
    var idStr:String!
    fileprivate var peopleData = PeopleData(idArr: [], firstnameArr: [], surnameArr: [], photoArr: [], companyIdArr: [], contactArr: [], interestsArr: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObservers()
        switch idStr {
        case "1":
            getData(String(), interest: String())
        case "2":
            getData(Session.sharedInstance.profileCompany, interest: String())
        case "3":
            getData(String(), interest: Session.sharedInstance.profileInterest)
        default:
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIcon")
        setupCollectionLayout()
    }
    
    @objc fileprivate func reloadData(){
        collectionView?.reloadData()
    }
    
    fileprivate func getData(_ company:String,interest:String){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            PeopleReq().getPeople(company: company, interest: interest, completionHandler: { (done, idArr, firstnameArr, surnameArr, photoArr, companyIdArr, contactArr, interestsArr) in
                SwiftLoader.hide()
                if done {
                    self.peopleData = PeopleData(idArr: idArr, firstnameArr: firstnameArr, surnameArr: surnameArr, photoArr: photoArr, companyIdArr: companyIdArr, contactArr: contactArr, interestsArr: interestsArr)
                    self.collectionView?.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
            if Session.sharedInstance.idOfCompaniesArr.count == 0 {
                LoginReq().getCompanies { (done, idArr, nameArr, iconsArr, descArr, contactNameArr, contactEmailArr, contactPhotoArr) in
                    if done {
                        let s = Session.sharedInstance
                        s.idOfCompaniesArr = idArr
                        s.nameOfCompaniesArr = nameArr
                        s.iconsOfCompaniesArr = iconsArr
                        s.descOfCompaniesArr = descArr
                        self.collectionView?.reloadData()
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                    }
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func setupCollectionLayout(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 65, left: 15, bottom: 0, right: 15)
        layout.itemSize = CGSize(width: width / 2 - 23, height: 190)
        layout.minimumInteritemSpacing = 15
        layout.minimumLineSpacing = 15
        collectionView!.collectionViewLayout = layout
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(PeopleCVC.reloadData), name: NSNotification.Name(rawValue: "reloadDataInCells"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadDataInCells"), object: nil)
    }
}

extension PeopleCVC {

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PeopleCellVC", for: indexPath) as! PeopleCellVC
        cell.titleLabel.text = peopleData.firstnameArr[indexPath.row] + " " + peopleData.surnameArr[indexPath.row]
        if let i = Session.sharedInstance.idOfCompaniesArr.index(of: peopleData.companyIdArr[indexPath.row]) {
            let tempurl = URL(string:Session.sharedInstance.iconsOfCompaniesArr[i])
            cell.imgCompany.hnk_setImage(from: tempurl)
        } else {
            cell.imgCompany.image = UIImage()
        }
        if !peopleData.photoArr[indexPath.row].isEmpty {
            let tempurl = URL(string: peopleData.photoArr[indexPath.row])
            cell.imgProfile.hnk_setImage(from: tempurl, placeholder: UIImage(named:"placeholder_photo"), success: { (image) in
                cell.imgProfile.image = image
                cell.imgProfile.layer.cornerRadius = 40
                cell.imgProfile.clipsToBounds = true
            }, failure: { (err) in
                cell.imgProfile.image = UIImage(named: "placeholder_photo")
                cell.imgProfile.layer.cornerRadius = 0
                cell.imgProfile.clipsToBounds = true
                
            })
        } else {
            cell.imgProfile.image = UIImage(named: "placeholder_photo")
            cell.imgProfile.layer.cornerRadius = 0
            cell.imgProfile.clipsToBounds = true
        }
        cell.imgProfile.backgroundColor = UIColor.backgroundPhotoColor
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return peopleData.idArr.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "PeopleDetailVC") as! PeopleDetailVC
        openNewVC.userId = peopleData.idArr[indexPath.row]
        openNewVC.userFullName = peopleData.firstnameArr[indexPath.row] + " " + peopleData.surnameArr[indexPath.row]
        openNewVC.interestStr = peopleData.interestsArr[indexPath.row]
        openNewVC.contactStr = peopleData.contactArr[indexPath.row]
        openNewVC.userId = peopleData.idArr[indexPath.row]
        openNewVC.photoUser = peopleData.photoArr[indexPath.row]
        openNewVC.idCompany = peopleData.companyIdArr[indexPath.row]
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
}

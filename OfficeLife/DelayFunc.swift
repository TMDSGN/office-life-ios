//
//  DelayFunc.swift
//  Sphere
//
//  Created by Josef Antoni on 14.04.16.
//  Copyright © 2016 Pixelmate. All rights reserved.
//

import Foundation

class DelayFunc {
    static let sharedInstance = DelayFunc()
    fileprivate init() {}//This prevents others from using the default '()' initializer for this class.
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
}

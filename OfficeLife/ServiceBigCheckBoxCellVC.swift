//
//  ServiceBigCheckboxCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 04.03.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class ServiceBigCheckBoxCellVC: UITableViewCell {

    var prizePerProduct: String = ""
    fileprivate var totalPrice: String = ""
    fileprivate var isSwitcherOn = false
    
    var productTitle: String = ""
    var productPricePerUnit: String = ""

    override func awakeFromNib() {
        self.switcherContainer.layer.borderWidth = 0.5
        self.switcherContainer.layer.borderColor = UIColor.mainBlue.cgColor
        self.switcherContainer.layer.cornerRadius = 10
    }
    
    @IBAction func switcherPressedBtn(_ sender: Any) {
        manipulateWithSwitch()
    }
    
    fileprivate func manipulateWithSwitch(){
        self.switcherDot.backgroundColor = UIColor.mainBlue
        self.switcherDot.layer.cornerRadius = 8

        var temp:Double = 0.0
        if isSwitcherOn {
            isSwitcherOn = false
            UIView.animate(withDuration: 0.1, animations: {
                self.switcherDot.alpha = 0
            })
            temp = -1
        } else {
            isSwitcherOn = true
            UIView.animate(withDuration: 0.1, animations: {
                self.switcherDot.alpha = 1
            })
            temp = +1
        }
        let tempPrizePerProduct = prizePerProduct.components(separatedBy: " ")
        if let tempFirstItem = tempPrizePerProduct.first {
            if let tempDoubleFromString = Double(tempFirstItem.replacingOccurrences(of: ",", with: ".")) {
                let result: Double = temp * tempDoubleFromString
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "countFinalPrice"), object: ["prizePerProduct" : result, "totalProducts" : temp, "title": productTitle, "selected": isSwitcherOn, "quantity" : 1])
            }
        }
    }
    
    @IBOutlet weak var switcherContainer: UIView!
    @IBOutlet weak var switcherDot: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var finalPriceCountLabel: UILabel!
    @IBOutlet weak var container: UIView!
}

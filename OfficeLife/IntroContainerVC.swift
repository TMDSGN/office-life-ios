//
//  IntroContainerVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 26.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import EMPageViewController

class IntroContainerVC: UIViewController {
    
    var pageViewController: EMPageViewController?
    var currentPage: Int = 0
    var viewsArr: [Int] = [1,2,3,4,5]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObservers()
        setupLabels()
        setupPageVC()
        Session.sharedInstance.saveCurrentSession()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    fileprivate func setupPageVC(){
        let pageViewController = EMPageViewController()
        pageViewController.dataSource = self
        pageViewController.delegate = self
        let currentViewController = self.viewController(at: 0)!
        pageViewController.selectViewController(currentViewController, direction: .forward, animated: false, completion: nil)
        self.addChildViewController(pageViewController)
        self.view.insertSubview(pageViewController.view, at: 0)
        pageViewController.didMove(toParentViewController: self)
        self.pageViewController = pageViewController
    }
    
    fileprivate func setupLabels(){
        self.navigationController?.isNavigationBarHidden = true
        pageBtnOne.layer.cornerRadius = 5
        pageBtnTwo.layer.cornerRadius = 5
        pageBtnThree.layer.cornerRadius = 5
        pageBtnFour.layer.cornerRadius = 5
        pageBtnFifth.layer.cornerRadius = 5
        pageBtnOne.backgroundColor = UIColor.mainBlue
        pageBtnTwo.backgroundColor = UIColor.white
        pageBtnThree.backgroundColor = UIColor.white
        pageBtnFour.backgroundColor = UIColor.white
        pageBtnFifth.backgroundColor = UIColor.white
    }
    
    /*
    *   Transform page button according index.
    */
    @objc fileprivate func transformPageBtn(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let isPageOneSelected = dict["numberId"] as! Int
        switch isPageOneSelected {
        case 0:
            pageBtnOne.backgroundColor = UIColor.mainBlue
            pageBtnTwo.backgroundColor = UIColor.white
            pageBtnThree.backgroundColor = UIColor.white
            pageBtnFour.backgroundColor = UIColor.white
            pageBtnFifth.backgroundColor = UIColor.white
            
            pageOneWidth.constant = 25
            pageTwoWidth.constant = 10
            pageThreeWidth.constant = 10
            pageTourWidth.constant = 10
            pageFifthWidth.constant = 10
            
            UIView.animate(withDuration: 0.3){
                self.view.layoutIfNeeded()
            }
        case 1:
            pageBtnOne.backgroundColor = UIColor.white
            pageBtnTwo.backgroundColor = UIColor.mainBlue
            pageBtnThree.backgroundColor = UIColor.white
            pageBtnFour.backgroundColor = UIColor.white
            pageBtnFifth.backgroundColor = UIColor.white
            
            pageOneWidth.constant = 10
            pageTwoWidth.constant = 25
            pageThreeWidth.constant = 10
            pageTourWidth.constant = 10
            pageFifthWidth.constant = 10
            
            UIView.animate(withDuration: 0.3){
                self.pageContainer.alpha = 1
                self.view.layoutIfNeeded()
            }
        case 2:
            pageBtnTwo.backgroundColor = UIColor.white
            pageBtnOne.backgroundColor = UIColor.white
            pageBtnThree.backgroundColor = UIColor.mainBlue
            pageBtnFour.backgroundColor = UIColor.white
            pageBtnFifth.backgroundColor = UIColor.white
            
            pageOneWidth.constant = 10
            pageTwoWidth.constant = 10
            pageThreeWidth.constant = 25
            pageTourWidth.constant = 10
            pageFifthWidth.constant = 10
            
            UIView.animate(withDuration: 0.3){
                self.pageContainer.alpha = 1
                self.view.layoutIfNeeded()
            }
        case 3:
            pageBtnTwo.backgroundColor = UIColor.white
            pageBtnOne.backgroundColor = UIColor.white
            pageBtnThree.backgroundColor = UIColor.white
            pageBtnFour.backgroundColor = UIColor.mainBlue
            pageBtnFifth.backgroundColor = UIColor.white
            
            pageOneWidth.constant = 10
            pageTwoWidth.constant = 10
            pageThreeWidth.constant = 10
            pageTourWidth.constant = 25
            pageFifthWidth.constant = 10
            
            UIView.animate(withDuration: 0.3){
                self.pageContainer.alpha = 1
                self.view.layoutIfNeeded()
            }
        case 4:
            pageBtnTwo.backgroundColor = UIColor.white
            pageBtnOne.backgroundColor = UIColor.white
            pageBtnThree.backgroundColor = UIColor.white
            pageBtnFour.backgroundColor = UIColor.white
            pageBtnFifth.backgroundColor = UIColor.mainBlue
            
            pageOneWidth.constant = 10
            pageTwoWidth.constant = 10
            pageThreeWidth.constant = 10
            pageTourWidth.constant = 10
            pageFifthWidth.constant = 25
            
            UIView.animate(withDuration: 0.3){
                self.pageContainer.alpha = 0
                self.view.layoutIfNeeded()
            }
        default:
            break
        }
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(IntroContainerVC.transformPageBtn), name: NSNotification.Name(rawValue: "IntroContainerVCScroll"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "IntroContainerVCScroll"), object: nil)
    }
    
    @IBOutlet weak var pageContainer: UIView!
    @IBOutlet weak var pageBtnOne: UIView!
    @IBOutlet weak var pageBtnTwo: UIView!
    @IBOutlet weak var pageBtnThree: UIView!
    @IBOutlet weak var pageBtnFour: UIView!
    @IBOutlet weak var pageBtnFifth: UIView!
    @IBOutlet weak var pageOneWidth: NSLayoutConstraint!
    @IBOutlet weak var pageTwoWidth: NSLayoutConstraint!
    @IBOutlet weak var pageThreeWidth: NSLayoutConstraint!
    @IBOutlet weak var pageTourWidth: NSLayoutConstraint!
    @IBOutlet weak var pageFifthWidth: NSLayoutConstraint!
}

extension IntroContainerVC {
    
    @IBAction func firstSelectedBtn(_ sender: Any) {
        setupCurrentPage(0)
    }
    
    @IBAction func secondSelectedBtn(_ sender: Any) {
        setupCurrentPage(1)
    }
    
    @IBAction func thirdSelectedBtn(_ sender: Any) {
        setupCurrentPage(2)
    }
    
    @IBAction func fourthSelectedBtn(_ sender: Any) {
        setupCurrentPage(3)
    }
    
    @IBAction func fifthSelectedBtn(_ sender: Any) {
        setupCurrentPage(4)
    }
}

extension IntroContainerVC: EMPageViewControllerDataSource, EMPageViewControllerDelegate {
    
    fileprivate func scrollTo(index:Int) {
        let selectedIndex = self.index(of: self.pageViewController!.selectedViewController as! IntroContainerPageVC)!
        let viewController = self.viewController(at: index)!
        let direction:EMPageViewControllerNavigationDirection = index > selectedIndex ? .forward : .reverse
        self.pageViewController!.selectViewController(viewController, direction: direction, animated: true, completion: nil)
    }
    
    fileprivate func scrollAnimation(_ currentPage:Int){
        let selectedIndex = self.index(of: self.pageViewController!.selectedViewController as! IntroContainerPageVC)!
        let direction:EMPageViewControllerNavigationDirection = currentPage > selectedIndex ? .forward : .reverse
        self.currentPage = currentPage
        let viewController = self.viewController(at: currentPage)!
        self.pageViewController!.selectViewController(viewController, direction: direction, animated: true, completion: nil)
    }
    
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! IntroContainerPageVC) {
            let beforeViewController = self.viewController(at: viewControllerIndex - 1)
            currentPage = viewControllerIndex
            return beforeViewController
        } else {
            return nil
        }
    }
    
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! IntroContainerPageVC) {
            let afterViewController = self.viewController(at: viewControllerIndex + 1)
            currentPage = viewControllerIndex
            return afterViewController
        } else {
            return nil
        }
    }
    
    fileprivate func viewController(at index: Int) -> IntroContainerPageVC? {
        if (self.viewsArr.count == 0) || (index < 0) || (index >= self.viewsArr.count) {
            return nil
        }
        
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "IntroContainerPageVC") as! IntroContainerPageVC
        viewController.idStr = self.viewsArr[index]
        return viewController
    }
    
    fileprivate func index(of viewController: IntroContainerPageVC) -> Int? {
        if let indexOfVC: Int = viewController.idStr {
            return self.viewsArr.index(of: indexOfVC)
        } else {
            return nil
        }
    }
    
    /*
    *   Move to screen with index.
    */
    fileprivate func setupCurrentPage(_ index:Int){
        if currentPage != index {
            scrollTo(index: index)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "IntroContainerVCScroll"), object: ["numberId" : index])
        }
        currentPage = index
    }
}

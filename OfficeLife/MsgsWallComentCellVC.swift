//
//  MsgsWallComentCellView.swift
//  OfficeLife
//
//  Created by Josef Antoni on 14.02.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class MsgsWallComentCellVC: UITableViewCell {
    
    override func awakeFromNib() {
        imgIcon.layer.cornerRadius = imgIcon.frame.size.height/2
        imgIcon.clipsToBounds = true
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var descLabel: UITextView!
    @IBOutlet weak var createdLabel: UILabel!
    
}

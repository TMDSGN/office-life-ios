//
//  TypePickerVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 11.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class TypePickerVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TypePickerVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        setupLabels()
    }
    
    fileprivate func setupLabels(){
        self.navigationController?.navigationBar.isHidden = true
        self.cameraBtn.alpha = 0
        self.photoBtn.alpha = 0
        self.cameraBtn.backgroundColor = UIColor.mainBlue
        self.photoBtn.backgroundColor = UIColor.mainBlue
        DelayFunc.sharedInstance.delay(0.5) {
            UIView.animate(withDuration: 0.2 ,animations: {
                self.cameraBtn.alpha = 1
                self.photoBtn.alpha = 1
                
            },completion: nil)
        }
        
        cameraBtn.setTitle("Camera".localized(), for: .normal)
        photoBtn.setTitle("Photos".localized(), for: .normal)
    }
    
    func dismissKeyboard() {
        UIView.animate(withDuration: 0.3 ,animations: {
            self.cameraBtn.alpha = 0
            self.photoBtn.alpha = 0
            
        },completion: { finish in
            self.dismiss(animated: false)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "enableTabBar"), object: nil)
        })
    }
    
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var photoBtn: UIButton!
}

extension TypePickerVC {

    @IBAction func cameraBtn(_ sender: AnyObject) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "openCamera"), object: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func photosBtn(_ sender: AnyObject) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "openLibrary"), object: nil)
        self.dismiss(animated: false, completion: nil)
    }
}

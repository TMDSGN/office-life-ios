//
//  NotificationCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 27.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit

class NotificationCellVC: UITableViewCell {

    var idUser: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgIconContainer.layer.cornerRadius = imgIconContainer.frame.size.height/2
        imgIconContainer.layer.masksToBounds = true
    }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgIconContainer: UIView!
}

extension NotificationCellVC {
    @IBAction func openProfileBtn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openPeopleProfile"), object: ["userId" : idUser])
    }
}

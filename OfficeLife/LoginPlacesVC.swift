//
//  LoginPlacesVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class LoginPlaces: UIViewController, AlertMessage {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Loader().configLoader()
        registerObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDefaultValues()
        removeShadowBorderOnNavBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Session.sharedInstance.getCurrentSession()
        if !Session.sharedInstance.userToken.isEmpty && !Session.sharedInstance.securityToken.isEmpty {
            if Session.sharedInstance.isAfterRegistrationSelectedCompany {
                SwiftLoader.show(animated: true)
                Authentization().getToken { (_) in
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.performSegue(withIdentifier: "alreadySigned", sender: nil)
                    });
                }
            }
        }
    }
    
    @objc fileprivate func showLoginScreen(){
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")
        DispatchQueue.main.async(execute: { () -> Void in
            _ = self.navigationController?.popToRootViewController(animated: false)
            self.navigationController?.pushViewController(openNewVC!, animated: false)
        });
    }
    
    fileprivate func setupDefaultValues(){
        loginBtn.setTitle("Log in".localized(), for: .normal)
        registrationBtn.setTitle("Registration".localized(), for: .normal)
        titleLabel.text = "Enjoy exlusive convenience".localized()
    }
    
    /*
     * Remove the tiny line between transcluent Navigation bar and scene.
     */
    fileprivate func removeShadowBorderOnNavBar(){
        for parent in navigationController!.view.subviews {
            for child in parent.subviews {
                for view in child.subviews {
                    if view is UIImageView && view.frame.height == 0.5 {
                        view.alpha = 0
                    }
                }
            }
        }
    }
    
    /*
     *  Error occured so log out and return to main screen.
     */
    @objc fileprivate func serverErr(){
        SwiftLoader.hide()
        let alert = UIAlertController(title: "Error".localized(), message: "An error occurred on the server side, try it again later.".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            Session.sharedInstance.userToken = ""
            Session.sharedInstance.securityToken = ""
            Session.sharedInstance.saveCurrentSession()
            self.popToRootViewControllerAnimated()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
     *  Error occured but dont log out.
     */
    @objc fileprivate func serverErrNonDestructable(){
        SwiftLoader.hide()
        showSimpleAlert(title: "Error".localized(), message: "An error occurred on the server side, try it again later.".localized())
    }
    
    /*
     *  Connection was lost, return to main screen.
     */
    @objc fileprivate func connectionErr(){
        SwiftLoader.hide()
        let alert = UIAlertController(title: "No internet".localized(), message: "To use the application is an internet connection needed.".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            self.popToRootViewControllerAnimated()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc fileprivate func logOutAction(){
        self.popToRootViewControllerAnimated()
    }
    
    /*
    *   Discard everything that was helded and return to main screen.
    */
    @objc fileprivate func popToRootViewControllerAnimated() {
        SwiftLoader.hide()
        Session.sharedInstance.userToken = ""
        Session.sharedInstance.securityToken = ""
        Session.sharedInstance.facebookAcc = false
        Session.sharedInstance.isAfterRegistrationSelectedCompany = false
        Session.sharedInstance.isAfterRegistration = false
        Session.sharedInstance.profilePhoto = ""
        Session.sharedInstance.profileFirstName = ""
        Session.sharedInstance.profileSurName = ""
        Session.sharedInstance.profileEmail = ""
        Session.sharedInstance.profilePhone = ""
        Session.sharedInstance.profileCompany = ""
        Session.sharedInstance.profileFeedFromMe = false
        Session.sharedInstance.profilePoints = ""
        Session.sharedInstance.profileInterest = ""
        Session.sharedInstance.saveCurrentSession()
        DispatchQueue.main.async(execute: { () -> Void in
            _ = self.navigationController?.popToRootViewController(animated: true)
        })
    }
    
    fileprivate func registerObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.showLoginScreen), name: NSNotification.Name(rawValue: "showLoginScreen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.popToRootViewControllerAnimated), name: NSNotification.Name(rawValue: "popToRoot"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.connectionErr), name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.serverErr), name: NSNotification.Name(rawValue: "serverErr"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.logOutAction), name: NSNotification.Name(rawValue: "logOut"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.serverErrNonDestructable), name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "showLoginScreen"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "popToRoot"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "serverErr"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "logOut"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
    }
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registrationBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pinContainer: UIView!
}

extension LoginPlaces {
    
    @IBAction func loginPageBtn(_ sender: AnyObject) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC!, animated: true)
        });
    }
    
    @IBAction func registrationPageBtn(_ sender: AnyObject) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC")
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC!, animated: true)
        });
    }
}

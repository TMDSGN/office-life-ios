//
//  AddCreditVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 16.03.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class AddCreditVC: UIViewController, AlertMessage {

    fileprivate var iPaidAndWantToGoBackBool = false

    override func viewDidLoad() {
        registerObservers()
        setupLabels()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        if iPaidAndWantToGoBackBool {
            dismiss(animated: false, completion: nil)
        }
    }
    
    @objc fileprivate func iPaidAndWantToGoBack(){
        iPaidAndWantToGoBackBool = true
    }
    
    fileprivate func setupLabels(){
        creditsField.placeholder = "Set value u want to add".localized()
        addBtn.setTitle("Add credit".localized(), for: .normal)
        phoneField.placeholder = "phone".localized()
        creditsContainer.layer.borderWidth = 1
        creditsContainer.layer.borderColor = UIColor.mainBlue.cgColor
        mailContainer.layer.borderWidth = 1
        mailContainer.layer.borderColor = UIColor.mainBlue.cgColor
        phoneContainer.layer.borderWidth = 1
        phoneContainer.layer.borderColor = UIColor.mainBlue.cgColor
        emailField.text = Session.sharedInstance.profileEmail
        phoneField.text = Session.sharedInstance.profilePhone
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.iPaidAndWantToGoBack), name: NSNotification.Name(rawValue: "iPaidAndWantToGoBack"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "iPaidAndWantToGoBack"), object: nil)
    }
    
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var phoneContainer: UIView!
    @IBOutlet weak var creditsField: UITextField!
    @IBOutlet weak var creditsContainer: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var mailContainer: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var container: UIView!
    
}

extension AddCreditVC {

    @IBAction func addBtn(_ sender: Any) {
        if !emailField.text!.isEmpty {
            if !phoneField.text!.isEmpty {
                if Reachability.isConnectedToNetwork() {
                    if self.creditsField.text!.isNumber {
                        SwiftLoader.show(animated: true)
                        ServicesReq().orderService(serviceTypeBig: false, service_id: "2686", pieces: self.creditsField.text!, phone: phoneField.text!, descNote: "", paymentMethod: "gopayredirbinder", productTitleForSentArray:[], productPriceForSentArray:[], productQuantityForSentArray:[], returnUrlForPay: true, email: emailField.text!, isCompanyOrder: false, companyName: "", companyBid: "", companyVat:"", companyZip: "", companyAddress: "") { (done, url) in
                            SwiftLoader.hide()
                            if done {
                                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewGoPayVC") as! WebViewGoPayVC
                                openNewVC.targetUrl = url
                                self.navigationController?.isNavigationBarHidden = false
                                DispatchQueue.main.async(execute: { () -> Void in
                                    self.navigationController?.pushViewController(openNewVC, animated: true)
                                });
                            } else {
                                self.showSimpleAlert(title: "Error".localized(), message: "Your phone number or email are not valid.".localized())
                            }
                        }
                    }
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
                }
            } else {
                showSimpleAlert(title: "Error".localized(), message: "Your phone number is needed.".localized())
            }
        } else {
            showSimpleAlert(title: "Error".localized(), message: "Check your email".localized())
        }
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        dismiss(animated: false, completion: nil)
    }
}

extension String  {
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
}

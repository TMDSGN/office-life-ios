//
//  MsgsReq.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MsgsReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getReceivers(completionHandler: @escaping (Bool, [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/messages/v1/receivers"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                var idArr = [String]()
                var nameArr = [String]()
                var photoArr = [String]()

                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let name = json[i]["name"].stringValue
                    let photo = json[i]["photo"].stringValue
                    idArr.append(id)
                    nameArr.append(name)
                    photoArr.append(photo)
                }
                
                completionHandler(true, idArr, nameArr, photoArr)
            } else {
                completionHandler(false, [String](), [String](), [String]())
            }
        }
    }
    
    func getConversations(completionHandler: @escaping (Bool, [Bool], [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/messages/v1/conversations"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            
            if response.result.isSuccess {
                let data = response.result.value
                let json = JSON(data!)
                var readArr = [Bool]()
                var previewTextArr = [String]()
                var receiverIdArr = [String]()
                var receiverNameArr = [String]()
                var receiverPhotoArr = [String]()
                
                for i in 0..<json.count{
                    let read = json[i]["read"].boolValue
                    let previewText = json[i]["preview_text"].stringValue
                    let receiverId = json[i]["receiver"]["id"].stringValue
                    let receiverName = json[i]["receiver"]["name"].stringValue
                    let receiverPhoto = json[i]["receiver"]["photo"].stringValue
                    readArr.append(read)
                    previewTextArr.append(previewText)
                    receiverIdArr.append(receiverId)
                    receiverNameArr.append(receiverName)
                    receiverPhotoArr.append(receiverPhoto)
                }
                
                completionHandler(true, readArr, previewTextArr, receiverIdArr, receiverNameArr, receiverPhotoArr)
            } else {
                completionHandler(false, [Bool](), [String](), [String](), [String](), [String]())
            }
        }
    }

    func getConversationsBetweenPersons(userId: String, completionHandler: @escaping (Bool, [Bool], [String], [String], [String], [String], [String], [Bool], [[String]]) -> ()) -> (){
        
        let url = urlDomain + "/messages/v1/conversation/\(userId)"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                var readArr = [Bool]()
                var previewTextArr = [String]()
                var receiverIdArr = [String]()
                var receiverNameArr = [String]()
                var receiverPhotoArr = [String]()
                var dateArr = [String]()
                var myPostArr = [Bool]()
                var attachmentPhotoArr = [[String]]()

                for i in 0..<json.count{
                    let read = json[i]["read"].boolValue
                    let previewText = json[i]["text"].stringValue
                    let receiverId = json[i]["receiver"]["id"].stringValue
                    let receiverName = json[i]["receiver"]["name"].stringValue
                    let receiverPhoto = json[i]["receiver"]["photo"].stringValue
                    let date = json[i]["date"].doubleValue
                    let myPost = json[i]["my_post"].boolValue
                    
                    readArr.append(read)
                    previewTextArr.append(previewText)
                    receiverIdArr.append(receiverId)
                    receiverNameArr.append(receiverName)
                    receiverPhotoArr.append(receiverPhoto)
                    dateArr.append(DateConvertor.sharedInstance.timeAgoSinceDate(date: DateConvertor.sharedInstance.getDate(timeStamp: date, format: "dd.MM.yyyy HH:mm"), numericDates: true))
                    myPostArr.append(myPost)
                    
                    var tempPhotoArr = [String]()
                    for j in 0..<json[i]["photos"].count{
                        let photo = json[i]["photos"][j].stringValue
                        tempPhotoArr.append(photo)
                    }
                    attachmentPhotoArr.append(tempPhotoArr)
                }
                
                completionHandler(true, readArr, previewTextArr, receiverIdArr, receiverNameArr, receiverPhotoArr, dateArr, myPostArr, attachmentPhotoArr)
            } else {
                completionHandler(false, [Bool](), [String](), [String](), [String](), [String](), [String](), [Bool](), [[String]]())
            }
        }
    }
    
    func postNewMsg(userConversationId:String, text:String, photo:[String], completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/messages/v1/conversation/\(userConversationId)"
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        var para = [
            "text" : text
            ] as [String : Any]
        if photo != [String]() {
            para = [
                "text" : text,
                "photos" : photo
            ]
        }
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 201 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
}

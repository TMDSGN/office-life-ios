//
//  OrdersReq.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class OrdersReq {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getMyOrders(completionHandler: @escaping (Bool, [String], [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/orders/v1/my_orders"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                var idArr = [String]()
                var orderNameArr = [String]()
                var timeArr = [String]()
                var statusArr = [String]()
                var priceArr = [String]()
                
                for i in 0..<json.count{
                    let id = json[i]["id"].stringValue
                    let orderName = json[i]["order_name"].stringValue
                    let time = json[i]["date"].doubleValue
                    let status = json[i]["status"].stringValue
                    let price = json[i]["price"].stringValue
                    
                    idArr.append(id)
                    orderNameArr.append(orderName)
                    timeArr.append(DateConvertor.sharedInstance.getDateStr(timeStamp: time, format: "dd.MM.yyyy"))
                    statusArr.append(status)
                    priceArr.append(price)
                }
                
                completionHandler(true, idArr, orderNameArr, timeArr, statusArr, priceArr)
            } else {
                completionHandler(false, [String](), [String](), [String](), [String](), [String]())
            }
        }
    }
    
    func getOrderDetail(orders_id:String, completionHandler: @escaping (Bool, String, String, String, String, String, [String], [String], [String], [String]) -> ()) -> (){
        
        let url = urlDomain + "/orders/v1/\(orders_id)"
        
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                
                let orderName = json["order_name"].stringValue
                let time = json["date"].doubleValue//timestamp
                let status = json["status"].stringValue
                let price = json["price"].stringValue
                let timeStr = DateConvertor.sharedInstance.getDateStr(timeStamp: time, format: "dd.MM.yyyy")
                let paymentMethod = json["payment_method"].stringValue
                
                var addonsArr = [String]()
                var priceArr = [String]()
                var quantityArr = [String]()
                var titleArr = [String]()
                
                for i in 0..<json["services"].count{
                    let addonsPerItem = json["services"][i]["addons"].stringValue
                    let pricePerItem = json["services"][i]["price"].stringValue
                    let quantityPerItem = json["services"][i]["quantity"].stringValue
                    let titlePerItem = json["services"][i]["title"].stringValue
                    addonsArr.append(addonsPerItem)
                    priceArr.append(pricePerItem)
                    quantityArr.append(quantityPerItem)
                    titleArr.append(titlePerItem)
                }
                
                completionHandler(true, paymentMethod, orderName, timeStr, status, price, addonsArr, priceArr, quantityArr, titleArr)
            } else {
                completionHandler(false, String(), String(), String(), String(), String(), [String](), [String](), [String](), [String]())
            }
        }
    }
}

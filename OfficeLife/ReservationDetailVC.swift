//
//  ReservationDetailVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 05.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader
import Haneke
import DDCalendarView
import EventKit
import EventKitUI

//pro event v kalendáři.
class EventView: DDCalendarEventView {
    override var active : Bool {
        didSet {
            self.backgroundColor = UIColor.red.withAlphaComponent(0.5)
            self.layer.borderColor = nil
            self.layer.borderWidth = 0
        }
    }
}

class ReservationDetailVC: UIViewController {
    
    fileprivate var hourFromArr = [String]()
    fileprivate var minuteFromArr = [String]()
    fileprivate var hourToArr = [String]()
    fileprivate var minuteToArr = [String]()
    fileprivate var currentDay = Date()
    var idOfObject: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObserver()
        setupLabels()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            self.calendarView.scrollDateToVisible(self.currentDay, animated: false)
        })
    }
    
    fileprivate func getData(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ReservationReq().getReservationDetail(type: idOfObject) { (done, name, description, photo) in
                SwiftLoader.hide()
                if done {
                    self.titleLabel.text = name
                    self.desctxtView.text = description
                    if !photo.isEmpty {
                        let tempurl = URL(string: photo)
                        self.imgIcon.hnk_setImage(from: tempurl)
                    } else {
                        self.imgIcon.backgroundColor = UIColor.backgroundPhotoColor
                        self.imgIcon.image = UIImage(named: "placeholder")
                    }
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func getAvailability(targetDate: Date){
        if Reachability.isConnectedToNetwork() {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            formatter.timeZone = TimeZone.current
            let result = formatter.date(from: formatter.string(from: currentDay))!.timeIntervalSince1970
            ReservationReq().getReservationAvailibility(type: idOfObject, daytimestamp: String(describing: result) ,completionHandler: { (done, hourFromArr, minuteFromArr, hourToArr, minuteToArr) in
                if done {
                    self.hourFromArr = hourFromArr
                    self.minuteFromArr = minuteFromArr
                    self.hourToArr = hourToArr
                    self.minuteToArr = minuteToArr
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.calendarView.reloadData()
                    })
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @objc fileprivate func reservationSuccess(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let temp = dict["dayStr"] as! String
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "GratzAlertVC") as! GratzAlertVC
        modal.type = 3
        modal.dateStr = temp
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: false, completion: {
            self.getData()
        })
    }
    
    fileprivate func setupLabels(){
        self.calendarView.showsDayName = false
        title = "Booking".localized()
        blueBtn.setTitle("Order".localized(), for: .normal)
        descLabel.text = "Description".localized()
        availabilityLabel.text = "Availability".localized()
        previousBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
    }
    
    fileprivate func registerObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(ReservationDetailVC.reservationSuccess), name: NSNotification.Name(rawValue: "executeReservationSuccess"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "executeReservationSuccess"), object: nil)
    }
    
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var availableView: UIView!
    @IBOutlet weak var calendarDateLabel: UILabel!
    @IBOutlet weak var descLeading: NSLayoutConstraint!
    @IBOutlet weak var descTrailing: NSLayoutConstraint!
    @IBOutlet weak var availableLeading: NSLayoutConstraint!
    @IBOutlet weak var availableTrailing: NSLayoutConstraint!
    @IBOutlet weak var lowerViewConstant: NSLayoutConstraint!
    @IBOutlet weak var higherViewConstant: NSLayoutConstraint!
    @IBOutlet weak var blueBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var desctxtView: UITextView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var previousBtn: UIButton!
    @IBOutlet var calendarView: DDCalendarView!;
}

extension ReservationDetailVC {
    
    // MARK: - IBActions
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    @IBAction func createReservationBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ReservationDetailSelectionVC") as! ReservationDetailSelectionVC
        openNewVC.reservationId = idOfObject
        openNewVC.nameOfProduct = self.titleLabel.text!
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func showPreviousDateBtn(_ sender: Any) {
        let nextDay = Calendar.current.date(byAdding: .day, value: -1, to: currentDay)!
        currentDay = nextDay
        DispatchQueue.main.async(execute: { () -> Void in
            self.calendarView.scrollDateToVisible(nextDay, animated: true)
        });
    }
    
    @IBAction func showNextDateBtn(_ sender: Any) {
        let nextDay = Calendar.current.date(byAdding: .day, value: 1, to: currentDay)!
        currentDay = nextDay
        DispatchQueue.main.async(execute: { () -> Void in
            self.calendarView.scrollDateToVisible(nextDay, animated: true)
        })
    }

    @IBAction func descriptionBtn(_ sender: Any) {
        descLeading.isActive = true
        availableLeading.isActive = false
        UIView.animate(withDuration: 0.1, animations: {
            self.availableView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (true) in
            self.descTrailing.isActive = true
            self.availableTrailing.isActive = false
            UIView.animate(withDuration: 0.1, animations: {
                self.descView.alpha = 1
                self.view.layoutIfNeeded()
            }) { (true) in
                self.lowerViewConstant.isActive = true
                self.higherViewConstant.isActive = false
                UIView.animate(withDuration: 0.7, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @IBAction func availableBtn(_ sender: Any) {
        descTrailing.isActive = false
        availableTrailing.isActive = true
        UIView.animate(withDuration: 0.1, animations: {
            self.descView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (true) in
            self.descLeading.isActive = false
            self.availableLeading.isActive = true
            UIView.animate(withDuration: 0.1, animations: {
                self.availableView.alpha = 1
                self.view.layoutIfNeeded()
            }) { (true) in
                self.lowerViewConstant.isActive = false
                self.higherViewConstant.isActive = true
                UIView.animate(withDuration: 0.7, animations: {
                    self.view.layoutIfNeeded()
                })
                DelayFunc.sharedInstance.delay(0.7, closure: {
                    self.calendarView.scrollDateToVisible(Date(), animated: true)
                })
            }
        }
    }
}

extension ReservationDetailVC: DDCalendarViewDelegate, DDCalendarViewDataSource, EKEventViewDelegate {
    
    /*
     *   Add your events and display them on screen
     */
    func events(_ dayMod: Int) -> [Any] {
        var calendarEventsArr = [DDCalendarEvent]()
        for i in 0..<hourFromArr.count {
            let hourFrom: Int = Int(hourFromArr[i])!
            let minuteFrom: Int = Int(minuteFromArr[i])!
            let hourTo: Int = Int(hourToArr[i])!
            let minuteTo: Int = Int(minuteToArr[i])!
            let event = DDCalendarEvent()
            event.title = ""
            event.dateBegin = NSDate(hour: hourFrom, min: minuteFrom, inDays: dayMod) as Date!
            event.dateEnd = NSDate(hour: hourTo, min: minuteTo, inDays: dayMod) as Date!
            calendarEventsArr.append(event)
        }
        return calendarEventsArr
    }
    
    func calendarView(_ view: DDCalendarView, eventsForDay date: Date) -> [Any]? {
        let newDate = date as NSDate
        let daysMod: Int = newDate.days(from: Date())
        let newE: [DDCalendarEvent] = self.events(daysMod) as! [DDCalendarEvent]
        var dates = [Any]()
        for e: DDCalendarEvent in newE {
            // if e.dateBegin == date || e.dateEnd == date {
            dates.append(e)
            // }
        }
        return dates
    }
    
    func calendarView(_ view: DDCalendarView, viewFor event: DDCalendarEvent) -> DDCalendarEventView? {
        return EventView(event: event)
    }
    
    func eventViewController(_ controller: EKEventViewController, didCompleteWith action: EKEventViewAction) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func calendarView(_ view: DDCalendarView, focussedOnDay date: Date) {
        currentDay = date
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        calendarDateLabel.text = formatter.string(from: currentDay as Date)
        getAvailability(targetDate: currentDay)
    }
    
    func calendarView(_ view: DDCalendarView, allowEditing event: DDCalendarEvent) -> Bool {
        return false
    }
}

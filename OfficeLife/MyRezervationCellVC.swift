//
//  MyRezervationCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 22.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class MyRezervationCellVC: UITableViewCell {
    
    var reservationId: String = ""
    var created: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        rightCornerIcon.layer.cornerRadius = 10
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    @IBAction func deleteReservation(_ sender: Any) {
        SwiftLoader.show(animated: true)
        ReservationReq().deleteReservation(reservationId: reservationId, created: created) { (done) in
            SwiftLoader.hide()
            if done {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMyReservation"), object: nil)
            }
        }
    }
    
    @IBOutlet weak var rightCornerIcon: UIView!
    @IBOutlet weak var mainIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    
}

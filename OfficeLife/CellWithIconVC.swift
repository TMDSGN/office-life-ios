//
//  CellWithIconVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 15.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class CellWithIconVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftImg: UIImageView!
    @IBOutlet weak var rightImg: UIImageView!
    @IBOutlet weak var blueView: UIView!
}

//
//  IntroSelectCompanyNameVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 26.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader
import DropDown

class IntroSelectCompanyNameVC: UIViewController, AlertMessage {
    
    fileprivate let dropDown = DropDown()
    var selectedCompanyId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.complexLabel.text = "Choose your company".localized()
        self.welcomeTitleLabel.text = "Welcome to OfficeLife".localized()
        self.continueBtn.setTitle("Learn about app advantages".localized(), for: .normal)
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDropDown()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        self.pinContainer.isHidden = true
        if Session.sharedInstance.facebookAcc {
            self.pinContainer.isHidden = false
            pinField.placeholder = "Entry code".localized()
            pinField.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
        }
    }
    
    fileprivate func getData(){
        if Reachability.isConnectedToNetwork() {
            Authentization().getToken(completionHandler: { (done) in
                if done {
                    LoginReq().getCompanies { (done, idArr, nameArr, iconsArr, descArr, contactNameArr, contactEmailArr, contactPhotoArr) in
                        SwiftLoader.hide()
                        if done {
                            let s = Session.sharedInstance
                            s.idOfCompaniesArr = idArr
                            s.nameOfCompaniesArr = nameArr
                            s.iconsOfCompaniesArr = iconsArr
                            s.descOfCompaniesArr = descArr
                            self.dropDown.dataSource = nameArr
                        } else {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                        }
                    }
                    ProfileReq().getMyProfileInfo(desiredProfileId: String(), completionHandler: { (done, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr, photoStr) in
                        SwiftLoader.hide()
                        if done {
                            let s = Session.sharedInstance
                            s.profileFirstName = name
                            s.profileSurName = surname
                            s.profileEmail = email
                            s.profilePhone = phone
                            s.profileCompany = company
                            s.profileFeedFromMe = feedFromMe
                            s.profilePhoto = photo
                            s.profilePoints = points
                            s.profileInterest = interestsArr.joined(separator: "")
                        } else {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                        }
                    })
                }
            })
            
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    private func setupDropDown(){
        self.dropDown.anchorView = self.dropDownBtn
        self.dropDown.direction = .bottom
        self.dropDown.bottomOffset = CGPoint(x: 0, y:self.dropDownBtn.bounds.height)
        self.dropDown.textFont = UIFont.systemFont(ofSize: 11)
        self.dropDown.backgroundColor = .white
    }
    
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var complexLabel: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var welcomeTitleLabel: UILabel!
    @IBOutlet weak var pinContainer: UIView!
    @IBOutlet weak var pinField: UITextField!
}

extension IntroSelectCompanyNameVC {

    @IBAction func dropDownBtn(_ sender: Any) {
        if dropDown.isHidden {
            dropDown.show()
        }
        self.dropDown.selectionAction = { [unowned self] (index, item) in
            self.complexLabel.text = item
            if let temp = Int(Session.sharedInstance.idOfCompaniesArr[index]) {
                Session.sharedInstance.companyTutorial = String(temp)
            }
        }
    }
    
    @IBAction func pushNextVcBtn(_ sender: Any) {
        if Session.sharedInstance.facebookAcc {
            if pinField.text!.isEmpty {
                showSimpleAlert(title: "Error".localized(), message: "You have to enter valid Entry code".localized())
            } else if Session.sharedInstance.companyTutorial.isEmpty {
                showSimpleAlert(title: "Error".localized(), message: "You have to pick your company".localized())
            } else {
                SwiftLoader.show(animated: true)
                LoginReq().registerUserBuildingCode(buildingSecretCode: pinField.text!, completionHandler: { (done, buildingCodeId) in
                    SwiftLoader.hide()
                    if done {
                        LoginReq().notifyServerThatUserSeenWelcomeScreen(completionHandler: { (done) in })
                        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "IntroContainerVC") as! IntroContainerVC
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.navigationController?.pushViewController(openNewVC, animated: true)
                        });
                    } else {
                        self.showSimpleAlert(title: "Error".localized(), message: "Entry code is not valid, check your email".localized())
                    }
                })
            }
        } else {
            if !Session.sharedInstance.companyTutorial.isEmpty {
                let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "IntroContainerVC") as! IntroContainerVC
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController?.pushViewController(openNewVC, animated: true)
                });
            } else {
                showSimpleAlert(title: "Error".localized(), message: "You have to pick your company".localized())
            }
        }
    }
}

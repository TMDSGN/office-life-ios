//
//  ReservationCategoryVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 05.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

class ReservationCategoryVC: UITableViewCell {

    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    
}

extension ReservationCategoryVC {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}

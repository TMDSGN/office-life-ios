//
//  ReservationDetailSelectionVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 04.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader

class ReservationDetailSelectionVC: UIViewController, AlertMessage {

    var reservationId: String = ""
    var nameOfProduct: String = ""

    fileprivate var dateStr: String = ""
    fileprivate var timeFromStr: String = ""
    fileprivate var timeToStr: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        registerObserver()
        setupLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.1, animations: {
            self.heightConstant.constant = self.container.frame.size.width
            self.view.layoutIfNeeded()
        })
    }

    fileprivate func setupLabels(){
        title = "Booking".localized()
        titleLabel.text = nameOfProduct
        reserveBtn.setTitle("Reserve".localized(), for: .normal)
        dateLabel.text = "Date".localized()
        timeFromLabel.text = "Time from".localized()
        timeToLabel.text = "Time to".localized()
    }
    
    func updateDay(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let temp = dict["dayStr"] as! String
        self.dateLabel.text = "Date ".localized() + temp
        dateStr = temp
    }
    
    func updateTimeFrom(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let temp = dict["timeStr"] as! String
        self.timeFromLabel.text = "Time from ".localized() + temp
        timeFromStr = temp
    }
    
    func updateTimeTo(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let temp = dict["timeStr"] as! String
        self.timeToLabel.text = "Time to ".localized() + temp
        timeToStr = temp
    }
    
    fileprivate func presentModal(_ modal:UIViewController){
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: false, completion: nil)
    }
    
    fileprivate func registerObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(ReservationDetailSelectionVC.updateDay), name: NSNotification.Name(rawValue: "updateDay"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ReservationDetailSelectionVC.updateTimeFrom), name: NSNotification.Name(rawValue: "updateTimeFrom"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ReservationDetailSelectionVC.updateTimeTo), name: NSNotification.Name(rawValue: "updateTimeTo"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updateDay"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updateTimeFrom"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updateTimeTo"), object: nil)
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeFromLabel: UILabel!
    @IBOutlet weak var timeToLabel: UILabel!
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var reserveBtn: UIButton!
}

extension ReservationDetailSelectionVC {

    // MARK: - IBActions
    @IBAction func pickDayBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "DateSelectionVC") as! DateSelectionVC
        presentModal(modal)
    }
    
    @IBAction func timeFromBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "TimeSelectionVC") as! TimeSelectionVC
        modal.typeOfView = 1
        presentModal(modal)
    }
    
    @IBAction func timeToBtn(_ sender: Any) {
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "TimeSelectionVC") as! TimeSelectionVC
        modal.typeOfView = 2
        presentModal(modal)
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    @IBAction func reservationBtn(_ sender: Any) {
        if !dateStr.isEmpty {
            if !timeFromStr.isEmpty {
                if !timeToStr.isEmpty {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
                    dateFormatter.timeZone = TimeZone.current
                    let tempDateFrom = dateFormatter.date(from: "\(dateStr) \(timeFromStr)")!.timeIntervalSince1970
                    let tempDateTo = dateFormatter.date(from: "\(dateStr) \(timeToStr)")!.timeIntervalSince1970
                    if tempDateFrom < tempDateTo {
                        if Reachability.isConnectedToNetwork() {
                            SwiftLoader.show(animated: true)
                            ReservationReq().createReservation(reservationId: reservationId, from: String(tempDateFrom), to:String(tempDateTo), completionHandler: { (done) in
                                SwiftLoader.hide()
                                if done {
                                    CalendarWriterVC().addEventToCalendar(title: self.nameOfProduct, description: String(), startDate: dateFormatter.date(from: "\(self.dateStr) \(self.timeFromStr)")! as NSDate, endDate: dateFormatter.date(from: "\(self.dateStr) \(self.timeToStr)")! as NSDate)
                                    
                                    DelayFunc.sharedInstance.delay(0.5, closure: {
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "executeReservationSuccess"), object: ["dayStr" : "\(self.dateStr) \(self.timeFromStr)-\(self.timeToStr)"])
                                    })
                                    if let navigationController = self.navigationController {
                                        navigationController.popViewController(animated: true)
                                    }
                                } else {
                                    self.showSimpleAlert(title: "Error".localized(), message: "Server sent error.".localized())
                                }
                            })
                        } else {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
                        }
                    } else {
                        showSimpleAlert(title: "Error", message: "Check your reservation time. It is not possible.")
                    }
                } else {
                    showSimpleAlert(title: "Error", message: "Select the start time of the reservation.".localized())
                }
            } else {
                showSimpleAlert(title: "Error", message: "Select the end time of the reservation.".localized())
            }
        } else {
            showSimpleAlert(title: "Error", message: "Select the date of the reservation.".localized())
        }
    }
}

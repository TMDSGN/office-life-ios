//
//  ComentsToWallPostVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 14.02.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import Haneke
import SwiftLoader

class ComentsToWallPostVC: UIViewController {
    
    var comentNameArr:[String] = []
    var comentPhotoArr:[String] = []
    var comentCreatedArr:[String] = []
    var comentUserIdArr:[String] = []
    
    var postId:String = ""
    var activityId:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegister()
        title = "Coments".localized()
        self.txtField.delegate = self
        self.txtField.placeholder = "Write message".localized()
        tableView.delegate = self
        sendBtn.setTitle("Close".localized(), for: .normal)
    }
    
    fileprivate func postWallComent(){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            HomeWallReq().createNewComent(text: txtField.text!, postId: postId) { (done) in
                self.comentNameArr.insert(self.txtField.text!, at: 0)
                self.comentPhotoArr.insert(Session.sharedInstance.profilePhoto, at: 0)
                self.comentCreatedArr.insert("now", at: 0)
                self.comentUserIdArr.insert(Session.sharedInstance.userToken, at: 0)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTheWall"), object: nil)
                self.tableView.reloadData()
                SwiftLoader.hide()
            }
        }
    }
    
    fileprivate func postActivityComent(){
        if Reachability.isConnectedToNetwork() {
            ActivitiesReq().createNewActivityWallComent(activityId: activityId, postId: postId, text: txtField.text!) { (done) in
                if done {
                    self.comentNameArr.insert(self.txtField.text!, at: 0)
                    self.comentPhotoArr.insert(Session.sharedInstance.profilePhoto, at: 0)
                    self.comentCreatedArr.insert("now", at: 0)
                    self.comentUserIdArr.insert(Session.sharedInstance.userToken, at: 0)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTheWall"), object: nil)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "MsgsWallComentCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 200
    }
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtField: UITextField!
    
}

extension ComentsToWallPostVC {
    
    @IBAction func sendBtn(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

extension ComentsToWallPostVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comentUserIdArr.count

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! MsgsWallComentCellVC
        cell.createdLabel.text = "posted a new comment ".localized() + comentCreatedArr[indexPath.row]
        cell.descLabel.text = comentNameArr[indexPath.row]
        if !comentPhotoArr[indexPath.row].isEmpty {
            let tempUrl = URL(string: comentPhotoArr[indexPath.row])
            cell.imgIcon.hnk_setImage(from: tempUrl)
        } else {
            cell.imgIcon.image = UIImage(named: "placeholder_photo")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileReq().getMyProfileInfo(desiredProfileId: comentUserIdArr[indexPath.row], completionHandler: { (done, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr, photoStr) in
                SwiftLoader.hide()
                if done {
                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "PeopleDetailVC") as! PeopleDetailVC
                    openNewVC.userFullName = name + " " + surname
                    openNewVC.interestStr = interestsArr.joined(separator: "")
                    openNewVC.contactStr = email
                    openNewVC.userId = self.comentUserIdArr[indexPath.row]
                    openNewVC.photoUser = photo
                    openNewVC.idCompany = photoStr
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(openNewVC, animated: true)
                    });
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension ComentsToWallPostVC: UITextFieldDelegate {
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if activityId.isEmpty {
            postWallComent()
        } else {
            postActivityComent()
        }
        return true
    }
}

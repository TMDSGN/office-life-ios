//
//  ServiceBigSelectionCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 04.03.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

class ServiceBigSelectionCellVC: UITableViewCell {
    
    var prizePerProduct: String = ""
    fileprivate var totalPrice: String = ""
    fileprivate var countNumber: Int = 0

    var productTitle: String = ""
    var productPricePerUnit: String = ""

    override func awakeFromNib() {
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    @IBAction func minusBtn(_ sender: Any) {
        count("-")
    }
    
    @IBAction func plusBtn(_ sender: Any) {
        count("+")
    }
    
    fileprivate func count(_ char:String){
        var temp:Double = 0.0
        if countNumber >= 0 {
            switch char {
            case "+":
                countNumber += 1
                temp = 1
            case "-":
                if countNumber > 0 {
                    countNumber -= 1
                    temp = -1
                }
            default:
                break
            }
            countTitleLabel.text = countNumber.description
            let tempPrizePerProduct = prizePerProduct.components(separatedBy: " ")
            if let tempFirstItem = tempPrizePerProduct.first {
                if let tempDoubleFromString = Double(tempFirstItem.replacingOccurrences(of: ",", with: ".")) {
                    let prizePerProduct: Double = temp * tempDoubleFromString
                    var adding = false
                    if prizePerProduct > 0 {
                        adding = true
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "countFinalPriceMultiplier"), object: ["adding": adding, "title": productTitle, "prizePerProduct" : prizePerProduct, "quantity" : countNumber])
                }
            }
        }
    }

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var finalPriceLabel: UILabel!
    @IBOutlet weak var finalPriceCountLabel: UILabel!
    
    @IBOutlet weak var countContainer: UIView!
    @IBOutlet weak var countTitleLabel: UILabel!
    
}

//
//  CalendarCellViewVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 04.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import JTAppleCalendar

class CalendarCellVC: JTAppleDayCellView {
    
    var id: String = ""
    
    override func awakeFromNib() {
        selectedView.backgroundColor = UIColor.mainBlue
        bgView.backgroundColor = UIColor.cellBg
        blueDot.layer.cornerRadius = blueDot.frame.size.height/2
        blueDot.backgroundColor = UIColor.mainBlue
        blueDot.isHidden = true
    }
    
    @IBOutlet var selectedView: UIView!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var blueDot: UIView!
}

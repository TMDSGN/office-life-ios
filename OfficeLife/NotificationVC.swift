//
//  NotificationVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 27.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader
import Haneke

class NotificationVC: HamburgerContainerVC {
    
    fileprivate var idArr:[String] = []
    fileprivate var titleArr:[String] = []
    fileprivate var subtitleArr:[String] = []
    fileprivate var idUserArr:[String] = []
    fileprivate var imageUserArr:[String] = []
    fileprivate var refreshControl: UIRefreshControl!
    fileprivate let s = Session.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        registerObservers()
        customCellRegister()
        setupPullToRefresh()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIconOval")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.s.newListNotification = false
    }
    
    fileprivate func setupPullToRefresh(){
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    @objc fileprivate func getData(){
        self.s.getNotification()
        self.idArr = self.s.idNotificationArr
        self.titleArr = self.s.titleNotificationArr
        self.subtitleArr = self.s.bodyNotificationArr
        self.imageUserArr = self.s.userPhotoNotificationArr
        self.idUserArr = self.s.userIdNotificationArr
        self.tableView.reloadData()
    }
    
    @objc fileprivate func openPeopleProfile(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let tempUserId = dict["userId"] as! String
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ProfileReq().getMyProfileInfo(desiredProfileId: tempUserId, completionHandler: { (done, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr, photoStr) in
                SwiftLoader.hide()
                if done {
                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "PeopleDetailVC") as! PeopleDetailVC
                    openNewVC.userFullName = name + " " + surname
                    openNewVC.interestStr = interestsArr.joined(separator: "")
                    openNewVC.contactStr = email
                    openNewVC.photoUser = photo
                    openNewVC.idCompany = photoStr
                    openNewVC.userId = tempUserId
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(openNewVC, animated: true)
                    });
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            })
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    func refresh(sender:AnyObject) {
        getData()
        self.refreshControl!.endRefreshing()
    }
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "NotificationCellView", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "NotificationCellVC")
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.getData), name: NSNotification.Name(rawValue: "reloadNotificationVC"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openPeopleProfile), name: NSNotification.Name(rawValue: "openPeopleProfile"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadNotificationVC"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openPeopleProfile"), object: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
}

extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titleArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "NotificationCellVC") as! NotificationCellVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.idUser = idUserArr[indexPath.row]
        cell.titleLabel.text = titleArr[indexPath.row].localized()
        cell.subTitleLabel.text = subtitleArr[indexPath.row]
        if imageUserArr[indexPath.row].isEmpty {
            cell.imgIcon.image = UIImage(named: "placeholder_photo")
        } else {
            let url: URL = URL(string: imageUserArr[indexPath.row])!
            cell.imgIcon.hnk_setImage(from: url, placeholder: UIImage(named: "placeholder_photo"))
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if idArr[indexPath.row] != "1" && !idArr[indexPath.row].isEmpty && titleArr[indexPath.row] != "New annoucement" {
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ActivityDetailVC") as! ActivityDetailVC
            openNewVC.activityId = idArr[indexPath.row].description
            openNewVC.titleCategory = "Activities".localized()
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
        } else if idArr[indexPath.row] != "1" && idArr[indexPath.row].isEmpty && titleArr[indexPath.row] != "New annoucement" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
            vc.conversationReceiverId = idUserArr[indexPath.row]
            vc.messagesWall = true
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(vc, animated: true)
            });
        } else {
            openMsgsTableView()
        }
    }
}

//
//  ReservationTableVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 05.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader
import Haneke

class ReservationTableVC: HamburgerContainerVC {
    
    var storedOffsets = [Int: CGFloat]()
    var categoryArr = ["All".localized(), "Meeting room".localized(), "Car sharing"]
    var categoryIdArr = ["1", "26", "25"]
    
    fileprivate var selectedCategory = "1"
    fileprivate var idArr = [String]()
    fileprivate var nameArr = [String]()
    fileprivate var photoArr = [String]()
    fileprivate var podlaziArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegister()
        title = "Booking".localized()
        getData("1")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupDefaultMenuButton()
        setupNavBar("chatIcon", "notificationIcon")
    }
    
    fileprivate func getData(_ selectedCat:String){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ReservationReq().getReservationTypeList(type: selectedCat) { (done, idArr, nameArr, photoArr, podlaziArr, fromDateArr, fromTimeArr, fromTimestampForDeleteArr) in
                SwiftLoader.hide()
                if done {
                    self.idArr = idArr
                    self.nameArr = nameArr
                    self.photoArr = photoArr
                    self.podlaziArr = podlaziArr
                    self.reservationTable.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reservationTable: UITableView!
    
}

extension ReservationTableVC: UITableViewDataSource, UITableViewDelegate {
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "ReservationTableCellView", bundle: nil)
        reservationTable.register(nib, forCellReuseIdentifier: "ReservationTableCell")
        reservationTable.estimatedRowHeight = 200
        reservationTable.separatorStyle = .none
        tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == reservationTable {
            return self.idArr.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == reservationTable {
            return 260
        } else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        if tableView == reservationTable {
            let cell = self.reservationTable.dequeueReusableCell(withIdentifier: "ReservationTableCell") as! ReservationTableCellVC
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            let tempurl = URL(string:photoArr[indexPath.row])
            cell.imageIcon.hnk_setImage(from: tempurl)
            cell.titleLabel.text = nameArr[indexPath.row]
            cell.descLabel.text = podlaziArr[indexPath.row]
            
            if !photoArr[indexPath.row].isEmpty {
                let tempurl = URL(string: photoArr[indexPath.row])
                cell.imageIcon.hnk_setImage(from: tempurl)
            } else {
                cell.imageIcon.backgroundColor = UIColor.backgroundPhotoColor
                cell.imageIcon.image = UIImage(named: "placeholder")
            }
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ReservationCategoryCell") as! ReservationCategoryVC
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == reservationTable {
            let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ReservationDetailVC") as! ReservationDetailVC
            openNewVC.idOfObject = idArr[indexPath.row]
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.pushViewController(openNewVC, animated: true)
            });
            self.reservationTable.deselectRow(at: indexPath, animated: false)
        } else {
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView != reservationTable {
            guard let tableViewCell = cell as? ReservationCategoryVC else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView != reservationTable {
            guard let tableViewCell = cell as? ReservationCategoryVC else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
    }
    
}

extension ReservationTableVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 50
        var width:CGFloat = 50
        
        switch categoryArr[indexPath.item] {
        case "Meeting room".localized():
            width = 130
        default:
            width = (self.view.frame.size.width-30)/3
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! ReservationCategoryCollectionCell
        cell.titleLabel.text = categoryArr[indexPath.item]
        if selectedCategory == categoryIdArr[indexPath.row] {
            UIView.animate(withDuration: 0.1) {
                cell.underLineView.isHidden = false
            }
        } else {
            UIView.animate(withDuration: 0.1) {
                cell.underLineView.isHidden = true
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategory = categoryIdArr[indexPath.row]
        self.reservationTable.setContentOffset(CGPoint.zero, animated: false)
        DelayFunc.sharedInstance.delay(0.1) {
            collectionView.reloadData()
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.getData(self.selectedCategory)
        }
    }
}

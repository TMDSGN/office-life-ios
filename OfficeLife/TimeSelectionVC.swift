//
//  TimeSelectionVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 04.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

class TimeSelectionVC: UIViewController {

    var typeOfView: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.darkBg.alpha = 0
        self.timeContainer.alpha = 0
        self.timeFromLabel.text = "Time from".localized()
        self.hourLabel.text = "Hour".localized()
        self.minuteLabel.text = "Minute".localized()
        self.blueBtn.setTitle("Pick".localized(), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.darkBg.alpha = 1
            self.timeContainer.alpha = 1
        }
    }
        
    fileprivate func dismissView(){
        UIView.animate(withDuration: 0.5) {
            self.darkBg.alpha = 0
            self.timeContainer.alpha = 0
        }
        DelayFunc.sharedInstance.delay(0.5) {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    fileprivate func setupTime(){
        
        var hourTemp = String(Int(hourSlider.value))
        var minuteTemp = String(Int(minuteSlider.value))
        
        if hourSlider.value < 10 {
            hourTemp = "0\(hourTemp)"
        }
        
        switch Int(minuteSlider.value) {
        case 1:
            minuteTemp = "15"
        case 2:
            minuteTemp = "30"
        case 3:
            minuteTemp = "45"
        default:
            minuteTemp = "00"
        }
        
        timeLabel.text = "\(hourTemp):\(minuteTemp)"
    }
    
    fileprivate func setupView(){
        hourSlider.minimumValue = 0
        hourSlider.maximumValue = 23
        hourSlider.value = 11
        minuteSlider.minimumValue = 0
        minuteSlider.maximumValue = 4
        minuteSlider.value = 2
        
        let blue = UIColor.mainBlue
        let gray = UIColor.lightGray
        
        hourSlider.thumbTintColor = blue
        minuteSlider.thumbTintColor = blue
        
        hourSlider.minimumTrackTintColor = blue
        minuteSlider.minimumTrackTintColor = blue
        
        hourSlider.maximumTrackTintColor = gray
        minuteSlider.maximumTrackTintColor = gray
        
        blueBtn.backgroundColor = blue
    }
    
    @IBOutlet weak var blueBtn: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var hourSlider: UISlider!
    @IBOutlet weak var minuteSlider: UISlider!
    
    @IBOutlet weak var darkBg: UIView!
    @IBOutlet weak var timeContainer: UIView!
    @IBOutlet weak var timeFromLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
}

extension TimeSelectionVC {
 
    @IBAction func hourSliderBtn(_ sender: Any) {
        setupTime()
    }
    
    @IBAction func minuteSliderBtn(_ sender: Any) {
        setupTime()
    }
    
    @IBAction func createTime(_ sender: Any) {
        switch typeOfView {
        case 1:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTimeFrom"), object: ["timeStr" : timeLabel.text])
        case 2:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTimeTo"), object: ["timeStr" : timeLabel.text])
        default:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTime"), object: ["timeStr" : timeLabel.text])
        }
        dismissView()
    }
    
    @IBAction func outsideContainerBtn(_ sender: Any) {
        dismissView()
    }
}

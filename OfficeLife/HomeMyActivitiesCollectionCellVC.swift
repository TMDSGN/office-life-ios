//
//  HomeMyActivitiesCollectionCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 14.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class HomeMyActivitiesCollectionCellVC: UICollectionViewCell {
    
    override func awakeFromNib() {
        ecmptyPlaceholder.layer.borderWidth = 0.5
        ecmptyPlaceholder.layer.borderColor = UIColor.black.cgColor
    }
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var noActivityContainer: UIView!
    @IBOutlet weak var ecmptyPlaceholder: UIView!
    
}

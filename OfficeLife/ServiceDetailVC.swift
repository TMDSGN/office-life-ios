//
//  ServiceDetailVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 03.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader

fileprivate struct ActivityData {
    
    var priceOfProduct, addonsName, addonsTypeOfView: String
    var accordingGateway: Bool
    var addonsLabelArr, addonsPriceArr: [String]
    
    init(priceOfProduct: String, addonsName: String, addonsTypeOfView: String, addonsLabelArr: [String], addonsPriceArr: [String], accordingGateway: Bool) {
        self.priceOfProduct = priceOfProduct
        self.addonsName = addonsName
        self.addonsTypeOfView = addonsTypeOfView
        self.addonsLabelArr = addonsLabelArr
        self.addonsPriceArr = addonsPriceArr
        self.accordingGateway = accordingGateway
    }
}

class ServiceDetailVC: UIViewController {
    
    var serviceId: String = ""
    var titleCategory: String = ""
    fileprivate var activityData = ActivityData(priceOfProduct: "", addonsName: "", addonsTypeOfView: "", addonsLabelArr: [], addonsPriceArr: [], accordingGateway: false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData(serviceId: serviceId)
        registerObserver()
        setupLabels()
    }
    
    fileprivate func setupLabels(){
        blueBtn.setTitle("Order".localized(), for: .normal)
        descLabel.text = "Description".localized()
        sellecDesc.text = "Seller".localized()
        contactLabel.text = "Contact".localized()
        adressContactLabel.text = "Address".localized()
        title = titleCategory
    }
    
    fileprivate func getData(serviceId: String){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ServicesReq().getServicesDetail(serviceId: serviceId) { (done, photo, name, description, supplierStreet , supplierCity, supplierPhone, supplierEmail, price, addonsName, addonsTypeOfView, addonsLabelArr, addonsPriceArr, accordingGateway) in
                SwiftLoader.hide()
                if done {
                    self.titleOfObject.text = name
                    self.descTxtView.text = description
                    self.emailLabel.text = supplierEmail
                    self.adressLabel.text = supplierStreet
                    self.townLabel.text = supplierCity
                    self.phoneLabel.text = supplierPhone
                    if !photo.isEmpty {
                        let tempurl = URL(string: photo)
                        self.imgIcon.hnk_setImage(from: tempurl)
                    } else {
                        self.imgIcon.backgroundColor = UIColor.backgroundPhotoColor
                        self.imgIcon.image = UIImage(named: "placeholder")
                    }
                    
                    self.activityData = ActivityData(priceOfProduct: price, addonsName: addonsName, addonsTypeOfView: addonsTypeOfView, addonsLabelArr: addonsLabelArr, addonsPriceArr: addonsPriceArr, accordingGateway: accordingGateway)
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    fileprivate func presentModal(modal: UIViewController){
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
    
    @objc fileprivate func pushOrderToFinish(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        let tempNumberOfItems = dict["numberOfItems"] as! String
        let totalPriceOfProduct = dict["totalPriceOfProduct"] as! String
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ServiceOrderFinishVC") as! ServiceOrderFinishVC
        if let simpleView: Bool = dict["simpleView"] as? Bool {
            if simpleView {
                let productTitleForSentArray: [String] = dict["productTitleForSentArray"] as! [String]
                let productPriceForSentArray: [Double] = dict["productPriceForSentArray"] as! [Double]
                let productQuantityForSentArray: [Int] = dict["productQuantityForSentArray"] as! [Int]
                openNewVC.productTitleForSentArray = productTitleForSentArray
                openNewVC.productPriceForSentArray = productPriceForSentArray
                openNewVC.productQuantityForSentArray = productQuantityForSentArray
                openNewVC.serviceTypeBig = true
            }
        }
        
        openNewVC.serviceId = serviceId
        openNewVC.titleStr = title!
        openNewVC.nameOfProduct = titleOfObject.text!
        openNewVC.numberOfItems = tempNumberOfItems
        openNewVC.totalPrice = totalPriceOfProduct
        openNewVC.pricePerProduct = activityData.priceOfProduct
        openNewVC.accordingGateway = activityData.accordingGateway
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    fileprivate func registerObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(ServiceDetailVC.pushOrderToFinish), name: NSNotification.Name(rawValue: "executePushOrderToFinish"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "executePushOrderToFinish"), object: nil)
    }
    
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var sellerView: UIView!
    @IBOutlet weak var descLeading: NSLayoutConstraint!
    @IBOutlet weak var descTrailing: NSLayoutConstraint!
    @IBOutlet weak var sellerLeading: NSLayoutConstraint!
    @IBOutlet weak var sellerTrailing: NSLayoutConstraint!
    @IBOutlet weak var blueBtn: UIButton!
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var townLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var titleOfObject: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var sellecDesc: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var adressContactLabel: UILabel!
}

extension ServiceDetailVC {

    @IBAction func descBtn(_ sender: Any) {
        descLeading.isActive = true
        sellerLeading.isActive = false
        UIView.animate(withDuration: 0.1, animations: {
            self.sellerView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (true) in
            self.descTrailing.isActive = true
            self.sellerTrailing.isActive = false
            UIView.animate(withDuration: 0.1, animations: {
                self.descView.alpha = 1
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func sellerBtn(_ sender: Any) {
        descTrailing.isActive = false
        sellerTrailing.isActive = true
        UIView.animate(withDuration: 0.1, animations: {
            self.descView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (true) in
            self.descLeading.isActive = false
            self.sellerLeading.isActive = true
            UIView.animate(withDuration: 0.1, animations: {
                self.sellerView.alpha = 1
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func makeAreservationBtn(_ sender: Any) {
        let s = Session.sharedInstance
        s.serviceOrderEmail = ""
        s.serviceOrderNumber = ""
        s.serviceOrderDesc = ""
        //company reset
        s.serviceOrderIsCompanyOrder = Bool()
        s.serviceOrderCompanyName = ""
        s.serviceOrderCompanyBid = ""
        s.serviceOrderCompanyVat = ""
        s.serviceOrderCompanyZip = ""
        s.serviceOrderCompanyAddress = ""
        
        if activityData.addonsTypeOfView.isEmpty {
            let modal = self.storyboard?.instantiateViewController(withIdentifier: "GratzAlertVC") as! GratzAlertVC
            modal.type = 2
            modal.priceServiceOrder = activityData.priceOfProduct
            presentModal(modal: modal)
        } else if activityData.addonsTypeOfView == "input_multiplier" {
            let modal = self.storyboard?.instantiateViewController(withIdentifier: "ServiceBigSelectionTableVC") as! ServiceBigSelectionTableVC
            modal.addOnTitle = activityData.addonsLabelArr
            modal.addonPrice = activityData.addonsPriceArr
            modal.showCheckBoxView = false
            modal.addonsName = activityData.addonsName
            presentModal(modal: modal)
        } else if activityData.addonsTypeOfView == "checkbox" {
            let modal = self.storyboard?.instantiateViewController(withIdentifier: "ServiceBigSelectionTableVC") as! ServiceBigSelectionTableVC
            modal.addOnTitle = activityData.addonsLabelArr
            modal.addonPrice = activityData.addonsPriceArr
            modal.showCheckBoxView = true
            modal.addonsName = activityData.addonsName
            presentModal(modal: modal)
        }
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

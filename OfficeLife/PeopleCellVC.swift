//
//  PeopleCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 30.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class PeopleCellVC: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgContainer.layer.cornerRadius = imgContainer.frame.size.height/2
        imgContainer.clipsToBounds = true
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgCompany: UIImageView!
    @IBOutlet weak var imgContainer: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    
    //constant
    @IBOutlet weak var trailingC: NSLayoutConstraint!
    @IBOutlet weak var topC: NSLayoutConstraint!
    @IBOutlet weak var leadingC: NSLayoutConstraint!
    @IBOutlet weak var botC: NSLayoutConstraint!
}

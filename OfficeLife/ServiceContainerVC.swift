//
//  ServiceContainerVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 02.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import EMPageViewController

class ServiceContainerVC: UIViewController {
    
    var pageViewController: EMPageViewController?
    var currentPage: Int = 0
    var isFoodView = Bool()
    var itemsArr = [String]()
    var itemsIdArr = [String]()
    fileprivate var idArr = [String]()
    fileprivate var photoArr = [String]()
    fileprivate var nameArr = [String]()
    fileprivate var supplierArr = [String]()
    fileprivate var priceArr = [String]()
    var storedOffsets = [Int: CGFloat]()
    var selectedCategory: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegister()
        switch isFoodView {
        case true:
            title = "Services - Food and beverages".localized()
            itemsArr = ["All".localized(), "Express".localized(), "Steadily".localized()]
            itemsIdArr = ["13", "40", "46"]
        default:
            title = "Services - Other".localized()
            itemsArr = ["All".localized(), "Car Wash".localized(), "Dry Cleaning".localized()]
            itemsIdArr = ["15", "41", "42"]
        }
        selectedCategory = itemsIdArr[0]
        setupPageVC()
    }
    
    fileprivate func setupPageVC(){
        let pageViewController = EMPageViewController()
        
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        let currentViewController = self.viewController(at: 0)!
        pageViewController.selectViewController(currentViewController, direction: .forward, animated: false, completion: nil)
        
        self.addChildViewController(pageViewController)
        self.view.insertSubview(pageViewController.view, at: 0)
        pageViewController.didMove(toParentViewController: self)
        
        self.pageViewController = pageViewController
    }
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "ReservationTableCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ReservationTableCell")
        tableView.separatorStyle = .none
    }
    
    fileprivate func scrollAnimation(_ currentPage:Int){
        let selectedIndex = self.index(of: self.pageViewController!.selectedViewController as! ServiceTypeTableVC)!
        let direction:EMPageViewControllerNavigationDirection = currentPage > selectedIndex ? .forward : .reverse
        self.currentPage = currentPage
        let viewController = self.viewController(at: currentPage)!
        self.pageViewController!.selectViewController(viewController, direction: direction, animated: true, completion: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
}

extension ServiceContainerVC {

    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

extension ServiceContainerVC: EMPageViewControllerDataSource, EMPageViewControllerDelegate {

    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! ServiceTypeTableVC) {
            let beforeViewController = self.viewController(at: viewControllerIndex - 1)
            currentPage = viewControllerIndex
            //selectedCategory = currentPage
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadServiceCell"), object: nil)
            
            //scrollAnimation(currentPage: currentPage)
            return beforeViewController
        } else {
            return nil
        }
    }
    
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! ServiceTypeTableVC) {
            let afterViewController = self.viewController(at: viewControllerIndex + 1)
            currentPage = viewControllerIndex
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadServiceCell"), object: nil)
            
            return afterViewController
        } else {
            return nil
        }
    }
    
    fileprivate func viewController(at index: Int) -> ServiceTypeTableVC? {
        if (self.itemsIdArr.count == 0) || (index < 0) || (index >= self.itemsIdArr.count) {
            return nil
        }
        
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "ServiceTypeTableVC") as! ServiceTypeTableVC
        viewController.idStr = self.itemsIdArr[index]
        viewController.titleCategory = title!
        viewController.typesArr = itemsIdArr
        viewController.selectedCategory = selectedCategory
        return viewController
    }
    
    fileprivate func index(of viewController: ServiceTypeTableVC) -> Int? {
        if let greeting: String = viewController.idStr {
            return self.itemsIdArr.index(of: greeting)
        } else {
            return nil
        }
    }
}

extension ServiceContainerVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ReservationCategoryCell") as! ReservationCategoryVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? ServiceCategoryVC else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? ServiceCategoryVC else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
}

extension ServiceContainerVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 50
        var width:CGFloat = 50
        let newWidth = view.frame.size.width/3
        width = newWidth-5
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! ServiceCategoryCollectionCell
        
        cell.titleLabel.text = itemsArr[indexPath.item]
        if selectedCategory == itemsIdArr[indexPath.row] {
            UIView.animate(withDuration: 0.1) {
                cell.underLineView.isHidden = false
            }
        } else {
            UIView.animate(withDuration: 0.1) {
                cell.underLineView.isHidden = true
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategory = itemsIdArr[indexPath.row]
        currentPage = indexPath.row
        scrollAnimation(currentPage)
        collectionView.reloadData()
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}

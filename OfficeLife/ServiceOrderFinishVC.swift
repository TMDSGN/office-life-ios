//
//  ServiceOrderFinishVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 03.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import EMPageViewController
import SwiftLoader

class ServiceOrderFinishVC: UIViewController, AlertMessage {
    
    var pageViewController: EMPageViewController?
    var currentPage: Int = 0
    var viewsArr: [String] = ["1", "2"]
    var serviceTypeBig: Bool = false
    var titleStr: String = ""
    var serviceId: String = ""
    var nameOfProduct: String = ""
    var numberOfItems: String = ""
    var pricePerProduct: String = ""
    var totalPrice: String = ""
    var productTitleForSentArray: [String] = []
    var productPriceForSentArray: [Double] = []
    var productQuantityForSentArray: [Int] = []
    var accordingGateway = Bool()
    
    fileprivate var finishedOrderNowPushBack = false
    fileprivate var iPaidAndWantToGoBackBool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Session.sharedInstance.orderPaymentMethod = ""
        Session.sharedInstance.orderPaymentMethodServer = ""
        
        let pageViewController = EMPageViewController()
        
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        let currentViewController = self.viewController(at: 0)!
        pageViewController.selectViewController(currentViewController, direction: .forward, animated: false, completion: nil)
        
        self.addChildViewController(pageViewController)
        self.view.insertSubview(pageViewController.view, at: 0)
        pageViewController.didMove(toParentViewController: self)
        
        self.pageViewController = pageViewController
        self.title = titleStr
        blueBtn.backgroundColor = UIColor.mainBlue
        blueBtn.setTitle("Continue".localized(), for: .normal)
        updateBlueBtn()
        registerObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        pageBtnOne.backgroundColor = UIColor.mainBlue
        pageBtnTwo.backgroundColor = UIColor.lightGray
        pageBtnOne.layer.cornerRadius = 5
        pageBtnTwo.layer.cornerRadius = 5
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if iPaidAndWantToGoBackBool {
            newOrderSuccess()
        }
    }
    
    @objc fileprivate func iPaidAndWantToGoBack(){
        iPaidAndWantToGoBackBool = true
    }
    
    @objc fileprivate func orderFinishedExecute(){
        DelayFunc.sharedInstance.delay(0.5) {
            //o několik vc zpatky
            let vc = self.navigationController!.viewControllers.count-3
            _ = self.navigationController?.popToViewController(self.navigationController!.viewControllers[vc], animated: true)
        }
    }
    
    fileprivate func setupCurrentPage(_ index:Int){
        if currentPage != index {
            scrollTo(index: index)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "transformPageBtnService"), object: ["show" : index])
        }
        currentPage = index
        if currentPage == 1 {
            Session.sharedInstance.orderScreenCanOrder = true
        } else {
            Session.sharedInstance.orderScreenCanOrder = false
        }
    }
    
    @objc fileprivate func transformPageBtn(_ notification: NSNotification){
        let dict = notification.object as! NSDictionary
        switch dict["show"] as! Int {
        case 0:
            pageBtnOne.backgroundColor = UIColor.mainBlue
            pageBtnTwo.backgroundColor = UIColor.lightGray
            pageOneWidth.constant = 25
            pageTwoWidth.constant = 10
            UIView.animate(withDuration: 0.3){
                self.view.layoutIfNeeded()
            }
        case 1:
            pageBtnOne.backgroundColor = UIColor.lightGray
            pageBtnTwo.backgroundColor = UIColor.mainBlue
            pageOneWidth.constant = 10
            pageTwoWidth.constant = 25
            UIView.animate(withDuration: 0.3){
                self.view.layoutIfNeeded()
            }
        default:
            break
        }
    }
    
    fileprivate func makeOrder(openGoPay: Bool){
        SwiftLoader.show(animated: true)
        let s = Session.sharedInstance
        ServicesReq().orderService(serviceTypeBig: serviceTypeBig, service_id: serviceId, pieces: numberOfItems, phone: Session.sharedInstance.serviceOrderNumber, descNote: Session.sharedInstance.serviceOrderDesc, paymentMethod: Session.sharedInstance.orderPaymentMethodServer, productTitleForSentArray:productTitleForSentArray, productPriceForSentArray:productPriceForSentArray, productQuantityForSentArray:productQuantityForSentArray, returnUrlForPay: openGoPay, email: Session.sharedInstance.serviceOrderEmail, isCompanyOrder: s.serviceOrderIsCompanyOrder, companyName: s.serviceOrderCompanyName, companyBid: s.serviceOrderCompanyBid, companyVat: s.serviceOrderCompanyVat, companyZip: s.serviceOrderCompanyZip, companyAddress: s.serviceOrderCompanyAddress) { (done, url) in
            SwiftLoader.hide()
            if done {
                if url.isEmpty {
                    self.newOrderSuccess()
                } else {
                    let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewGoPayVC") as! WebViewGoPayVC
                    openNewVC.targetUrl = url
                    self.navigationController?.isNavigationBarHidden = false
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(openNewVC, animated: true)
                    });
                }
            } else if !done && !url.isEmpty {
                self.showSimpleAlert(title: "Error".localized(), message: "NOT_ENOUGHT_CREDITS".localized())
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
            }
        }
    }
    
    fileprivate func newOrderSuccess(){
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "GratzAlertVC") as! GratzAlertVC
        modal.type = 1
        modal.orderType = nameOfProduct
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: false, completion: nil)
    }
    
    fileprivate func updateBlueBtn(){
        if currentPage == 1 {
            blueBtn.setTitle("Order".localized(), for: .normal)
        } else {
            blueBtn.setTitle("Continue".localized(), for: .normal)
        }
    }
    
    fileprivate func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.iPaidAndWantToGoBack), name: NSNotification.Name(rawValue: "iPaidAndWantToGoBack"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ServiceOrderFinishVC.transformPageBtn), name: NSNotification.Name(rawValue: "transformPageBtnService"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ServiceOrderFinishVC.orderFinishedExecute), name: NSNotification.Name(rawValue: "orderFinished"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "iPaidAndWantToGoBack"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "transformPageBtnService"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "orderFinished"), object: nil)
    }
    
    @IBOutlet weak var pageBtnOne: UIView!
    @IBOutlet weak var pageBtnTwo: UIView!
    @IBOutlet weak var pageOneWidth: NSLayoutConstraint!
    @IBOutlet weak var pageTwoWidth: NSLayoutConstraint!
    @IBOutlet weak var blueBtn: UIButton!
    
}

extension ServiceOrderFinishVC {
    
    @IBAction func blueBtn(_ sender: Any) {
        let s = Session.sharedInstance
        updateBlueBtn()
        if currentPage == 0 {
            setupCurrentPage(1)
        } else {
            if !s.serviceOrderEmail.isEmpty {
                if !s.orderPaymentMethod.isEmpty {
                    if !s.serviceOrderNumber.isEmpty {
                        if s.serviceOrderIsCompanyOrder {
                            if !s.serviceOrderCompanyName.isEmpty {
                                if !s.serviceOrderCompanyBid.isEmpty {
                                    if !s.serviceOrderCompanyZip.isEmpty {
                                        if !s.serviceOrderCompanyAddress.isEmpty {
                                            if s.orderPaymentMethodServer == "gopayredirbinder" {
                                                makeOrder(openGoPay: true)
                                            } else {
                                                makeOrder(openGoPay: false)
                                            }
                                        } else {
                                            showSimpleAlert(title: "Error".localized(), message: "Enter company place address.".localized())
                                        }
                                    } else {
                                        showSimpleAlert(title: "Error".localized(), message: "Enter company zip code.".localized())
                                    }
                                } else {
                                    showSimpleAlert(title: "Error".localized(), message: "Enter company bid code.".localized())
                                }
                            } else {
                                showSimpleAlert(title: "Error".localized(), message: "Enter company full name.".localized())
                            }
                        } else {
                            if(s.orderPaymentMethodServer == "gopayredirbinder"){
                                makeOrder(openGoPay: true)
                            } else {
                                makeOrder(openGoPay: false)
                            }
                        }
                    } else {
                        showSimpleAlert(title: "Error".localized(), message: "Your phone number is needed.".localized())
                    }
                } else {
                    showSimpleAlert(title: "Error".localized(), message: "Add payment method.".localized())
                }
            } else {
                showSimpleAlert(title: "Error".localized(), message: "Your email address is needed.".localized())
            }
        }
    }
    
    @IBAction func pageOneBtnPressed(_ sender: AnyObject) {
        setupCurrentPage(0)
    }
    
    @IBAction func pageTwoBtnPressed(_ sender: AnyObject) {
        setupCurrentPage(1)
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

extension ServiceOrderFinishVC: EMPageViewControllerDataSource, EMPageViewControllerDelegate {
    
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! ServiceOrderFinishTableVC) {
            let beforeViewController = self.viewController(at: viewControllerIndex - 1)
            currentPage = viewControllerIndex
            if currentPage == 1 {
                Session.sharedInstance.orderScreenCanOrder = true
            } else {
                Session.sharedInstance.orderScreenCanOrder = false
            }
            updateBlueBtn()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "transformPageBtnService"), object: ["show" : viewControllerIndex])
            return beforeViewController
        } else {
            return nil
        }
    }
    
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! ServiceOrderFinishTableVC) {
            let afterViewController = self.viewController(at: viewControllerIndex + 1)
            currentPage = viewControllerIndex
            if currentPage == 1 {
                Session.sharedInstance.orderScreenCanOrder = true
            } else {
                Session.sharedInstance.orderScreenCanOrder = false
            }
            updateBlueBtn()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "transformPageBtnService"), object: ["show" : viewControllerIndex])
            return afterViewController
        } else {
            return nil
        }
    }
    
    func viewController(at index: Int) -> ServiceOrderFinishTableVC? {
        if (self.viewsArr.count == 0) || (index < 0) || (index >= self.viewsArr.count) {
            return nil
        }
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "ServiceOrderFinishTableVC") as! ServiceOrderFinishTableVC
        viewController.idStr = self.viewsArr[index]
        viewController.index = String(index)
        viewController.titleStr = titleStr
        viewController.serviceId = serviceId
        viewController.numberOfItems = numberOfItems
        viewController.pricePerProduct = pricePerProduct
        viewController.totalPrice = totalPrice
        viewController.nameOfProduct = nameOfProduct
        viewController.accordingGateway = accordingGateway
        return viewController
    }
    
    func index(of viewController: ServiceOrderFinishTableVC) -> Int? {
        if let greeting: String = viewController.idStr {
            return self.viewsArr.index(of: greeting)
        } else {
            return nil
        }
    }
    
    func scrollTo(index:Int) {
        let selectedIndex = self.index(of: self.pageViewController!.selectedViewController as! ServiceOrderFinishTableVC)!
        let viewController = self.viewController(at: index)!
        let direction:EMPageViewControllerNavigationDirection = index > selectedIndex ? .forward : .reverse
        self.pageViewController!.selectViewController(viewController, direction: direction, animated: true, completion: nil)
    }
}

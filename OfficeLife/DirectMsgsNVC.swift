//
//  DirectMsgsNVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 30.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit

class DirectMsgsNVC: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        redDot.layer.cornerRadius = 5
        imgIcon.layer.cornerRadius = 20
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    @IBOutlet weak var redDot: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
}

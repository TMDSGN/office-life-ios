//
//  ManagementCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 28.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit

class ManagementCellVC: UICollectionViewCell {
    
    var userId: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageContainer.layer.cornerRadius = 50
        self.imageContainer.clipsToBounds = true
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var phoneBtn: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imageContainer: UIView!
    //photo constants
    @IBOutlet weak var topC: NSLayoutConstraint!
    @IBOutlet weak var botC: NSLayoutConstraint!
    @IBOutlet weak var leadC: NSLayoutConstraint!
    @IBOutlet weak var trailC: NSLayoutConstraint!
}

extension ManagementCellVC {
    
    @IBAction func contactPersonBtn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "contactUser"), object: ["userId":userId])
    }
}

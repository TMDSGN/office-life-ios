//
//  MyActivitiesCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 22.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class MyActivityCellVC: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        rightCornerIcon.layer.cornerRadius = 10
        userProfileIcon.layer.cornerRadius = 20
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    @IBOutlet weak var rightCornerIcon: UIView!
    @IBOutlet weak var userProfileIcon: UIImageView!
    @IBOutlet weak var mainIcon: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userDateLabel: UILabel!
    @IBOutlet weak var userStreetLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var activityIcon: UIImageView!
}

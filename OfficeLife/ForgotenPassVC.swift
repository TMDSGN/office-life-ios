//
//  ForgotenPassVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 11.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader

class ForgotenPassVC: UIViewController, AlertMessage {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLabels()
    }
    
    fileprivate func setupLabels(){
        emailField.setValue(UIColor.init(colorLiteralRed: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0), forKeyPath: "_placeholderLabel.textColor")
        lostPassLabel.text = "Lost password".localized()
        sendPassBtn.setTitle("Send lost password".localized(), for: .normal)
    }
    
    @IBOutlet weak var lostPassLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var sendPassBtn: UIButton!
}

extension ForgotenPassVC {
    
    /*
     *   Request new password for your account
     */
    @IBAction func sendBtn(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            if !emailField.text!.isEmpty {
                SwiftLoader.show(animated: true)
                LoginReq().lostPass(email: emailField.text!) { (done) in
                    SwiftLoader.hide()
                    if done {
                        let alert = UIAlertController(title: "Done".localized(), message: "Login information have been successfully sent to your email.".localized(), preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                            if let navigationController = self.navigationController {
                                navigationController.popViewController(animated: true)
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErrNonDestructable"), object: nil)
                    }
                }
            } else {
                showSimpleAlert(title: "Error".localized(), message: "Fill email".localized())
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @IBAction func backBtn(_ sender: Any) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

//
//  ServiceCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 02.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class ServiceCellVC: UITableViewCell {
    
    override func awakeFromNib() {
        imgIcon.clipsToBounds = true
    }
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var supplierLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var tagContainer: UIView!
    @IBOutlet weak var tagLabel: UILabel!
    
}

//
//  Authentization.swift
//  OfficeLife
//
//  Created by Josef Antoni on 10.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Authentization {
    
    fileprivate var urlDomain: String = Session.sharedInstance.urlDomain
    
    func getToken(completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/jwt-auth/v1/token"
        
        let para = [
            "username": "mobile_client_ios",
            "password": "Ura^nexpYPdMOdiA4j#Y6R2z"
        ]
        
        Alamofire.request(url, method: .post, parameters:para, encoding: JSONEncoding.default).responseJSON { response in
            if response.response?.statusCode == 200 {
                let data = response.result.value
                let json = JSON(data!)
                let token = json["token"].stringValue

                if !token.isEmpty {
                    Session.sharedInstance.securityToken = token
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            } else {
                completionHandler(false)
            }
        }
    }
    
    func registerGcmToken(token: String, completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = urlDomain + "/services_app/v1/notifications"
        let header = [
            "User-Id": Session.sharedInstance.userToken,
            "Authorization" : "Bearer \(Session.sharedInstance.securityToken)"
        ]
        let para = [
            "ios_token": token
        ]

        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if response.response?.statusCode == 204 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
}

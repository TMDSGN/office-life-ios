//
//  IntroContainerPageVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 26.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class IntroContainerPageVC:UIViewController {
    
    var idStr:Int!
    fileprivate var interestArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLabels()
        setupLabelFrames()
        setupPhotoScene()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let temp = idStr {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "IntroContainerVCScroll"), object: ["numberId" : temp-1])
            if temp == 5 {
                UIView.animate(withDuration: 1, animations: { 
                    self.finishBasicTutBtn.alpha = 1
                    self.finishBasicTutBtn.layer.zPosition = 1
                })
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.finishBasicTutBtn.alpha = 0
    }
    
    /*
    *   Tell user what app can do and according to screen index update information providing.
    */
    fileprivate func setupLabels(){
        travelLabel.text = "Travel".localized()
        foodLabel.text = "Food".localized()
        musicLabel.text = "Music".localized()
        fashionLabel.text = "Fashion".localized()
        partyLabel.text = "Party".localized()
        sportLabel.text = "Sport".localized()
        technologyLabel.text = "Technology".localized()
        artLabel.text = "Art".localized()
        photoLabel.text = "Photo".localized()
        finishBasicTutBtn.setTitle("Enter OfficeLife".localized(), for: .normal)
        self.navigationController?.isNavigationBarHidden = true
        if let temp = idStr {
            self.backgroundNumberImg.image = UIImage(named:"\(temp)bg")
        }
        titleLabel.textColor = UIColor(red: 124/255.0, green: 176/255.0, blue: 227/255.0, alpha: 1)
        finishBasicTutBtn.alpha = 0
        self.finishBasicTutBtn.layer.zPosition = -1
        switch idStr {
        case 1:
            titleLabel.text = "Communication in a building has never been easier.".localized()
            subtitleLabel.text = ""
            threeViewFirstLabel.text = "building feed - place for all important information and announcements from the building administration".localized()
            threeViewSecondLabel.text = "single platform connecting the entire community".localized()
            threeViewThirdLabel.text = "notifications keep you updated all the time".localized()
            threeViewContainer.isHidden = false
            fourViewContainer.isHidden = true
            categoryContainer.isHidden = true
        case 2:
            titleLabel.text = "Access to all activities.".localized()
            subtitleLabel.text = ""
            threeViewFirstLabel.text = "every user can join organized activities and share own with the entire community or colleagues".localized()
            threeViewSecondLabel.text = "ready to set regular activities".localized()
            threeViewThirdLabel.text = "office yoga, BBQ party, carpool, jogging, charity event and many else – everything organized in one single app".localized()
            threeViewContainer.isHidden = false
            fourViewContainer.isHidden = true
            categoryContainer.isHidden = true
        case 3:
            titleLabel.text = "Services for daily life.".localized()
            subtitleLabel.text = ""
            fourViewFirstLabel.text = "express services and regular plans for your daily life".localized()
            fourViewSecondLabel.text = "simple order process".localized()
            fourViewThirdLabel.text = "various payment options and credit system".localized()
            fourViewFourthLabel.text = "local providers delivering everyday to the office".localized()
            threeViewContainer.isHidden = true
            fourViewContainer.isHidden = false
            categoryContainer.isHidden = true
        case 4:
            titleLabel.text = "Get the advantage of sharing.".localized()
            subtitleLabel.text = ""
            threeViewFirstLabel.text = "easy booking process".localized()
            threeViewSecondLabel.text = "tracking all bookings".localized()
            threeViewThirdLabel.text = "notifications of upcoming bookings".localized()
            threeViewContainer.isHidden = false
            fourViewContainer.isHidden = true
            categoryContainer.isHidden = true
        case 5:
            titleLabel.text = "Interests".localized()
            subtitleLabel.text = "Before you start taking full advantage of the app, let's start with filling out your interests. Their specification helps to target and organize activities for you.".localized()
            
            threeViewContainer.isHidden = true
            fourViewContainer.isHidden = true
            categoryContainer.isHidden = false
        default:
            break
        }
    }
    
    fileprivate func setupLabelFrames(){
        threeViewFirstTitle.layer.cornerRadius = threeViewFirstTitle.frame.size.height/2
        threeViewFirstTitle.layer.borderWidth = 1
        threeViewFirstTitle.layer.borderColor = UIColor.white.cgColor
        
        threeViewSecondTitle.layer.cornerRadius = threeViewSecondTitle.frame.size.height/2
        threeViewSecondTitle.layer.borderWidth = 1
        threeViewSecondTitle.layer.borderColor = UIColor.white.cgColor
        
        threeViewThirdTitle.layer.cornerRadius = threeViewThirdTitle.frame.size.height/2
        threeViewThirdTitle.layer.borderWidth = 1
        threeViewThirdTitle.layer.borderColor = UIColor.white.cgColor
        
        fourViewFirstTitle.layer.cornerRadius = threeViewThirdTitle.frame.size.height/2
        fourViewFirstTitle.layer.borderWidth = 1
        fourViewFirstTitle.layer.borderColor = UIColor.white.cgColor
        
        fourViewSecondTitle.layer.cornerRadius = threeViewThirdTitle.frame.size.height/2
        fourViewSecondTitle.layer.borderWidth = 1
        fourViewSecondTitle.layer.borderColor = UIColor.white.cgColor
        
        fourViewThirdTitle.layer.cornerRadius = threeViewThirdTitle.frame.size.height/2
        fourViewThirdTitle.layer.borderWidth = 1
        fourViewThirdTitle.layer.borderColor = UIColor.white.cgColor
        
        fourViewFourthTitle.layer.cornerRadius = threeViewThirdTitle.frame.size.height/2
        fourViewFourthTitle.layer.borderWidth = 1
        fourViewFourthTitle.layer.borderColor = UIColor.white.cgColor
    }
    
    fileprivate func setupPhotoScene(){
        travelCatImg.alpha = 0.2
        foodCatImg.alpha = 0.2
        musicCatImg.alpha = 0.2
        modeCatImg.alpha = 0.2
        partyCatImg.alpha = 0.2
        sportCatImg.alpha = 0.2
        techCatImg.alpha = 0.2
        artCatImg.alpha = 0.2
        photoCatImg.alpha = 0.2
    }
    
    fileprivate func changeImgAlpha(_ image:UIImageView,_ imageName:String){
        if image.alpha == 1 {
            interestArr.remove(object: imageName)
            UIView.animate(withDuration: 0.5, animations: {
                image.alpha = 0.2
            })
        } else {
            interestArr.append(imageName)
            UIView.animate(withDuration: 0.5, animations: {
                image.alpha = 1
            })
        }
    }
    
    @IBOutlet weak var travelCatImg: UIImageView!
    @IBOutlet weak var foodCatImg: UIImageView!
    @IBOutlet weak var musicCatImg: UIImageView!
    @IBOutlet weak var modeCatImg: UIImageView!
    @IBOutlet weak var partyCatImg: UIImageView!
    @IBOutlet weak var sportCatImg: UIImageView!
    @IBOutlet weak var techCatImg: UIImageView!
    @IBOutlet weak var artCatImg: UIImageView!
    @IBOutlet weak var photoCatImg: UIImageView!
    
    @IBOutlet weak var travelLabel: UILabel!
    @IBOutlet weak var foodLabel: UILabel!
    @IBOutlet weak var musicLabel: UILabel!
    @IBOutlet weak var fashionLabel: UILabel!
    @IBOutlet weak var partyLabel: UILabel!
    @IBOutlet weak var sportLabel: UILabel!
    @IBOutlet weak var technologyLabel: UILabel!
    @IBOutlet weak var artLabel: UILabel!
    @IBOutlet weak var photoLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backgroundNumberImg: UIImageView!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var threeViewContainer: UIView!
    @IBOutlet weak var fourViewContainer: UIView!
    @IBOutlet weak var categoryContainer: UIView!
    
    @IBOutlet weak var threeViewFirstLabel: UILabel!
    @IBOutlet weak var threeViewSecondLabel: UILabel!
    @IBOutlet weak var threeViewThirdLabel: UILabel!
    
    @IBOutlet weak var threeViewFirstTitle: UILabel!
    @IBOutlet weak var threeViewSecondTitle: UILabel!
    @IBOutlet weak var threeViewThirdTitle: UILabel!
    
    @IBOutlet weak var fourViewFirstLabel: UILabel!
    @IBOutlet weak var fourViewSecondLabel: UILabel!
    @IBOutlet weak var fourViewThirdLabel: UILabel!
    @IBOutlet weak var fourViewFourthLabel: UILabel!
    
    @IBOutlet weak var fourViewFirstTitle: UILabel!
    @IBOutlet weak var fourViewSecondTitle: UILabel!
    @IBOutlet weak var fourViewThirdTitle: UILabel!
    @IBOutlet weak var fourViewFourthTitle: UILabel!
    @IBOutlet weak var finishBasicTutBtn: UIButton!
    
}

extension IntroContainerPageVC {

    @IBAction func travelBtn(_ sender: Any) {
        changeImgAlpha(travelCatImg, "Travel".localized())
    }
    
    @IBAction func foodBtn(_ sender: Any) {
        changeImgAlpha(foodCatImg, "Food".localized())
    }
    
    @IBAction func musicBtn(_ sender: Any) {
        changeImgAlpha(musicCatImg, "Music".localized())
    }
    
    @IBAction func fashionBtn(_ sender: Any) {
        changeImgAlpha(modeCatImg, "Fashion".localized())
    }
    
    @IBAction func partyBtn(_ sender: Any) {
        changeImgAlpha(partyCatImg, "Party".localized())
    }
    
    @IBAction func sportBtn(_ sender: Any) {
        changeImgAlpha(sportCatImg, "Sport".localized())
    }
    
    @IBAction func technologyBtn(_ sender: Any) {
        changeImgAlpha(techCatImg, "Technology".localized())
    }
    
    @IBAction func artBtn(_ sender: Any) {
        changeImgAlpha(artCatImg, "Art".localized())
    }
    
    @IBAction func photoBtn(_ sender: Any) {
        changeImgAlpha(photoCatImg, "Photo".localized())
    }

    /*
    *   Uživatel chce skončit intro. Ulož jeho záliby a ukaž HOME screen.
    */
    @IBAction func finishBasicTutBtn(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            let tempStr = interestArr.joined(separator: ",").replacingOccurrences(of: " ,", with: ",").replacingOccurrences(of: ", ", with: ",").replacingOccurrences(of: " , ", with: ",").replacingOccurrences(of: " ", with: ",")
            let tempArr:[String] = tempStr.components(separatedBy: ",")
            let s = Session.sharedInstance
            ProfileReq().updateMyProfileInfo(name: s.profileFirstName, surname: s.profileSurName, email: s.profileEmail, phone: s.profilePhone, company: Session.sharedInstance.companyTutorial, feedFromMe: true, interestsArr: tempArr) { (done, name, surname, email, phone, company, feedFromMe, photo, points, interestsArr) in
                if done {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.performSegue(withIdentifier: "finishedTutorial", sender: nil)
                    });
                }
            }
        } else {
            SwiftLoader.hide()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }

}

extension Array where Element: Equatable {
    
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}

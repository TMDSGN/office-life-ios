//
//  ServiceTypeTableVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 02.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit
import SwiftLoader

class ServiceTypeTableVC: HamburgerContainerVC {
    
    var selectedCategory: String = ""

    var idStr:String!
    var titleCategory: String = ""
    var typesArr = [String]()
    var idArr = [String]()
    var photoArr = [String]()
    var nameArr = [String]()
    var supplierArr = [String]()
    var priceArr = [String]()
    var tagArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaultMenuButton()
        customCellRegister()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar("chatIcon", "notificationIcon")
        title = "Tenants".localized()
        getData(selectedCategory)
    }
    
    fileprivate func getData(_ serviceId:String){
        if Reachability.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            ServicesReq().getServicesList(serviceId: serviceId) { (done, idArr, photoArr, nameArr, supplierArr , priceArr, tagArr) in
                SwiftLoader.hide()
                if done {
                    self.idArr = idArr
                    self.photoArr = photoArr
                    self.nameArr = nameArr
                    self.supplierArr = supplierArr
                    self.priceArr = priceArr
                    self.tagArr = tagArr
                    self.tableView.reloadData()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverErr"), object: nil)
                }
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectionErr"), object: nil)
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
}

extension ServiceTypeTableVC: UITableViewDataSource, UITableViewDelegate {
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "ServicesCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ServicesCell")
        tableView.estimatedRowHeight = 200
        tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ServicesCell") as! ServiceCellVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.supplierLabel.text = supplierArr[indexPath.row].htmlDecoded
        cell.productNameLabel.text = nameArr[indexPath.row].htmlDecoded
        cell.priceLabel.text = priceArr[indexPath.row].htmlDecoded
        if !self.photoArr[indexPath.row].isEmpty {
            let tempurl = URL(string: self.photoArr[indexPath.row])
            cell.imgIcon.hnk_setImage(from: tempurl)
        } else {
            cell.imgIcon.backgroundColor = UIColor.backgroundPhotoColor
            cell.imgIcon.image = UIImage(named: "placeholder")
        }
        if tagArr[indexPath.row].isEmpty {
            cell.tagContainer.isHidden = true
        } else {
            cell.tagContainer.isHidden = false
            cell.tagLabel.text = tagArr[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "ServiceDetailVC") as! ServiceDetailVC
        openNewVC.serviceId = idArr[indexPath.row]
        openNewVC.titleCategory = titleCategory
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return idArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

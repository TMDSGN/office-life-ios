import Foundation

class Session {
    
    static let sharedInstance = Session()
    fileprivate init(){//This prevents others from using the default '()' initializer for this class.
        if debugMode {
            self.urlDomain = "http://lowcost-env.xh7gcsxvk9.eu-central-1.elasticbeanstalk.com/wp-json"
        } else {
            self.urlDomain = "http://officelife.re/wp-json"
        }
    }
    
    //is app in debugging mode for server
    let debugMode = false
    var urlDomain: String
    
    var securityToken: String = ""
    var userToken: String = ""
    
    //facebook
    var facebookAcc: Bool = false
    
    var isAfterRegistrationSelectedCompany: Bool = false
    var isAfterRegistration: Bool = false
    
    //profile info
    var profilePhoto: String = ""
    var profileFirstName: String = ""
    var profileSurName: String = ""
    var profileEmail: String = ""
    var profilePhone: String = ""
    var profileCompany: String = ""
    var profileFeedFromMe: Bool = false
    var profilePoints: String = ""
    var profileInterest: String = ""
    var profileWriteHomeWallPermission: Bool = false
    
    //companies
    var idOfCompaniesArr: [String] = []
    var nameOfCompaniesArr: [String] = []
    var iconsOfCompaniesArr: [String] = []
    var descOfCompaniesArr: [String] = []
    
    //pick innitial company
    var companyTutorial: String = ""
    
    //order
    var orderScreenCanOrder: Bool = false
    var orderPaymentMethod: String = ""
    var orderPaymentMethodServer: String = ""
    
    //cached notifications
    var idNotificationArr: [String] = []
    var titleNotificationArr: [String] = []
    var bodyNotificationArr: [String] = []
    var userIdNotificationArr: [String] = []
    var userPhotoNotificationArr: [String] = []
    
    //service order
    var serviceOrderEmail: String = ""
    var serviceOrderNumber: String = ""
    var serviceOrderDesc: String = ""
    
    //service order company
    var serviceOrderIsCompanyOrder: Bool = false
    var serviceOrderCompanyName: String = ""
    var serviceOrderCompanyBid: String = ""
    var serviceOrderCompanyVat: String = ""
    var serviceOrderCompanyZip: String = ""
    var serviceOrderCompanyAddress: String = ""
    
    //
    var newFeedNotification = false
    var newListNotification = false
    
    /*
     *   Zde uložíme všechny příchozí notifikace, aby se mohly používat v NotificationVC
     */
    func saveNotification(){
        let defaults = UserDefaults.standard
        defaults.set(idNotificationArr, forKey: "\(userToken)idNotificationArr")
        defaults.set(titleNotificationArr, forKey: "\(userToken)titleNotificationArr")
        defaults.set(bodyNotificationArr, forKey: "\(userToken)bodyNotificationArr")
        defaults.set(userIdNotificationArr, forKey: "\(userToken)userIdNotificationArr")
        defaults.set(userPhotoNotificationArr, forKey: "\(userToken)userPhotoNotificationArr")
    }
    
    /*
     *   Zde uložené notifikace načítáme z paměti pro využití v NotificationVC
     */
    func getNotification(){
        if let savedValue = UserDefaults.standard.stringArray(forKey: "\(userToken)idNotificationArr") {
            idNotificationArr = savedValue
        }
        if let savedValue = UserDefaults.standard.stringArray(forKey: "\(userToken)titleNotificationArr") {
            titleNotificationArr = savedValue
        }
        if let savedValue = UserDefaults.standard.stringArray(forKey: "\(userToken)bodyNotificationArr") {
            bodyNotificationArr = savedValue
        }
        if let savedValue = UserDefaults.standard.stringArray(forKey: "\(userToken)userIdNotificationArr") {
            userIdNotificationArr = savedValue
        }
        if let savedValue = UserDefaults.standard.stringArray(forKey: "\(userToken)userPhotoNotificationArr") {
            userPhotoNotificationArr = savedValue
        }
    }
    
    /*
     *   Uložení všeho potřebného pro snadné automatické přihlašování.
     */
    func saveCurrentSession(){
        let defaults = UserDefaults.standard
        defaults.set(userToken, forKey: "userToken")
        defaults.set(securityToken, forKey: "\(userToken)securityToken")
        defaults.set(facebookAcc, forKey: "\(userToken)facebookAcc")
        defaults.set(isAfterRegistrationSelectedCompany, forKey: "\(userToken)isAfterRegistrationSelectedCompany")
    }
    
    func getCurrentSession(){
        if let savedValue = UserDefaults.standard.string(forKey: "userToken") {
            userToken = savedValue
        }
        if let savedValue = UserDefaults.standard.string(forKey: "\(userToken)securityToken") {
            securityToken = savedValue
        }
        if let savedValue = UserDefaults.standard.string(forKey: "\(userToken)facebookAcc") {
            facebookAcc = savedValue.toBool()
        }
        if let savedValue = UserDefaults.standard.string(forKey: "\(userToken)isAfterRegistrationSelectedCompany") {
            isAfterRegistrationSelectedCompany = savedValue.toBool()
        }
    }
    
}

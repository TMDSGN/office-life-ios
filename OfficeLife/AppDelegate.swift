//
//  AppDelegate.swift
//  OfficeLife
//
//  Created by Josef Antoni on 09.12.16.
//  Copyright © 2016 Josef Antoni. All rights reserved.
//

import UIKit
import iOS_Slide_Menu
import IQKeyboardManagerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import LinkedinSwift
import GooglePlaces
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var navigationController: UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        navigationController = application.windows[0].rootViewController as? UINavigationController
        
        UINavigationBar.appearance().barTintColor = UIColor.navBarColor
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Medium", size: 13)!, NSForegroundColorAttributeName: UIColor.white]
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().disabledToolbarClasses.append(MsgsWallVC.self)
        IQKeyboardManager.sharedManager().disabledToolbarClasses.append(ComentsToWallPostVC.self)
        
        GMSPlacesClient.provideAPIKey("AIzaSyAxDwfNauglsTOXupVGbvvdHzrOEeR7T0E")
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let leftMenu = (mainStoryboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC)
        SlideNavigationController.sharedInstance().leftMenu = leftMenu
        
        // Use Firebase library to configure APIs
        FIRApp.configure()
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        //GPAPI.shared().setClientID("1061399163", clientSecret: "stDTmVXF", productionMode: false)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotificaiton), name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func tokenRefreshNotificaiton(notification: NSNotification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            connectToFcm()
        }
    }
    
    //foregraund Notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let json = JSON(userInfo)
        let id = json["id"].stringValue
        let title = json["title"].stringValue
        let body = json["body"].stringValue
        let userId = json["user_id"].stringValue
        let userPhoto = json["user_photo"].stringValue
        let s = Session.sharedInstance
        s.getNotification()
        s.idNotificationArr.insert(id, at: 0)
        s.titleNotificationArr.insert(title, at: 0)
        s.bodyNotificationArr.insert(body, at: 0)
        s.userIdNotificationArr.insert(userId, at: 0)
        s.userPhotoNotificationArr.insert(userPhoto, at: 0)
        if s.idNotificationArr.count>20 {
            s.idNotificationArr.remove(at: 20)
            s.titleNotificationArr.remove(at: 20)
            s.bodyNotificationArr.remove(at: 20)
            s.userIdNotificationArr.remove(at: 20)
            s.userPhotoNotificationArr.remove(at: 20)
        }
        s.saveNotification()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNotificationVC"), object: nil)
        
        if title != "New message" && title != "New activity" {
            s.newFeedNotification = true
        } else {
            s.newListNotification = true
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshNavBar"), object: nil)
        //reload WALL screen
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadWall"), object: nil)
        //zkontrolovat zda v současném aktivní messageWALL okně je konverzace na kterou přišla notifikace
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationForCurrentThreadCheck"), object: ["receiverId":userId])

        completionHandler(.newData)
    }
    
    // NOTE: Need to use this when swizzling is disabled
    public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken as Data, type: FIRInstanceIDAPNSTokenType.prod)
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            connectToFcm()
        }
    }
    
    func tokenRefreshNotification(notification: NSNotification) {
        _ = FIRInstanceID.instanceID().token()
        connectToFcm()
    }
    
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM.")
            } else {
                print("Connected to FCM.")
                self.registerToGcm()
            }
        }
    }
    
    fileprivate func registerToGcm(){
        if let temp = FIRInstanceID.instanceID().token() {
            print("TOKEN: \(temp)")
            if !temp.description.isEmpty && !Session.sharedInstance.userToken.isEmpty && !Session.sharedInstance.securityToken.isEmpty {
                LoginReq().checkIfUserEnteredBuildingCode(completionHandler: { (connectionSucess, enteredCode, idOfBuilding) in
                    if connectionSucess {
                        if enteredCode {
                            Authentization().registerGcmToken(token: temp, completionHandler: { (done) in
                            })
                        }
                    }
                })
            }
        }
    }
    
    fileprivate func deleteToken(){
        FIRInstanceID.instanceID().delete { (err:Error?) in
            if err != nil{
                print(err.debugDescription);
            }else{
                print("Token Deleted");
            }
        }
        connectToFcm()
    }
    
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app,open: url as URL!,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    public func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        // Linkedin sdk handle redirect
        if LinkedinSwiftHelper.shouldHandle(url) {
            return LinkedinSwiftHelper.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url as URL!,
            sourceApplication: sourceApplication,
            annotation: annotation)
    }
    
    var shouldSupportAllOrientation = false
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if shouldSupportAllOrientation {
            return UIInterfaceOrientationMask.all
        }
        return UIInterfaceOrientationMask.portrait
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, didReceiveNotificationResponse response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void) {
        //Handle the notification
        if let temp = FIRInstanceID.instanceID().token() {
            if !temp.description.isEmpty && !Session.sharedInstance.userToken.isEmpty && !Session.sharedInstance.securityToken.isEmpty {
                let json = JSON(response.notification.request.content.userInfo)
                let title = json["title"].stringValue
                let id = json["id"].stringValue
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                if title == "New message" {
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
                    vc.conversationReceiverId = json["user_id"].stringValue
                    vc.messagesWall = true
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(vc, animated: true)
                    });
                } else if title == "New activity" {
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "ActivityDetailVC") as! ActivityDetailVC
                    vc.activityId = id
                    vc.titleCategory = "Activities".localized()
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(vc, animated: true)
                    });
                } else {
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(vc, animated: true)
                    });
                }
            }
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let temp = FIRInstanceID.instanceID().token() {
            if !temp.description.isEmpty && !Session.sharedInstance.userToken.isEmpty && !Session.sharedInstance.securityToken.isEmpty {
                let json = JSON(response.notification.request.content.userInfo)
                let title = json["title"].stringValue
                let id = json["id"].stringValue
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                if title == "New message" {
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
                    vc.conversationReceiverId = json["user_id"].stringValue
                    vc.messagesWall = true
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(vc, animated: true)
                    });
                } else if title == "New activity" {
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "ActivityDetailVC") as! ActivityDetailVC
                    vc.activityId = id
                    vc.titleCategory = "Activities".localized()
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(vc, animated: true)
                    });
                } else {
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MsgsWallVC") as! MsgsWallVC
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.navigationController?.pushViewController(vc, animated: true)
                    });
                }
            }
        }
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
    }
}

extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("%@", remoteMessage.appData)
    }
}


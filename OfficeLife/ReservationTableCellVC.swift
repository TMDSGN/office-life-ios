//
//  ReservationTableCellVC.swift
//  OfficeLife
//
//  Created by Josef Antoni on 15.01.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import UIKit

class ReservationTableCellVC: UITableViewCell {
    
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
}
